﻿using System;
using AVFoundation;
using Foundation;
using Speech;
using Xamarin.Forms;

[assembly: Dependency(typeof(butia.iOS.ASRService))]
namespace butia.iOS
{
	public class ASRService : IASRService
	{
		public event TextRecognized TextRecognized;

		private AVAudioEngine AudioEngine = new AVAudioEngine();
		private SFSpeechRecognizer SpeechRecognizer = new SFSpeechRecognizer();
		private SFSpeechAudioBufferRecognitionRequest LiveSpeechRequest;
		private SFSpeechRecognitionTask RecognitionTask;
		private bool started = false;

		public ASRService()
		{
			
		}

		public void Init()
		{
			if (started) return;
			
			// Request user authorization
			SFSpeechRecognizer.RequestAuthorization((SFSpeechRecognizerAuthorizationStatus status) =>
			{
				// Take action based on status
				switch (status)
				{
					case SFSpeechRecognizerAuthorizationStatus.Authorized:
						// User has approved speech recognition
						// Setup audio session
						var node = AudioEngine.InputNode;
						var recordingFormat = node.GetBusOutputFormat(0);
						node.InstallTapOnBus(0, 1024, recordingFormat, (AVAudioPcmBuffer buffer, AVAudioTime when) =>
						{
							// Append buffer to recognition request
							LiveSpeechRequest.Append(buffer);
						});
						started = true;
						break;
					case SFSpeechRecognizerAuthorizationStatus.Denied:
						// User has declined speech recognition

						break;
					case SFSpeechRecognizerAuthorizationStatus.NotDetermined:
						// Waiting on approval

						break;
					case SFSpeechRecognizerAuthorizationStatus.Restricted:
						// The device is not permitted

						break;
				}
			});
		}

		public void Listen()
		{
			LiveSpeechRequest = new SFSpeechAudioBufferRecognitionRequest();
			LiveSpeechRequest.ContextualStrings = new string[] {
				"seguir la pelotita",
				"la escondida",
				"tito",
				"atrás",
				"adelante",
				"buscar",
				"frenar",
				"izquierda",
				"derecha",
				"fin de juego"
			};
			// Start recording
			AudioEngine.Prepare();
			NSError error;
			AudioEngine.StartAndReturnError(out error);

			// Did recording start?
			if (error != null)
			{
				// Handle error and return
				return;
			}

			// Start recognition
			RecognitionTask = SpeechRecognizer.GetRecognitionTask(LiveSpeechRequest, (SFSpeechRecognitionResult result, NSError err) =>
			{
				// Was there an error?
				if (err != null)
				{
					// Handle error
				}
				else
				{
					// Is this the final translation?
					if (result.Final)
					{
						TextRecognized?.Invoke(result.BestTranscription.FormattedString);
					}
				}
			});
		}

		public void StopListening()
		{
			AudioEngine.Stop();
			RecognitionTask.Finish();
			LiveSpeechRequest.EndAudio();
		}
	}
}
