﻿using System;
using butia;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(butia.MyButton), typeof(MyButtonRenderer))]
namespace butia
{
	public class MyButtonRenderer : ButtonRenderer
	{
		public MyButtonRenderer()
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				/*
				Control.TouchUpInside += (sender, ev) =>
				{
					(Element as MyButton).TouchUp?.Execute(Element);
				};
				Control.TouchDown += (sender, ev) =>
				{
					(Element as MyButton).TouchDown?.Execute(Element);
				};
				*/
				var button = Element as MyButton;
				UILongPressGestureRecognizer recognizer = new UILongPressGestureRecognizer((UILongPressGestureRecognizer obj) =>
				{
					switch (obj.State)
					{
						case UIGestureRecognizerState.Began:
							button.TouchDown?.Execute(button);
							break;
						case UIGestureRecognizerState.Ended:
						case UIGestureRecognizerState.Failed:
						case UIGestureRecognizerState.Cancelled:
							button.TouchUp?.Execute(button);
							break;

					}
				});
				Control.AddGestureRecognizer(recognizer);
			}
		}
	}
} 