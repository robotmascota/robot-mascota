﻿using System;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Speech;
using Xamarin.Forms;

[assembly: Dependency(typeof(butia.Droid.ASRService))]
namespace butia.Droid
{
	public class ASRService : Java.Lang.Object, IRecognitionListener, IASRService
	{
		private SpeechRecognizer speechRecognizer;

		public ASRService()
		{
		}

		public event TextRecognized TextRecognized;

		public void Init()
		{
			speechRecognizer = SpeechRecognizer.CreateSpeechRecognizer(Android.App.Application.Context);

			speechRecognizer.SetRecognitionListener(this);
		}

		public void Listen()
		{
			var intent = new Intent();
			intent.PutExtra(RecognizerIntent.ExtraLanguage, "es-UY");
			intent.PutExtra(RecognizerIntent.ExtraMaxResults, 1);
			speechRecognizer.StartListening(intent);
		}

		public void StopListening()
		{
			speechRecognizer.StopListening();
			//throw new NotImplementedException();
		}

		public void OnBeginningOfSpeech()
		{
			//throw new NotImplementedException();
		}

		public void OnBufferReceived(byte[] buffer)
		{
			//throw new NotImplementedException();
		}

		public void OnEndOfSpeech()
		{
			//throw new NotImplementedException();
		}

		public void OnError([GeneratedEnum] SpeechRecognizerError error)
		{
			//throw new NotImplementedException();
		}

		public void OnEvent(int eventType, Bundle @params)
		{
			//throw new NotImplementedException();
		}

		public void OnPartialResults(Bundle partialResults)
		{
			//throw new NotImplementedException();
		}

		public void OnReadyForSpeech(Bundle @params)
		{
			//throw new NotImplementedException();
		}

		public void OnResults(Bundle results)
		{
			var res = results.GetStringArrayList(SpeechRecognizer.ResultsRecognition);
			TextRecognized?.Invoke(res[0]);
			//throw new NotImplementedException();
		}

		public void OnRmsChanged(float rmsdB)
		{
			//throw new NotImplementedException();
		}

	}


}
