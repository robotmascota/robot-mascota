﻿using System;
using butia;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(butia.MyButton), typeof(MyButtonRenderer))]
namespace butia
{
	public class MyButtonRenderer : ButtonRenderer
	{
		public MyButtonRenderer()
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				Control.Touch += (sender, ev) =>
				{
					if (ev.Event.Action == Android.Views.MotionEventActions.Down)
						(Element as MyButton).TouchDown?.Execute(Element);
					else if (ev.Event.Action == Android.Views.MotionEventActions.Up)
						(Element as MyButton).TouchUp?.Execute(Element);

				};
			}
		}
	}
}