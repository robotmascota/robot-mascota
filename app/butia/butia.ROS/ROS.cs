﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebSocket4Net;
using Xamarin.Forms;

[assembly: Dependency(typeof(butia.ROS.ROS))]
namespace butia.ROS
{
	public class ROS : IROS
	{
		private const string defaultWsUrl = "ws://192.168.1.71:9090";
		private const int bufferSize = 4096 * 20;
		private CancellationToken cancellationToken;
		WebSocket client;

		public bool Connected
		{
			get
			{
				return client != null && client.State == WebSocketState.Open;
			}
		}

		public ROS()
		{
		}

		public event MessageReceived MessageReceived;
		public event ConnectionChanged ConnectionChanged;

		public async Task Advertise(string topicName, string topicType)
		{
			var advertiseMessage = "{\"op\": \"advertise\",\"topic\": \"" + topicName + "\",\"type\": \"" + topicType + "\"}";
			client.Send(advertiseMessage);
		}

		public async Task Connect(string url = null)
		{
			var wsUri = url != null ? url : defaultWsUrl;
			client = new WebSocket(wsUri);

			client.Opened += async (sender, e) =>
			{
				await Advertise("ping_topic", "std_msgs/String");
				await Advertise("butia_app_listen_topic", "layer1/app_listen");
				await Subscribe("imgs");
				await Subscribe("texts");

				this.ConnectionChanged?.Invoke();

				StartListening();
			};

			client.Closed += (sender, e) =>
			{
				this.ConnectionChanged?.Invoke();
			};

			client.Open();
		}

		public async Task Disconnect()
		{
			try
			{
				client.Close();
				this.ConnectionChanged?.Invoke();
			}
			catch (Exception ex)
			{
				Console.WriteLine("--------------------------------->Error");
				Console.WriteLine(ex);
			}
		}

		private async Task StartListening()
		{
			client.MessageReceived += (sender, e) =>
			{
				var msg = JsonConvert.DeserializeObject<RosMessage>(e.Message);
				MessageReceived?.Invoke(msg);
			};
		}

		public async Task SendMessage(string topicName, string message)
		{
			try
			{
				var jsonMessage = "{\"op\":\"publish\",\"topic\":\"" + topicName + "\",\"msg\":{\"phrase\":\"{" + message.ToLower() + "}\"}}";
				client.Send(jsonMessage);
			}
			catch
			{
				this.ConnectionChanged?.Invoke();
			}
		}

		public async Task Subscribe(string topicName)
		{
			try
			{
				var subscribeMessage = "{\"op\": \"subscribe\",\"topic\": \"/" + topicName + "\",\"type\": \"std_msgs/String\"}";
				client.Send(subscribeMessage);
			}
			catch
			{
				this.ConnectionChanged?.Invoke();
			}
		}

	}
}
