﻿using System;
using System.Threading.Tasks;

namespace butia.ROS
{
	public delegate void MessageReceived(RosMessage msg);
	public delegate void ConnectionChanged();

	public interface IROS
	{
		event MessageReceived MessageReceived;
		event ConnectionChanged ConnectionChanged;
		bool Connected { get; }
		Task Connect(string url = null);
		Task Disconnect();
		Task Advertise(string topicName, string topicType);
		Task Subscribe(string topicName);
		Task SendMessage(string topicName, string message);
	}
}
