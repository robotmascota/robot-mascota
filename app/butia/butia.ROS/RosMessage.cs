﻿using System;
namespace butia.ROS
{
	public class RosMessage
	{
		public RosMessage() { }

		public string Topic { get; set; }
		public DataWrapper Msg { get; set; }
	}

	public class DataWrapper
	{
		public DataWrapper() { }

		public string Data { get; set; }
	}
}
