﻿using Plugin.Settings;

namespace butia.Utils
{
	public static class Settings
	{
		public static string IP
		{
			get { return CrossSettings.Current.GetValueOrDefault("IP", "192.168.1.70"); }
			set { CrossSettings.Current.AddOrUpdateValue("IP", value); }
		}

		public static bool IsTesting
		{
			get { return CrossSettings.Current.GetValueOrDefault("Testing", false); }
			set { CrossSettings.Current.AddOrUpdateValue("Testing", value); }
		}
	}
}
