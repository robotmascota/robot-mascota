﻿using System;
using System.Collections.Generic;
using butia.ViewModels;
using Xamarin.Forms;

namespace butia.Pages
{
	public partial class Drawer : ContentPage
	{
		public Drawer()
		{
			InitializeComponent();
			BindingContext = new DrawerViewModel();
		}
	}
}
