﻿using System;
using System.Collections.Generic;
using butia.ViewModels;
using Xamarin.Forms;

namespace butia.Pages
{
	public partial class Main : MasterDetailPage
	{
		public Main()
		{
			InitializeComponent();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			MessagingCenter.Subscribe<DrawerViewModel>(this, "Configure", (obj) => {
				if (!(Detail is Configuration))
				{
					var navigationPage = new NavigationPage(new Configuration());
					navigationPage.Title = "Configuration";
					navigationPage.BarTextColor = Color.White;
					navigationPage.BarBackgroundColor = Color.FromRgb(116, 183, 12);
					Detail = navigationPage;
					IsPresented = false;
				}
			});

			MessagingCenter.Subscribe<DrawerViewModel>(this, "Chat", (obj) =>
			{
				if (!(Detail is Chat))
				{
					var navigationPage = new NavigationPage(new Chat());
					navigationPage.Title = "Butia";
					navigationPage.BarTextColor = Color.White;
					navigationPage.BarBackgroundColor = Color.FromRgb(116, 183, 12);
					Detail = navigationPage;
					IsPresented = false;
				}
			});
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			MessagingCenter.Unsubscribe<DrawerViewModel>(this, "Configure");
			MessagingCenter.Unsubscribe<DrawerViewModel>(this, "Chat");
		}
	}
}
