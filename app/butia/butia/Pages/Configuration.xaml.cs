﻿using System;
using System.Collections.Generic;
using butia.ViewModels;
using Xamarin.Forms;

namespace butia.Pages
{
	public partial class Configuration : ContentPage
	{
		public Configuration()
		{
			InitializeComponent();
			BindingContext = new ConfigurationViewModel();
		}
	}
}
