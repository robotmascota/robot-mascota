﻿using System;
using butia.ROS;
using butia.ViewModels;
using Xamarin.Forms;

namespace butia.Pages
{
	public partial class Chat : ContentPage
	{
		ChatViewModel viewModel;

		public Chat()
		{
			InitializeComponent();

			viewModel = new ChatViewModel();
			BindingContext = viewModel;

			viewModel.History.CollectionChanged += (o, e) =>
			{
				Device.BeginInvokeOnMainThread(() => HistoryList.ScrollTo(viewModel.History[viewModel.History.Count - 1], ScrollToPosition.End, true));
			};

			viewModel.Init();

			DisableSelection();
		}

		void DisableSelection()
		{
			HistoryList.ItemSelected += (sender, e) =>
			{
				((ListView)sender).SelectedItem = null;
			};
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			DependencyService.Get<IROS>(DependencyFetchTarget.GlobalInstance).Disconnect();
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			viewModel.Reconnect();
		}
	}
}
