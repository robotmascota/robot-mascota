﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace butia
{
	public class MyButton : Button
	{
		public MyButton()
		{
		}

		public static readonly BindableProperty TouchDownProperty =
			BindableProperty.Create("TouchDown", typeof(ICommand), typeof(ICommand), defaultBindingMode: BindingMode.OneWay);

		public static readonly BindableProperty TouchUpProperty =
			BindableProperty.Create("TouchUpProperty", typeof(ICommand), typeof(ICommand), defaultBindingMode: BindingMode.OneWay);
		
		public ICommand TouchDown
		{
			get { return (ICommand)GetValue(TouchDownProperty); }
			set { SetValue(TouchDownProperty, value); }
		}

		public ICommand TouchUp
		{
			get { return (ICommand)GetValue(TouchUpProperty); }
			set { SetValue(TouchUpProperty, value); }
		}
	}
}
