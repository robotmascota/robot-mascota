﻿using System;
namespace butia
{
	public delegate void TextRecognized(string text);

	public interface IASRService
	{
		event TextRecognized TextRecognized;
		void Listen();
		void StopListening();
		void Init();
	}
}
