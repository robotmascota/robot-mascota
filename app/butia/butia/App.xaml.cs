﻿using butia.Pages;
using butia.ROS;
using Xamarin.Forms;

namespace butia
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			MainPage = new Pages.Main();
		}

		protected override void OnStart()
		{
		}

		protected override void OnSleep()
		{
			//DependencyService.Get<IROS>(DependencyFetchTarget.GlobalInstance).Disconnect();
		}

		protected override void OnResume()
		{
			//DependencyService.Get<IROS>(DependencyFetchTarget.GlobalInstance).Connect();
		}
	}
}
