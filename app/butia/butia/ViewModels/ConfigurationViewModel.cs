﻿using System;
using System.ComponentModel;
using butia.Utils;

namespace butia.ViewModels
{
	public class ConfigurationViewModel : INotifyPropertyChanged
	{
		public ConfigurationViewModel()
		{
		}

		public string IP
		{
			get { return Settings.IP; }
			set { Settings.IP = value; }
		}

		public bool Testing
		{
			get { return Settings.IsTesting; }
			set { Settings.IsTesting = value; }
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}
