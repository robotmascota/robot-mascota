﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using butia.ROS;
using butia.Utils;
using Plugin.Connectivity;
using Plugin.Vibrate;
using Xamarin.Forms;

namespace butia.ViewModels
{
	public class ChatViewModel : INotifyPropertyChanged
	{
		bool isRecording = false;
		bool isConnected = false;
		bool hasImage = false;
		ImageSource lastImage;
		IROS ros;
		IASRService asr;

		public event PropertyChangedEventHandler PropertyChanged;

		public ObservableCollection<Message> History { get; set; } = new ObservableCollection<Message>();
		public ICommand RecordCommand { get; set; }
		public ICommand StopCommand { get; set; }
		public ICommand ReconnectCommand { get; set; }
		public ICommand StopGameCommand { get; set; }


		public ImageSource LastImage
		{
			get { return lastImage; }
			set
			{
				lastImage = value;
				ShowImage = true;
				PropertyChanged(this, new PropertyChangedEventArgs("LastImage"));
			}
		}

		public bool ShowImage
		{
			get
			{
				return hasImage && isConnected;
			}
			set
			{
				hasImage = value;
				PropertyChanged(this, new PropertyChangedEventArgs("ShowImage"));
			}
		}

		public bool IsConnected
		{
			get
			{
                return isConnected || Settings.IsTesting;
			}
			set
			{
				isConnected = value;
				PropertyChanged(this, new PropertyChangedEventArgs("IsConnected"));
				PropertyChanged(this, new PropertyChangedEventArgs("IsNotConnected"));
				PropertyChanged(this, new PropertyChangedEventArgs("ShowImage"));
			}
		}

		public bool IsNotConnected
		{
			get
			{
				return !IsConnected;
			}
		}

		public bool IsRecording
		{
			get
			{
				return isRecording;
			}
			set 
			{
				isRecording = value;
				PropertyChanged(this, new PropertyChangedEventArgs("IsRecording"));
				PropertyChanged(this, new PropertyChangedEventArgs("IsNotRecording"));
			}
		}

		public bool IsNotRecording
		{
			get
			{
				return !IsRecording;
			}
		}

		public ChatViewModel()
		{
			RecordCommand = new Command(Record);
			StopCommand = new Command(Stop);
			ReconnectCommand = new Command(Reconnect);
			StopGameCommand = new Command(StopGame);

			asr = DependencyService.Get<IASRService>(DependencyFetchTarget.GlobalInstance);
			ros = DependencyService.Get<IROS>(DependencyFetchTarget.GlobalInstance);

			CrossConnectivity.Current.ConnectivityChanged += (sender, e) =>
			{
				Ros_ConnectionChanged();
			};
		}

		public async Task Init()
		{
			InitASR();
			await InitROS();
		}

		async Task InitROS()
		{
			ros.ConnectionChanged += Ros_ConnectionChanged;
		}

		void Ros_ConnectionChanged()
		{
			if (ros.Connected && !IsConnected)
			{
				ros.MessageReceived += Ros_MessageReceived;
				CrossVibrate.Current.Vibration(300);
				IsConnected = true;
			}
			else if (IsConnected)
			{
				ros.MessageReceived -= Ros_MessageReceived;
				CrossVibrate.Current.Vibration(300);
				IsConnected = false;
			}
		}

		private object _lock = new object();

		void Ros_MessageReceived(RosMessage msg)
		{
			lock(_lock)
			{
				if (msg.Topic != "/imgs")
				{
					History.Add(new Message()
					{
						Direction = MessageDirection.Incoming,
						Type = msg.Topic == "/imgs" ? MessageType.Image : MessageType.Text,
						Value = msg.Msg.Data,
					});
				}
				else
				{ 
					LastImage = ImageSource.FromStream(() => new MemoryStream(System.Convert.FromBase64String(msg.Msg.Data)));
				}

				//CrossVibrate.Current.Vibration(300);
			}
		}

		void InitASR()
		{
			asr = DependencyService.Get<IASRService>(DependencyFetchTarget.GlobalInstance);
			asr.TextRecognized -= Asr_TextRecognized;
			asr.TextRecognized += Asr_TextRecognized; 
			asr.Init();
		}

		async void Asr_TextRecognized(string text)
		{
			if (ros.Connected || Settings.IsTesting)
			{
                if (!Settings.IsTesting)
                {
                    await ros.SendMessage("butia_app_listen_topic", text);
                }
				History.Add(new Message()
				{
					Direction = MessageDirection.Outgoing,
					Type = MessageType.Text,
					Value = text
				});
			}
		}

		public void Record()
		{
			CrossVibrate.Current.Vibration(300);
			IsRecording = true;
			asr.Listen();
			//CrossVibrate.Current.Vibration(300);
		}

		public void Stop()
		{
			IsRecording = false;
			asr.StopListening();
			CrossVibrate.Current.Vibration(300);
		}

		public async void StopGame()
		{ 
			await ros.SendMessage("butia_app_listen_topic", "Fin de juego");
		}

		public void Reconnect()
		{
			ros.Connect($"ws://{Settings.IP}:9090");
		}
	}
}
