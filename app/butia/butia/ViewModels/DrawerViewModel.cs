﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using butia.Utils;
using Xamarin.Forms;

namespace butia.ViewModels
{
	public class DrawerViewModel : INotifyPropertyChanged
	{
		public ICommand ConfigureCommand { get; set; }
		public ICommand ChatCommand { get; set; }
		public string IP
		{
			get { return Settings.IP; }
			set { Settings.IP = value; }
		}
		public event PropertyChangedEventHandler PropertyChanged;

		public DrawerViewModel()
		{
			ConfigureCommand = new Command((obj) => MessagingCenter.Send(this, "Configure"));
			ChatCommand = new Command((obj) => MessagingCenter.Send(this, "Chat"));
		}
	}
}
