﻿using System;
using System.IO;
using Xamarin.Forms;

namespace butia
{
	public class Message
	{
		private ImageSource image;

		public MessageDirection Direction { get; set; }
		public MessageType Type { get; set; }
		public string Value { get; set; }
		public DateTime Timestamp { get; set; }
		public bool IsImage { get { return Type == MessageType.Image; } }
		public bool IsText { get { return Type == MessageType.Text; } }
		public string Text
		{
			get
			{
				if (IsText) return Value;
				return null;
			}
		}
		public ImageSource Image { get
			{
				if (IsImage)
				{
					if (image == null)
						image = ImageSource.FromStream(() => new MemoryStream(System.Convert.FromBase64String(Value)));
					return image;
				}
				return null;
			}
		}
		public Message()
		{
			Timestamp = DateTime.Now;
		}
	}
}
