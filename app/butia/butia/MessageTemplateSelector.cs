﻿using System;
using Xamarin.Forms;

namespace butia
{
	public class MessageTemplateSelector : Xamarin.Forms.DataTemplateSelector
	{
		private readonly DataTemplate OutgoingMessageTemplate;
		private readonly DataTemplate IncomingMessageTemplate;

		public MessageTemplateSelector()
		{	
			OutgoingMessageTemplate = new DataTemplate(typeof(OutgoingMessageCell));
			IncomingMessageTemplate = new DataTemplate(typeof(IncomingMessageCell));
		}

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			var message = item as Message;
			return message.Direction == MessageDirection.Incoming ? IncomingMessageTemplate : OutgoingMessageTemplate;
		}
	}
}
