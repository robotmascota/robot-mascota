#!/usr/bin/env python
import rospy
from butia_tracking.msg import tracking
from utils import log_data_info
from layer1.srv import simple_object_detect
from layer2.srv import butia_search_service
from layer2.msg import butia_move_msg, butia_search_msg
from api_constants import SEARCH_NODE_NAME, SEARCH_MODES, TARGETS, \
    GENERAL_NODE_RESULTS, VISION_OBJECT_ID, SEARCH_SRV_NAME, SEARCH_TOPIC_NAME,\
    EXECUTION_STATUS, MOVEMENT_DIRECTIONS, \
    IMAGE_PROCESSING_SRV_NAME, IMAGE_PROCESSING_TOPIC_NAME,  \
    MOVE_TOPIC_NAME

# create messages that are used to publish feedback/result
image_processing_srv = None
image_processing_topic = None
move_topic = None
input_params = None
publisher = None
execution_status = EXECUTION_STATUS['disabled']


def service_callback(data):
    global execution_status
    if data.enable:
        # Start node execution
        log_data_info(SEARCH_NODE_NAME, 'Init')

        # -------------- VALIDATIONS --------------
        if data.mode not in SEARCH_MODES.values() \
                or data.target not in TARGETS.values():
            log_data_info(SEARCH_NODE_NAME, 'Aborted because of mode:%s '
                                            'and target:%s' %
                          (data.mode, data.target))
            return GENERAL_NODE_RESULTS['invalid_parameter']

        if execution_status == EXECUTION_STATUS['disabled']:
            res = image_processing_srv(VISION_OBJECT_ID[data.target])
            if res:
                # Listen to vision topic
                global image_processing_topic
                image_processing_topic = rospy.Subscriber(
                    IMAGE_PROCESSING_TOPIC_NAME, tracking, vision_callback)

                execution_status = EXECUTION_STATUS['enabled']
                log_data_info(SEARCH_NODE_NAME, 'Service enabled')
        else:
            log_data_info(SEARCH_NODE_NAME,"------- ALREADY STARTED -------")

        global input_params
        input_params = data
        move_around_state(data.mode, data.start_moving_left)
        return GENERAL_NODE_RESULTS['processing']
    else:
        if execution_status == EXECUTION_STATUS['enabled']:
            execution_status = EXECUTION_STATUS['disabled']
            image_processing_topic.unregister()
            stop_state(input_params.mode)

        log_data_info(SEARCH_NODE_NAME, 'Service disabled')
        return GENERAL_NODE_RESULTS['disabled']


def vision_callback(data):
    try:
        if data.obj_found:
            log_data_info(SEARCH_NODE_NAME, 'state: FOUND')
            stop_state(input_params.mode)
            publish_result(
                data.obj_type,
                data.pic_height,
                data.pic_width,
                data.obj_height,
                data.obj_width,
                data.obj_coord_x,
                data.obj_coord_y
            )
        else:
            log_data_info(SEARCH_NODE_NAME, 'state: NOT FOUND')
            publish_result(obj_found=False)
    except Exception as e:
        if execution_status == EXECUTION_STATUS['enabled']:
            raise e


def move_around_state(mode, start_moving_left):
    if mode == SEARCH_MODES['around']:
        log_data_info(SEARCH_NODE_NAME, 'state: MOVE_AROUND')
        move_data = butia_move_msg()
        move_data.direction = MOVEMENT_DIRECTIONS['right']
        if start_moving_left:
            move_data.direction = MOVEMENT_DIRECTIONS['left']
        move_topic.publish(move_data)


def stop_state(mode):
    if mode == SEARCH_MODES['around']:
        log_data_info(SEARCH_NODE_NAME, 'state: STOP')
        move_data = butia_move_msg()
        move_data.direction = MOVEMENT_DIRECTIONS['stop']
        move_topic.publish(move_data)


def publish_result(obj_type=0, pic_height=0, pic_width=0, obj_height=0,
                   obj_width=0, obj_coord_x=0, obj_coord_y=0, obj_found=True):
    if execution_status == EXECUTION_STATUS['enabled']:
        res = butia_search_msg()
        res.obj_found = obj_found
        if obj_found:
            res.obj_type = obj_type
            res.pic_height = pic_height
            res.pic_width = pic_width
            res.obj_height = obj_height
            res.obj_width = obj_width
            res.obj_coord_x = obj_coord_x
            res.obj_coord_y = obj_coord_y

        publisher.publish(res)


if __name__ == '__main__':
    rospy.init_node(SEARCH_NODE_NAME, log_level=rospy.INFO)
    log_data_info(SEARCH_NODE_NAME, '------- START ------- ')

    # -------------- CONNECTIONS --------------
    # BUTIA_IMAGE service
    log_data_info(SEARCH_NODE_NAME, 'waiting for service: %s' %
                  IMAGE_PROCESSING_SRV_NAME)
    rospy.wait_for_service(IMAGE_PROCESSING_SRV_NAME)
    log_data_info(SEARCH_NODE_NAME, 'calling for service: %s' %
                  IMAGE_PROCESSING_SRV_NAME)
    image_processing_srv = rospy.ServiceProxy(
        IMAGE_PROCESSING_SRV_NAME, simple_object_detect)

    # BUTIA_MOVE service
    log_data_info(SEARCH_NODE_NAME, '    waiting for topic: %s' %
                  MOVE_TOPIC_NAME)
    move_topic = rospy.Publisher(MOVE_TOPIC_NAME, butia_move_msg, queue_size=1)
    # ------------------------------------------

    log_data_info(SEARCH_NODE_NAME, 'waiting for a service call')
    rospy.Service(SEARCH_SRV_NAME, butia_search_service, service_callback)
    global publisher
    publisher = rospy.Publisher(
        SEARCH_TOPIC_NAME, butia_search_msg, queue_size=1)
    
    rospy.spin()
