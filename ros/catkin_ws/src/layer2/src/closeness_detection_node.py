#!/usr/bin/env python
import rospy
from layer1.msg import sharp_distance_array
from utils import log_data_info
from layer2.msg import butia_close_detection_msg
from api_constants import CLOSE_DETECTION_NODE_NAME, CLOSENESS_DETECTION_TOPIC_NAME, \
    CLOSENESS_TOPIC_NAME

# create messages that are used to publish feedback/result
publisher = None

SENSOR_DIRECTIONS_MAPS = {
    1: 'front',
    2: 'back'
}
CLOSENESS_THRESHOLD = 0.2


def closeness_callback(data):
    res = butia_close_detection_msg()

    for sensor_data in data.sensors:
        too_close = sensor_data.metersDistance <= CLOSENESS_THRESHOLD
        if SENSOR_DIRECTIONS_MAPS[sensor_data.portNumber] == 'front':
            res.front = too_close
        elif SENSOR_DIRECTIONS_MAPS[sensor_data.portNumber] == 'back':
            res.back = too_close
        elif SENSOR_DIRECTIONS_MAPS[sensor_data.portNumber] == 'left':
            res.left = too_close
        elif SENSOR_DIRECTIONS_MAPS[sensor_data.portNumber] == 'right':
            res.right = too_close

    if res.front or res.back or res.left or res.right:
        publisher.publish(res)


if __name__ == '__main__':
    rospy.init_node(CLOSE_DETECTION_NODE_NAME, log_level=rospy.INFO)
    log_data_info(CLOSE_DETECTION_NODE_NAME, '------- START ------- ')

    global publisher
    publisher = rospy.Publisher(
        CLOSENESS_DETECTION_TOPIC_NAME,
        butia_close_detection_msg,
        queue_size=1)

    closeness_topic = rospy.Subscriber(
        CLOSENESS_TOPIC_NAME, sharp_distance_array, closeness_callback)

    rospy.spin()
