#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from utils import log_data_info
from layer2.msg import butia_move_msg
from api_constants import MOVEMENT_DIRECTIONS, MOVE_NODE_NAME, MOVE_TOPIC_NAME,\
    SERVO_TOPIC_NAME

publisher = None


def turnRight():
    twist = Twist()
    twist.linear.x = 0.15
    twist.linear.y = 0
    twist.linear.z = 0
    twist.angular.x = 0
    twist.angular.y = 0
    twist.angular.z = -0.15
    call_servo(twist)


def turnLeft():
    twist = Twist()
    twist.linear.x = 0.15
    twist.linear.y = 0
    twist.linear.z = 0
    twist.angular.x = 0
    twist.angular.y = 0
    twist.angular.z = 0.15
    call_servo(twist)


def moveForward():
    twist = Twist()
    twist.linear.x = 1
    twist.linear.y = 0
    twist.linear.z = 0
    twist.angular.x = 0
    twist.angular.y = 0
    twist.angular.z = 0
    call_servo(twist)

def moveBackward():
    twist = Twist()
    twist.linear.x = -1
    twist.linear.y = 0
    twist.linear.z = 0
    twist.angular.x = 0
    twist.angular.y = 0
    twist.angular.z = 0
    call_servo(twist)


def stop():
    twist = Twist()
    twist.linear.x = 0
    twist.linear.y = 0
    twist.linear.z = 0
    twist.angular.x = 0
    twist.angular.y = 0
    twist.angular.z = 0
    call_servo(twist)


def call_servo(directions):
    data = directions
    log_data_info(MOVE_NODE_NAME, '--------------------------->Calling Servo')
    move_topic.publish(data)


def callback(data):
    log_data_info(MOVE_NODE_NAME, 'moving %s' % data.direction)

    if data.direction not in MOVEMENT_DIRECTIONS.values():
        # return False
        return '1'

    if data.direction == MOVEMENT_DIRECTIONS['right']:
        turnRight()
    elif data.direction == MOVEMENT_DIRECTIONS['left']:
        turnLeft()
    elif data.direction == MOVEMENT_DIRECTIONS['forward']:
        moveForward()
    elif data.direction == MOVEMENT_DIRECTIONS['backward']:
        moveBackward()
    elif data.direction == MOVEMENT_DIRECTIONS['stop']:
        stop()


def move():
    rospy.init_node(MOVE_NODE_NAME)
    log_data_info(MOVE_NODE_NAME, '------- START ------- ')

    # -------------- CONNECTIONS --------------
    # butia_servo service
    log_data_info(MOVE_NODE_NAME, 'waiting for topic: %s' % SERVO_TOPIC_NAME)
    global move_topic
    move_topic = rospy.Publisher(SERVO_TOPIC_NAME, Twist, queue_size=1)
    # ------------------------------------------

    # rospy.Service(MOVE_SRV_NAME, butia_move_service, callback)
    rospy.Subscriber(MOVE_TOPIC_NAME, butia_move_msg, callback)

    rospy.spin()

if __name__ == '__main__':
    move()
