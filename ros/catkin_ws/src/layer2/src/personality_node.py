#!/usr/bin/env python
import rospy
import led_matrix_expressions
from layer1.srv import play_soundRequest, stop_soundRequest, stop_sound, \
    play_sound
from utils import log_data_info
from layer2.srv import butia_personality_sound_srv, \
    butia_personality_image_srv, butia_personality_clear_srv, \
    butia_personality_sound_srvResponse, butia_personality_image_srvResponse, \
    butia_personality_clear_srvResponse
from layer2.msg import butia_personality_high_logic_msg
from api_constants import PERSONALITY_NODE_NAME, SOUND_PLAY_SRV_NAME, \
    PERSONALITY_EMOTIONS, PERSONALITY_TASKS, \
    PERSONALITY_HIGH_LOGIC_TOPIC_NAME, PERSONALITY_HIGH_LOGIC_MODES, \
    PERSONALITY_SOUND_SRV_NAME, PERSONALITY_IMAGE_SRV_NAME, \
    PERSONALITY_CLEAR_SRV_NAME, SOUND_STOP_SRV_NAME


HAPPY_EMOTION_SOUND_PATH = '/root/robot-mascota/files/found.wav'
SAD_EMOTION_SOUND_PATH = '/root/robot-mascota/files/fail.wav'
LOVE_EMOTION_SOUND_PATH = '/root/robot-mascota/files/found.wav'
OK_EMOTION_SOUND_PATH = '/root/robot-mascota/files/found.wav'
BAD_EMOTION_SOUND_PATH = '/root/robot-mascota/files/found.wav'
CONFUSION_EMOTION_SOUND_PATH = '/root/robot-mascota/files/found.wav'
STOP_EMOTION_SOUND_PATH = '/root/robot-mascota/files/found.wav'

# create messages that are used to publish feedback/result
sound_play_srv = None
matrixExpressions = None


def high_logic_callback(data):
    # Validations
    if data.mode not in PERSONALITY_HIGH_LOGIC_MODES.values():
        log_data_info(PERSONALITY_NODE_NAME, 'Invalid mode!')
        return '1'

    if data.mode == PERSONALITY_HIGH_LOGIC_MODES['show_emotion']:
        if data.value not in PERSONALITY_EMOTIONS.values():
            log_data_info(PERSONALITY_NODE_NAME, ' Invalid emotion!')
            return '1'

        log_data_info(PERSONALITY_NODE_NAME, 'Showing an emotion: %s'
                      % data.value)
        if data.value == PERSONALITY_EMOTIONS['happy']:
            sound_play(HAPPY_EMOTION_SOUND_PATH)
            matrixExpressions.happy()
        elif data.value == PERSONALITY_EMOTIONS['sad']:
            sound_play(SAD_EMOTION_SOUND_PATH)
            matrixExpressions.sad()
        elif data.value == PERSONALITY_EMOTIONS['love']:
            sound_play(LOVE_EMOTION_SOUND_PATH)
            matrixExpressions.love()
        elif data.value == PERSONALITY_EMOTIONS['ok']:
            sound_play(OK_EMOTION_SOUND_PATH)
            matrixExpressions.ok()
        elif data.value == PERSONALITY_EMOTIONS['bad']:
            sound_play(BAD_EMOTION_SOUND_PATH)
            matrixExpressions.bad()
        elif data.value == PERSONALITY_EMOTIONS['confusion']:
            sound_play(CONFUSION_EMOTION_SOUND_PATH)
            matrixExpressions.question()
        elif data.value == PERSONALITY_EMOTIONS['stop']:
            sound_play(STOP_EMOTION_SOUND_PATH)
            matrixExpressions.stop()
    elif data.mode == PERSONALITY_HIGH_LOGIC_MODES['show_task']:
        if data.value == PERSONALITY_TASKS['forward']:
            matrixExpressions.moveUp()
        elif data.value == PERSONALITY_TASKS['back']:
            matrixExpressions.moveDown()
        elif data.value == PERSONALITY_TASKS['left']:
            matrixExpressions.moveLeft()
        elif data.value == PERSONALITY_TASKS['right']:
            matrixExpressions.moveRight()
        elif data.value == PERSONALITY_TASKS['searching']:
            matrixExpressions.search()
    elif data.mode == PERSONALITY_HIGH_LOGIC_MODES['show_phrase']:
        matrixExpressions.showMessage(data.value)

def sound_callback(data):
    sound_play(data.filename)
    res = butia_personality_sound_srvResponse()
    res.status = True
    return res


def image_callback(data):
    matrixExpressions.showDraw(data.matrix1_filename, data.matrix2_filename)
    res = butia_personality_image_srvResponse()
    res.status = True
    return res


def clear_callback(data):
    sound_stop()
    matrixExpressions.clear()
    res = butia_personality_clear_srvResponse()
    res.status = True
    return res


def sound_play(filename):
    log_data_info(PERSONALITY_NODE_NAME, 'Calling PLAY_SOUND')
    srv_data = play_soundRequest()
    srv_data.fileName = filename
    sound_play_srv(srv_data)


def sound_stop():
    log_data_info(PERSONALITY_NODE_NAME, 'Calling PLAY_SOUND')
    srv_data = stop_soundRequest()
    sound_stop_srv(srv_data)


if __name__ == '__main__':
    rospy.init_node(PERSONALITY_NODE_NAME, log_level=rospy.INFO)
    log_data_info(PERSONALITY_NODE_NAME, '------- START ------- ')

    # -------------- CONNECTIONS --------------
    # BUTIA_SOUND_PLAY service
    log_data_info(PERSONALITY_NODE_NAME, 'waiting for service: %s' %
                  SOUND_PLAY_SRV_NAME)
    rospy.wait_for_service(SOUND_PLAY_SRV_NAME)
    log_data_info(PERSONALITY_NODE_NAME, 'calling for service: %s' %
                  SOUND_PLAY_SRV_NAME)
    sound_play_srv = rospy.ServiceProxy(SOUND_PLAY_SRV_NAME, play_sound)

    # BUTIA_SOUND_STOP service
    log_data_info(PERSONALITY_NODE_NAME, 'waiting for service: %s' %
                  SOUND_STOP_SRV_NAME)
    rospy.wait_for_service(SOUND_STOP_SRV_NAME)
    log_data_info(PERSONALITY_NODE_NAME, 'calling for service: %s' %
                  SOUND_STOP_SRV_NAME)
    sound_stop_srv = rospy.ServiceProxy(SOUND_STOP_SRV_NAME, stop_sound)

    # Led Matrix class
    matrixExpressions = led_matrix_expressions.LedMatrixExpressions()

    # ------------------------------------------

    # High logic topic
    rospy.Subscriber(
        PERSONALITY_HIGH_LOGIC_TOPIC_NAME, butia_personality_high_logic_msg,
        high_logic_callback)

    # Services
    rospy.Service(PERSONALITY_SOUND_SRV_NAME, butia_personality_sound_srv,
                  sound_callback)
    rospy.Service(PERSONALITY_IMAGE_SRV_NAME, butia_personality_image_srv,
                  image_callback)
    rospy.Service(PERSONALITY_CLEAR_SRV_NAME, butia_personality_clear_srv,
                  clear_callback)

    log_data_info(PERSONALITY_NODE_NAME, 'waiting for node call')

    rospy.spin()

    matrixExpressions.close()
