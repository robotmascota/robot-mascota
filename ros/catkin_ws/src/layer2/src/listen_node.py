#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import ast

from layer1.msg import app_listen
from utils import log_data_info
from layer2.srv import butia_listen_service, butia_commands_listen_service
from layer2.msg import butia_listen_msg
from api_constants import LISTEN_NODE_NAME, LISTEN_SRV_NAME, \
    EXECUTION_STATUS, GENERAL_NODE_RESULTS, \
    LISTEN_APP_TOPIC_NAME, LISTEN_ADD_COMMANDS_SRV_NAME, \
    LISTEN_REMOVE_COMMANDS_SRV_NAME

butia_app_listen_topic = None
input_params = None
# commands_to_find is a dict with key=command, value=set(topic_name)
commands_to_find = dict()
execution_status = EXECUTION_STATUS['disabled']
# publishers is a dict with key=topic_name, value=set(publisher)
publishers = dict()


def service_add_commands_callback(data):
    global commands_to_find
    for command in ast.literal_eval(data.command_list):
        if command:
            command = str.lower(command)
            if command not in commands_to_find:
                commands_to_find[command] = set()
            commands_to_find[command].add(data.topic_name)
            if data.topic_name not in publishers:
                publishers[data.topic_name] = rospy.Publisher(
                    data.topic_name, butia_listen_msg, queue_size=1)

            log_data_info(LISTEN_NODE_NAME, 'command added: %s' % command)
    log_data_info(LISTEN_NODE_NAME, 'commands_to_find: %s' %
                  str(commands_to_find))
    log_data_info(LISTEN_NODE_NAME, 'publishers: %s' %
                  str(publishers))
    return GENERAL_NODE_RESULTS['processed']


def service_remove_commands_callback(data):
    global commands_to_find
    if data.topic_name in publishers:
        publishers.pop(data.topic_name)
    for command in ast.literal_eval(data.command_list):
        if command:
            command = str.lower(command)
            if command in commands_to_find and \
                            data.topic_name in commands_to_find[command]:
                commands_to_find[command].remove(data.topic_name)
            # if not commands_to_find[command]:
            #     commands_to_find.pop(command)

            log_data_info(LISTEN_NODE_NAME, 'command removed: %s' % command)
    log_data_info(LISTEN_NODE_NAME, 'commands_to_find: %s' %
                  str(commands_to_find))
    log_data_info(LISTEN_NODE_NAME, 'publishers: %s' %
                  str(publishers))
    return GENERAL_NODE_RESULTS['processed']


def service_callback(data):
    global execution_status
    if data.enable:
        if execution_status == EXECUTION_STATUS['disabled']:
            # Start node execution
            log_data_info(LISTEN_NODE_NAME, 'Start')

            global butia_app_listen_topic
            butia_app_listen_topic = rospy.Subscriber(
                LISTEN_APP_TOPIC_NAME, app_listen, listen_callback
            )

            execution_status = EXECUTION_STATUS['enabled']

            log_data_info(LISTEN_NODE_NAME, 'Service enabled')
            return GENERAL_NODE_RESULTS['processing']
        else:
            return GENERAL_NODE_RESULTS['already_running']
    else:
        if execution_status == EXECUTION_STATUS['enabled']:
            execution_status = EXECUTION_STATUS['disabled']
            butia_app_listen_topic.unregister()

        log_data_info(LISTEN_NODE_NAME, 'Service disabled')
        return GENERAL_NODE_RESULTS['disabled']


def listen_callback(data):
    try:
        log_data_info(LISTEN_NODE_NAME, 'listening callback, phrase: %s' %
                      data.phrase)

        # Language processing
        found_commands = [command for command in commands_to_find
                          if command in data.phrase]
        for fw in found_commands:
            for suscriptor_topic in commands_to_find[fw]:
                publish_result(suscriptor_topic, fw)
    except Exception as e:
        if execution_status == EXECUTION_STATUS['enabled']:
            raise e


def publish_result(topic_name, found_command):
    res = butia_listen_msg()
    res.command = found_command

    publishers[topic_name].publish(res)


if __name__ == '__main__':
    rospy.init_node(LISTEN_NODE_NAME, log_level=rospy.INFO)
    log_data_info(LISTEN_NODE_NAME, '------- START ------- ')

    log_data_info(LISTEN_NODE_NAME, 'waiting for a service call')
    rospy.Service(LISTEN_SRV_NAME, butia_listen_service, service_callback)
    rospy.Service(LISTEN_ADD_COMMANDS_SRV_NAME,
                  butia_commands_listen_service,
                  service_add_commands_callback)
    rospy.Service(LISTEN_REMOVE_COMMANDS_SRV_NAME,
                  butia_commands_listen_service,
                  service_remove_commands_callback)

    rospy.spin()
