#!/usr/bin/env python
import rospy

from utils import log_data_info
from layer2.msg import butia_follow_msg, butia_move_msg, butia_search_msg
from layer2.srv import butia_search_service, butia_follow_service, \
    butia_search_serviceRequest
from api_constants import FOLLOW_NODE_NAME, FOLLOW_SRV_NAME, \
    FOLLOW_TOPIC_NAME, GENERAL_NODE_RESULTS, MOVEMENT_DIRECTIONS, TARGETS, \
    SEARCH_SRV_NAME, SEARCH_TOPIC_NAME, EXECUTION_STATUS, \
    MOVE_TOPIC_NAME


IMAGE_TARGET_CENTERED_DELTA = 50
TARGET_MAXIMUM_DETECTED = 100

# create messages that are used to publish feedback/result
search_srv = None
butia_search_topic = None
move_topic = None
publisher = None
execution_status = EXECUTION_STATUS['disabled']
move_direction = MOVEMENT_DIRECTIONS['stop']
return_when_too_close = False


def service_callback(data):
    global execution_status
    if data.enable:
        if execution_status == EXECUTION_STATUS['disabled']:
            # Start node execution
            log_data_info(FOLLOW_NODE_NAME, 'Start')

            # -------------- VALIDATIONS --------------
            if data.target not in TARGETS.values():
                log_data_info(FOLLOW_NODE_NAME, '%s: Aborted because of target:%s' %
                              data.target)
                return GENERAL_NODE_RESULTS['invalid_parameter']

            global return_when_too_close
            return_when_too_close = data.return_when_too_close
            # Search_node
            search_srv_data = butia_search_serviceRequest()
            search_srv_data.enable = True
            search_srv_data.mode = 'STATIC'
            search_srv_data.target = data.target
            res = search_srv(search_srv_data)
            if res:
                global butia_search_topic
                butia_search_topic = rospy.Subscriber(
                    SEARCH_TOPIC_NAME, butia_search_msg, search_callback)

                execution_status = EXECUTION_STATUS['enabled']
                log_data_info(FOLLOW_NODE_NAME, 'Service enabled')
                return GENERAL_NODE_RESULTS['processing']
            return GENERAL_NODE_RESULTS['communication_error']
        else:
            return GENERAL_NODE_RESULTS['already_running']
    else:
        if execution_status == EXECUTION_STATUS['enabled']:
            tear_down()

        log_data_info(FOLLOW_NODE_NAME, 'Service disabled')
        return GENERAL_NODE_RESULTS['disabled']


def search_callback(data):
    try:
        if data.obj_found:
            # FOUND!
            log_data_info(FOLLOW_NODE_NAME, 'following target')
            # Calculate if the target is centered
            screen_center = data.pic_width/2
            screen_center_right = screen_center + IMAGE_TARGET_CENTERED_DELTA
            screen_center_left = screen_center - IMAGE_TARGET_CENTERED_DELTA
            if screen_center_left < data.obj_coord_x < screen_center_right:
                move(MOVEMENT_DIRECTIONS['forward'])
            elif data.obj_coord_x < screen_center_left:
                move(MOVEMENT_DIRECTIONS['left'])
            elif screen_center_right < data.obj_coord_x:
                move(MOVEMENT_DIRECTIONS['right'])

            if return_when_too_close and data.obj_width > TARGET_MAXIMUM_DETECTED:
                tear_down()
                log_data_info(FOLLOW_NODE_NAME, 'Target found by size!')
                res = butia_follow_msg()
                res.status = GENERAL_NODE_RESULTS['found']
                publisher.publish(res)
    except Exception as e:
        if execution_status == EXECUTION_STATUS['enabled']:
            raise e


def move(direction):
    if execution_status == EXECUTION_STATUS['enabled']:
        log_data_info(FOLLOW_NODE_NAME, 'state: MOVING TO %s' %
                      direction)
        move_data = butia_move_msg()
        move_data.direction = direction
        move_topic.publish(move_data)
        global move_direction
        move_direction = direction


def tear_down():
    move(MOVEMENT_DIRECTIONS['stop'])

    global execution_status
    execution_status = EXECUTION_STATUS['disabled']
    butia_search_topic.unregister()

    log_data_info(FOLLOW_NODE_NAME, 'Tear down')

    search_srv_data = butia_search_serviceRequest()
    search_srv_data.enable = False
    search_srv(search_srv_data)


if __name__ == '__main__':
    rospy.init_node(FOLLOW_NODE_NAME, log_level=rospy.INFO)
    log_data_info(FOLLOW_NODE_NAME, '------- START ------- ')

    # -------------- CONNECTIONS --------------
    # BUTIA_MOVE service
    log_data_info(FOLLOW_NODE_NAME, '    waiting for topic: %s' %
                  MOVE_TOPIC_NAME)
    move_topic = rospy.Publisher(MOVE_TOPIC_NAME, butia_move_msg, queue_size=1)

    # BUTIA_SEARCH service
    log_data_info(FOLLOW_NODE_NAME, '    waiting for service: %s' %
                  SEARCH_SRV_NAME)
    rospy.wait_for_service(SEARCH_SRV_NAME)
    log_data_info(FOLLOW_NODE_NAME, 'calling for service: %s' % SEARCH_SRV_NAME)
    search_srv = rospy.ServiceProxy(SEARCH_SRV_NAME, butia_search_service)
    # ------------------------------------------

    log_data_info(FOLLOW_NODE_NAME, 'waiting for a service call')
    rospy.Service(FOLLOW_SRV_NAME, butia_follow_service, service_callback)
    global publisher
    publisher = rospy.Publisher(FOLLOW_TOPIC_NAME, butia_follow_msg, queue_size=1)

    rospy.spin()
