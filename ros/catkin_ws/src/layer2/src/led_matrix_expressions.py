#!/usr/bin/env python
import rospy
import time
from threading import Thread
from layer1.msg import matrix_draw
from layer1.srv import draw_led_matrices, clear_led_matrices, \
    set_matrices_brightness, invert_matrices, rotate_matrices, move_draws, \
    move_draws_vertical, show_message
from api_constants import LED_MATRIX_DRAW_SRV_NAME, \
    LED_MATRIX_CLEAR_SRV_NAME, LED_MATRIX_BRIGHTNESS_SRV_NAME, \
    LED_MATRIX_INVERT_SRV_NAME, LED_MATRIX_ROTATE_SRV_NAME, \
    LED_MATRIX_MOVE_DRAWS_SRV_NAME, LED_MATRIX_MOVE_VERTICAL_DRAWS_SRV_NAME, \
    LED_MATRIX_MESSAGE_SRV_NAME


class LedMatrixExpressions:
    def __init__(self):
        rospy.wait_for_service(LED_MATRIX_DRAW_SRV_NAME)
        rospy.wait_for_service(LED_MATRIX_CLEAR_SRV_NAME)
        rospy.wait_for_service(LED_MATRIX_BRIGHTNESS_SRV_NAME)
        rospy.wait_for_service(LED_MATRIX_INVERT_SRV_NAME)
        rospy.wait_for_service(LED_MATRIX_ROTATE_SRV_NAME)
        rospy.wait_for_service(LED_MATRIX_MOVE_DRAWS_SRV_NAME)
        rospy.wait_for_service(LED_MATRIX_MOVE_VERTICAL_DRAWS_SRV_NAME)
        rospy.wait_for_service(LED_MATRIX_MESSAGE_SRV_NAME)

        self.__drawMatrix = rospy.ServiceProxy(LED_MATRIX_DRAW_SRV_NAME,
                                               draw_led_matrices)
        self.__clearMatrix = rospy.ServiceProxy(LED_MATRIX_CLEAR_SRV_NAME,
                                                clear_led_matrices)
        self.__setBrightness = rospy.ServiceProxy(
            LED_MATRIX_BRIGHTNESS_SRV_NAME, set_matrices_brightness)
        self.__invertMatrices = rospy.ServiceProxy(LED_MATRIX_INVERT_SRV_NAME,
                                                   invert_matrices)
        self.__rotateMatrices = rospy.ServiceProxy(LED_MATRIX_ROTATE_SRV_NAME,
                                                   rotate_matrices)
        self.__moveDraws = rospy.ServiceProxy(LED_MATRIX_MOVE_DRAWS_SRV_NAME,
                                              move_draws)
        self.__moveDrawsVertical = rospy.ServiceProxy(
            LED_MATRIX_MOVE_VERTICAL_DRAWS_SRV_NAME, move_draws_vertical)
        self.__showMessage = rospy.ServiceProxy(
            LED_MATRIX_MESSAGE_SRV_NAME, show_message)

        self.__setBrightness(4)
        self.__newAction = None
        self.__action = None
        self.__active = True
        self.__thread = Thread(target=self.__actionsThread)
        self.__thread.start()

    def __actionsThread(self):
        while self.__active:
            if not self.__newAction is None:
                if not self.__action is None:
                    self.__action["end"] = True
                self.__action = self.__newAction
                self.__newAction = None
            if self.__action is None:
                time.sleep(0.1)
            else:
                # try:
                if (not self.__action is None) and (
                not "started" in self.__action):
                    self.__action["start"]()
                    if not self.__action is None:
                        self.__action["started"] = True
                if (not self.__action is None) and ("loop" in self.__action):
                    self.__action["loop"]()
                else:
                    if not self.__action is None:
                        self.__action["end"] = True
                    self.__action = None

    def __setAction(self, action):
        self.__newAction = action

    def __waitActionEnd(self, action):
        while self.__active and (not "end" in action):
            time.sleep(0.1)

    def __showDraw(self, draw1, draw2):
        draws = []
        draws.append(matrix_draw())
        draws.append(matrix_draw())
        draws[0].type = "DEFAULT"
        draws[0].text = draw1
        draws[1].type = "DEFAULT"
        draws[1].text = draw2
        self.__drawMatrix(draws)

    def __showLetters(self, letter1, letter2):
        draws = []
        draws.append(matrix_draw())
        draws.append(matrix_draw())
        draws[0].type = "LETTER"
        draws[0].text = letter1
        draws[1].type = "LETTER"
        draws[1].text = letter2
        self.__drawMatrix(draws)

    def __blink(self, rangeFrom, rangeTo, seconds):
        for i in range(rangeFrom, rangeTo, 1):
            self.__setBrightness(i)
            time.sleep(seconds)
        for i in range(rangeTo, rangeFrom, -1):
            self.__setBrightness(i)
            time.sleep(seconds)

    def __blinkStep(self, rangeFrom, rangeTo, seconds):
        self.__setBrightness(self.__action["brightness"])
        if self.__action["brightness"] == rangeFrom or self.__action[
            "brightness"] < rangeTo and self.__action["up"]:
            self.__action["brightness"] = self.__action["brightness"] + 1
            self.__action["up"] = True
        else:
            self.__action["brightness"] = self.__action["brightness"] - 1
            self.__action["up"] = False
        time.sleep(seconds)

    def close(self):
        try:
            self.clear()
        except:
            print("")
        self.__active = False
        self.__thread.join()

    def clear(self):
        if not self.__action is None:
            self.__action["end"] = True
            self.__action = None
        self.__clearMatrix()

    def __moveRightStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(4)
        self.__showDraw("row_horizontal_1", "row_horizontal_2")

    def __moveRightLoop(self):
        self.__moveDraws(True, True)
        time.sleep(0.1)

    def moveRight(self):
        action = {
            "start": self.__moveRightStart,
            "loop": self.__moveRightLoop
        }
        self.__setAction(action)

    def __moveLeftStart(self):
        self.__rotateMatrices(180)
        self.__setBrightness(4)
        self.__showDraw("row_horizontal_1", "row_horizontal_2")

    def __moveLeftLoop(self):
        self.__moveDraws(True, True)
        time.sleep(0.1)

    def moveLeft(self):
        action = {
            "start": self.__moveLeftStart,
            "loop": self.__moveLeftLoop
        }
        self.__setAction(action)

    def __moveUpStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(4)
        self.__showDraw("row_vertical_1", "row_vertical_2")

    def __moveUpLoop(self):
        self.__moveDrawsVertical(True)
        time.sleep(0.1)

    def moveUp(self):
        action = {
            "start": self.__moveUpStart,
            "loop": self.__moveUpLoop
        }
        self.__setAction(action)

    def __moveDownStart(self):
        self.__rotateMatrices(180)
        self.__setBrightness(4)
        self.__showDraw("row_vertical_2", "row_vertical_1")

    def __moveDownLoop(self):
        self.__moveDrawsVertical(True)
        time.sleep(0.1)

    def moveDown(self):
        action = {
            "start": self.__moveDownStart,
            "loop": self.__moveDownLoop
        }
        self.__setAction(action)

    def __stopStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(0)
        self.__showDraw("full", "full")
        self.__blink(0, 7, 0.1)
        self.clear()

    def stop(self):
        action = {
            "start": self.__stopStart
        }
        self.__setAction(action)
        self.__waitActionEnd(action)

    def __okStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(0)
        self.__showDraw("ok_1", "ok_2")
        self.__blink(0, 7, 0.1)
        self.clear()

    def ok(self):
        action = {
            "start": self.__okStart
        }
        self.__setAction(action)
        self.__waitActionEnd(action)

    def __badStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(0)
        self.__showDraw("bad_1", "bad_2")
        self.__blink(0, 7, 0.1)
        self.clear()

    def bad(self):
        action = {
            "start": self.__badStart
        }
        self.__setAction(action)
        self.__waitActionEnd(action)

    def __questionStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(0)
        self.__showLetters("?", "?")
        self.__blink(0, 7, 0.1)
        self.clear()

    def question(self):
        action = {
            "start": self.__questionStart
        }
        self.__setAction(action)
        self.__waitActionEnd(action)

    def __happyStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(0)
        self.__showDraw("happy_1", "happy_2")
        self.__blink(0, 7, 0.1)
        self.clear()

    def happy(self):
        action = {
            "start": self.__happyStart
        }
        self.__setAction(action)
        self.__waitActionEnd(action)

    def __sadStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(0)
        self.__showDraw("sad_1", "sad_2")
        self.__blink(0, 7, 0.1)
        self.clear()

    def sad(self):
        action = {
            "start": self.__sadStart
        }
        self.__setAction(action)
        self.__waitActionEnd(action)

    def __loveStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(0)
        self.__showDraw("love_1", "love_2")
        self.__blink(0, 7, 0.1)
        self.clear()

    def love(self):
        action = {
            "start": self.__loveStart
        }
        self.__setAction(action)
        self.__waitActionEnd(action)

    def __searchStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(0)
        self.__showDraw("search_1", "search_2")

    def __searchLoop(self):
        self.__blinkStep(0, 7, 0.1)

    def search(self):
        action = {
            "start": self.__searchStart,
            "loop": self.__searchLoop,
            "brightness": 0,
            "up": True
        }
        self.__setAction(action)

    def __showMessageStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(4)
        self.__showMessage(self.__action["text"], self.__action["delay"])

    def showMessage(self, text, delay=0.05):
        action = {
            "start": self.__showMessageStart,
            "text": text,
            "delay": delay
        }
        self.__setAction(action)
        self.__waitActionEnd(action)

    def __showDrawStart(self):
        self.__rotateMatrices(0)
        self.__setBrightness(4)
        self.__showDraw(self.__action["image1"], self.__action["image2"])

    def showDraw(self, image1, image2):
        action = {
            "start": self.__showDrawStart,
            "image1": image1,
            "image2": image2
        }
        self.__setAction(action)
