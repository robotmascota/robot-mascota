# Node names
FOLLOW_NODE_NAME = 'butia_follow'
SEARCH_NODE_NAME = 'butia_search'
MOVE_NODE_NAME = 'butia_move'
LISTEN_NODE_NAME = 'butia_listen'
GESTURE_RECOGNITION_NODE_NAME = 'butia_gesture_recognition'
CLOSE_DETECTION_NODE_NAME = 'butia_close_detection'
PERSONALITY_NODE_NAME = 'butia_personality'


# Service names
FOLLOW_SRV_NAME = 'butia_follow_srv'
SEARCH_SRV_NAME = 'butia_search_srv'
LISTEN_SRV_NAME = 'butia_listen_srv'
LISTEN_ADD_COMMANDS_SRV_NAME = 'butia_add_commands_listen_srv'
LISTEN_REMOVE_COMMANDS_SRV_NAME = 'butia_remove_commands_listen_srv'
GESTURE_RECOGNITION_SRV_NAME = 'butia_gesture_recognition_srv'
PERSONALITY_SOUND_SRV_NAME = 'butia_personality_sound_srv'
PERSONALITY_IMAGE_SRV_NAME = 'butia_personality_image_srv'
PERSONALITY_CLEAR_SRV_NAME = 'butia_personality_clear_srv'

# Topic names
FOLLOW_TOPIC_NAME = 'butia_follow'
SEARCH_TOPIC_NAME = 'butia_search'
GESTURE_RECOGNITION_TOPIC_NAME = 'butia_gesture_recognition'
CLOSENESS_DETECTION_TOPIC_NAME = 'butia_closeness_detection'
PERSONALITY_HIGH_LOGIC_TOPIC_NAME = 'butia_personality_high_logic'
MOVE_TOPIC_NAME = 'butia_move'
CLOSENESS_TOPIC_NAME = 'butia_closeness'
LISTEN_APP_TOPIC_NAME = 'butia_app_listen_topic'
IMAGE_PROCESSING_TOPIC_NAME = 'butia_tracking'
SERVO_TOPIC_NAME = "butia_servo"


# External services
IMAGE_PROCESSING_SRV_NAME = 'object_detect'
GESTURE_SRV_NAME = 'gesture_detect'
SOUND_PLAY_SRV_NAME = 'butia_play_sound'
SOUND_STOP_SRV_NAME = 'butia_stop_sound'
LED_MATRIX_DRAW_SRV_NAME = 'butia_draw_led_matrices'
LED_MATRIX_CLEAR_SRV_NAME = 'butia_clear_led_matrices'
LED_MATRIX_BRIGHTNESS_SRV_NAME = 'butia_set_led_matrices_brightness'
LED_MATRIX_INVERT_SRV_NAME = 'butia_invert_led_matrices'
LED_MATRIX_ROTATE_SRV_NAME = 'butia_rotate_led_matrices'
LED_MATRIX_MOVE_DRAWS_SRV_NAME = 'butia_move_led_matrices_draws'
LED_MATRIX_MOVE_VERTICAL_DRAWS_SRV_NAME = 'butia_move_led_matrices_vertical_draws'
LED_MATRIX_MESSAGE_SRV_NAME = 'butia_show_message_in_led_matrix'


# Others
SEARCH_MODES = {
    'static': 'STATIC',
    'around': 'AROUND'
}

TARGETS = {
    'face': 'FACE',
    'object': 'OBJECT'
}
VISION_OBJECT_ID = {
    'FACE': 1,
    'OBJECT': 2,
    'GESTURE_1': 3
}
MOVEMENT_DIRECTIONS = {
    'left': 'LEFT',
    'right': 'RIGHT',
    'backward': 'BACKWARD',
    'forward': 'FORWARD',
    'stop': 'STOP'
}
EXECUTION_STATUS = {
    'enabled': 'ENABLED',
    'disabled': 'DISABLED'
}
GENERAL_NODE_RESULTS = {
    'processing': 'PROCESSING',
    'communication_error': 'COMMUNICATION_ERROR',
    'invalid_parameter': 'INVALID_PARAMETER',
    'disabled': 'DISABLED',
    'found': 'FOUND',
    'already_running': 'ALREADY_RUNNING',
    'processed': 'PROCESSED',
    'timeout': 'TIMEOUT'
}
LED_MATRIX_TYPES = {
    'letter': 'LETTER',
    'dots': 'DOTS',
    'default': 'DEFAULT'
}
PERSONALITY_HIGH_LOGIC_MODES = {
    'show_emotion': 'SHOW_EMOTION',
    'show_task': 'SHOW_TASK',
    'show_phrase': 'SHOW_PHRASE'
}
PERSONALITY_EMOTIONS = {
    'happy': 'HAPPY',
    'sad': 'SAD',
    'love': 'LOVE',
    'ok': 'OK',
    'bad': 'BAD',
    'confusion': 'CONFUSION',
    'stop': 'STOP'
}
PERSONALITY_TASKS = {
    'forward': 'FORWARD',
    'back': 'BACK',
    'left': 'LEFT',
    'right': 'RIGHT',
    'searching': 'SEARCHING'
}