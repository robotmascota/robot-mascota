#!/usr/bin/env python
import rospy
from butia_tracking.msg import tracking

from utils import log_data_info
from layer1.srv import gesture_detect
from layer2.srv import butia_gesture_recognition_service
from layer2.msg import butia_gesture_recognition_msg
from api_constants import GESTURE_RECOGNITION_NODE_NAME, \
    GENERAL_NODE_RESULTS, EXECUTION_STATUS, \
    GESTURE_SRV_NAME, IMAGE_PROCESSING_TOPIC_NAME, \
    GESTURE_RECOGNITION_SRV_NAME, GESTURE_RECOGNITION_TOPIC_NAME

# create messages that are used to publish feedback/result
image_processing_srv = None
image_processing_topic = None
input_params = None
publisher = None
execution_status = EXECUTION_STATUS['disabled']


def service_callback(data):
    global execution_status
    if data.enable:
        if execution_status == EXECUTION_STATUS['disabled']:
            # Start node execution
            log_data_info(GESTURE_RECOGNITION_NODE_NAME, 'Start')
            res = image_processing_srv(0)
            if res:
                # Listen to vision topic
                global input_params
                input_params = data
                global image_processing_topic
                image_processing_topic = rospy.Subscriber(
                    IMAGE_PROCESSING_TOPIC_NAME, tracking, vision_callback)

                execution_status = EXECUTION_STATUS['enabled']

                log_data_info(GESTURE_RECOGNITION_NODE_NAME,
                              'Service enabled')
                return GENERAL_NODE_RESULTS['processing']
            return GENERAL_NODE_RESULTS['timeout']
        else:
            return GENERAL_NODE_RESULTS['already_running']
    else:
        if execution_status == EXECUTION_STATUS['enabled']:
            execution_status = EXECUTION_STATUS['disabled']
            image_processing_topic.unregister()

        log_data_info(GESTURE_RECOGNITION_NODE_NAME, 'Service disabled')
        return GENERAL_NODE_RESULTS['disabled']


def vision_callback(data):
    try:
        if data.obj_found:
            log_data_info(GESTURE_RECOGNITION_NODE_NAME, 'state: FOUND')
            publish_result(True)
        else:
            log_data_info(GESTURE_RECOGNITION_NODE_NAME, 'state: NOT FOUND')
            publish_result()
    except Exception as e:
        if execution_status == EXECUTION_STATUS['enabled']:
            raise e


def publish_result(found=False):
    res = butia_gesture_recognition_msg()
    res.gesture_found = found
    publisher.publish(res)


if __name__ == '__main__':
    rospy.init_node(GESTURE_RECOGNITION_NODE_NAME, log_level=rospy.INFO)
    log_data_info(GESTURE_RECOGNITION_NODE_NAME, '------- START ------- ')

    # -------------- CONNECTIONS --------------
    # BUTIA_IMAGE service
    log_data_info(GESTURE_RECOGNITION_NODE_NAME, 'waiting for service: %s' %
                  GESTURE_SRV_NAME)
    rospy.wait_for_service(GESTURE_SRV_NAME)
    log_data_info(GESTURE_RECOGNITION_NODE_NAME, 'calling for service: %s' %
                  GESTURE_SRV_NAME)
    image_processing_srv = rospy.ServiceProxy(
        GESTURE_SRV_NAME, gesture_detect)
    # ------------------------------------------

    log_data_info(GESTURE_RECOGNITION_NODE_NAME, 'waiting for a service call')
    rospy.Service(GESTURE_RECOGNITION_SRV_NAME,
                  butia_gesture_recognition_service,
                  service_callback)
    global publisher
    publisher = rospy.Publisher(
        GESTURE_RECOGNITION_TOPIC_NAME,
        butia_gesture_recognition_msg,
        queue_size=1)
    
    rospy.spin()
