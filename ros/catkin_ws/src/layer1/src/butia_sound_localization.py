#!/usr/bin/env python
import rospy
from api_constants import SOUND_LOCALIZATION_NAME, LOCALIZE_SOUND_SRV_NAME, \
    MOVEMENT_DIRECTIONS
from layer1.srv import localize_sound
from utils import log_data_info
from time import sleep

LOCALIZATION_FILENAME = \
    '/root/robot-mascota/ros/catkin_ws/src/layer1/src/hark/Localization.txt'


def service_callback(data):
    with open(LOCALIZATION_FILENAME, "r") as f:
        f.seek(0, 2)           # Seek @ EOF
        fsize = f.tell()        # Get Size
        sleep(0.5)
        f.seek(max(fsize-5120, 0), 0) # Set pos @ last n chars
        lines = f.readlines()       # Read to end
    lines = reversed(lines[-10:])
    x = None
    y = None
    obj_id = None

    for line in lines:
        if "x=\"" in line and "y=\"" in line and "id=\"" in line:
            x = float(line.split("x=\"")[1].split("\"")[0].replace(',', '.'))
            y = float(line.split("y=\"")[1].split("\"")[0].replace(',', '.'))
            obj_id = line.split("id=\"")[1].split("\"")[0]
            break

    log_data_info(SOUND_LOCALIZATION_NAME, '_____>> y=%s <<_____' % y)
    if y:
        if y == 0:
            return MOVEMENT_DIRECTIONS['forward']
        elif y > 0:
            return MOVEMENT_DIRECTIONS['left']
        else:
            return MOVEMENT_DIRECTIONS['right']
    return ""


if __name__ == "__main__":
    rospy.init_node(SOUND_LOCALIZATION_NAME)
    log_data_info(SOUND_LOCALIZATION_NAME, '------- START ------- ')

    log_data_info(SOUND_LOCALIZATION_NAME, 'waiting for a service call')
    rospy.Service(LOCALIZE_SOUND_SRV_NAME, localize_sound, service_callback)

    rospy.spin()
