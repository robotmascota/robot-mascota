#!/usr/bin/env python
import rospy


class NativeServo:
    def __init__(self, motorsNumber):
        self.base = 77
        self.difference = 50
        self.__motors = []
        for i in range(motorsNumber):
            motor = {
                "duty": open('/sys/devices/platform/pwm-ctrl/duty' + str(i),
                             'w', 0),
                "freq": open('/sys/devices/platform/pwm-ctrl/freq' + str(i),
                             'w', 0),
                "enable": open('/sys/devices/platform/pwm-ctrl/enable' + str(i),
                               'w', 0)
            }
            motor["freq"].write("50")
            motor["enable"].write("1")
            self.__motors.append(motor)
            self.stop(i)

    def __getMotor(self, motorIndex):
        if motorIndex < 0 or motorIndex >= len(self.__motors):
            raise Exception("Motor not found: " + str(motorIndex))
        return self.__motors[motorIndex]

    def getValue(self, motorIndex):
        motor = self.__getMotor(motorIndex)
        return motor.value

    def move(self, motorIndex, value):
        if value > self.base + self.difference:
            value = self.base + self.difference
        elif value < self.base - self.difference:
            value = self.base - self.difference

        motor = self.__getMotor(motorIndex)
        motor["duty"].write(str(value))
        motor["value"] = value

    def stop(self, motorIndex):
        self.move(motorIndex, self.base)

    def close(self):
        for motorIndex in range(len(self.__motors)):
            motor = self.__getMotor(motorIndex)
            self.stop(motorIndex)
            motor["enable"].write("0")
            motor["duty"].close()
            motor["freq"].close()
            motor["enable"].close()
