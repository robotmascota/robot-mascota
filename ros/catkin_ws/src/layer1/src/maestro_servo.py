#!/usr/bin/env python
import rospy
import maestro


class MaestroServo:
    def __init__(self, motorsNumber):
        self.base = 6000
        self.difference = 3000
        self.__motors = []
        self.__maestro = maestro.Controller()
        for i in range(motorsNumber):
            motor = {}
            self.__maestro.setRange(i, self.base - self.difference,
                                    self.base + self.difference)
            self.__motors.append(motor)
            self.stop(i)

    def __getMotor(self, motorIndex):
        if motorIndex < 0 or motorIndex >= len(self.__motors):
            raise Exception("Motor not found: " + str(motorIndex))
        return self.__motors[motorIndex]

    def getValue(self, motorIndex):
        motor = self.__getMotor(motorIndex)
        return motor.value

    def move(self, motorIndex, value):
        if value > self.base + self.difference:
            value = self.base + self.difference
        elif value < self.base - self.difference:
            value = self.base - self.difference

        motor = self.__getMotor(motorIndex)
        self.__maestro.setTarget(motorIndex, value)
        motor["value"] = value

    def stop(self, motorIndex):
        self.move(motorIndex, self.base)

    def close(self):
        for motorIndex in range(len(self.__motors)):
            self.stop(motorIndex)
        self.__maestro.close()
