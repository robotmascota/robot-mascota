#ifndef object_detect_H
#define object_detect_H

class object_detect{

	protected:
		int objectId;
		int objectType;
	
	public:
		object_detect(int pObjectId, int pObjectType);
		void setId(int pObjectId);
		int getId();
		void setType(int pObjectType);
		int getType();

};

#endif
