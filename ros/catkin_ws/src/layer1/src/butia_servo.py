#!/usr/bin/env python
import rospy
from api_constants import SERVO_NODE_NAME, SERVO_TOPIC_NAME
from utils import log_data_info
import native_servo
import maestro_servo
from geometry_msgs.msg import Twist

servo = None


def callback(data):
    val0 = int(servo.base) + int(servo.difference/2 * data.linear.x) - \
           int(servo.difference/2 * data.angular.z)
    val1 = int(servo.base) - int(servo.difference/2 * data.linear.x) - \
           int(servo.difference/2 * data.angular.z)

    servo.move(0, val0)
    servo.move(1, val1)


def butia_servo():
    rospy.init_node(SERVO_NODE_NAME, anonymous=True)
    log_data_info(SERVO_NODE_NAME, '------- START ------- ')

    motors = 2
    global servo
    if rospy.get_param("/butia_servo/servo_mode", "MAESTRO") == "NATIVE":
        servo = native_servo.NativeServo(motors)
    else:
        servo = maestro_servo.MaestroServo(motors)

    rospy.Subscriber(SERVO_TOPIC_NAME, Twist, callback)
    rospy.spin()

    rospy.loginfo("closing")
    servo.close()

if __name__ == '__main__':
    butia_servo()
