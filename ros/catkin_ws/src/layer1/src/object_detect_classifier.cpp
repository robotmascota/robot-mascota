#include "object_detect_classifier.h"
#include "object_detect.h"

object_detect_classifier::object_detect_classifier(int pObjectId, int pObjectType, string pObjectClassifierName):
		object_detect(pObjectId, pObjectType){
    
	classifierPath = pObjectClassifierName;
	// Load the cascade for the object
    if (!objectClassifier.load(pObjectClassifierName)){
    }

}

void object_detect_classifier::setObjectClassifier(CascadeClassifier pObjectClassifier){
	objectClassifier = pObjectClassifier;
}

CascadeClassifier object_detect_classifier::getObjectClassifier(){
	return objectClassifier;
}

string object_detect_classifier::getClassifierPath(){
	return classifierPath;
}
