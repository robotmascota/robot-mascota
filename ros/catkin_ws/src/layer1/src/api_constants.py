# Node names
CLOSENESS_NODE_NAME = 'butia_closeness'
LED_MATRIX_NODE_NAME = 'butia_led_matrix'
SERVO_NODE_NAME = 'butia_servo'
PLAY_SOUND_NODE_NAME = 'butia_play_sound'
SOUND_LOCALIZATION_NAME = 'butia_sound_localization'

# ----- SERVICES -----
DRAW_LED_MATRICES_SRV_NAME = 'butia_draw_led_matrices'
CLEAR_LED_MATRICES_SRV_NAME = 'butia_clear_led_matrices'
SET_LED_MATRICES_SRV_NAME = 'butia_set_led_matrices_brightness'
INVERT_LED_MATRICES_SRV_NAME = 'butia_invert_led_matrices'
ROTATE_LED_MATRICES_SRV_NAME = 'butia_rotate_led_matrices'
MOVE_LED_MATRICES_DRAWS_SRV_NAME = 'butia_move_led_matrices_draws'
MOVE_LED_MATRICES_VERTICAL_DRAWS_SRV_NAME = \
    'butia_move_led_matrices_vertical_draws'
MOVE_SHOW_MESSAGE_IN_LED_MATRIX_SRV_NAME = 'butia_show_message_in_led_matrix'
PLAY_SOUND = 'butia_play_sound'
STOP_SOUND = 'butia_stop_sound'
LOCALIZE_SOUND_SRV_NAME = 'butia_localize_sound'


# ----- TOPICS -----
CLOSENESS_TOPIC_NAME = 'butia_closeness'
SERVO_TOPIC_NAME = "butia_servo"

# Others
MOVEMENT_DIRECTIONS = {
    'left': 'LEFT',
    'right': 'RIGHT',
    'backward': 'BACKWARD',
    'forward': 'FORWARD',
    'stop': 'STOP'
}