#!/usr/bin/env python
import rospy
import pygame
from threading import Thread
from time import sleep
from api_constants import PLAY_SOUND_NODE_NAME, STOP_SOUND, \
    PLAY_SOUND
from layer1.srv import play_soundResponse, stop_soundResponse, play_sound, \
    stop_sound
from utils import log_data_info

global currentFile
global newFile
global running
global default_audios_path
global waitPlaying


def play_sound_function():
    global currentFile
    global newFile
    global running
    global waitPlaying

    playing = False
    newFile = None
    currentFile = None

    while running:
        try:
            if newFile is not None:
                if playing:
                    # close
                    pygame.mixer.music.stop()
                    playing = False
                currentFile = newFile
                newFile = None

            if currentFile is None:
                if playing:
                    # close
                    pygame.mixer.music.stop()
                    playing = False
                    waitPlaying = False
                # Nothing to do
                sleep(0.1)
            else:
                # Load file
                if not playing:
                    pygame.mixer.music.load(currentFile)
                    pygame.mixer.music.play()
                    playing = True
                if pygame.mixer.music.get_busy() == False:
                    # close
                    playing = False
                    waitPlaying = False
                    currentFile = None
        except:
            log_data_info(PLAY_SOUND_NODE_NAME, "Error playing sounds")
            currentFile = None
            playing = False
            waitPlaying = False
    # end
    if playing:
        pygame.mixer.music.stop()


def handle_play_sound(req):
    global default_audios_path
    global newFile
    global waitPlaying
    result = True
    try:
        fileName = req.fileName
        if req.relative:
            fileName = default_audios_path + fileName
        rospy.loginfo("Playing sound %s" % (fileName))
        newFile = fileName

        if req.lock:
            waitPlaying = True
            while waitPlaying:
                sleep(0.1)
    except:
        result = False
    return play_soundResponse(result)


def handle_stop_play(req):
    global currentFile
    result = True
    try:
        rospy.loginfo("Stop playing sounds")
        currentFile = None
    except:
        result = False
    return stop_soundResponse(result)


def play_sound_server():
    global running
    global default_audios_path
    default_audios_path = rospy.get_param(
        "/butia_led_matrix/default_draws_path",
        "/usr/share/butia_mascota/audios")
    running = True
    thread = Thread(target=play_sound_function)
    thread.start()
    rospy.Service(PLAY_SOUND, play_sound, handle_play_sound)
    rospy.Service(STOP_SOUND, stop_sound, handle_stop_play)
    rospy.loginfo("Ready to play sounds")
    rospy.spin()
    running = False
    thread.join()


if __name__ == "__main__":
    rospy.init_node(PLAY_SOUND_NODE_NAME)
    log_data_info(PLAY_SOUND_NODE_NAME, '------- START ------- ')

    pygame.mixer.init()
    play_sound_server()

