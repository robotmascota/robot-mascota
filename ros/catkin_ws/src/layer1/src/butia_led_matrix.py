#!/usr/bin/env python
import rospy
import max7219.led as led
import os
from api_constants import LED_MATRIX_NODE_NAME, \
    DRAW_LED_MATRICES_SRV_NAME, CLEAR_LED_MATRICES_SRV_NAME, \
    SET_LED_MATRICES_SRV_NAME, INVERT_LED_MATRICES_SRV_NAME, \
    ROTATE_LED_MATRICES_SRV_NAME, MOVE_LED_MATRICES_DRAWS_SRV_NAME, \
    MOVE_LED_MATRICES_VERTICAL_DRAWS_SRV_NAME, \
    MOVE_SHOW_MESSAGE_IN_LED_MATRIX_SRV_NAME
from layer1.srv import draw_led_matricesResponse, clear_led_matricesResponse, \
    set_matrices_brightnessResponse, invert_matricesResponse, \
    rotate_matricesResponse, move_drawsResponse, move_draws_verticalResponse, \
    show_messageResponse, draw_led_matrices, clear_led_matrices, \
    set_matrices_brightness, invert_matrices, rotate_matrices, move_draws, \
    move_draws_vertical, show_message
from utils import log_data_info

global default_draws
global matrices
global device
global default_orientation


def rotate_up(self, redraw=True):
    """
    Scrolls the underlying buffer (for all cascaded devices) up one pixel
    """
    for x in range(0, (self.NUM_DIGITS * self._cascaded), 1):
        first_dot = self._buffer[x] & 0x1
        self._buffer[x] = self._buffer[x] >> 1
        if first_dot:
            self._buffer[x] |= (1 << 0x7)
        else:
            self._buffer[x] &= ~(1 << 0x7)

    if redraw:
        self.flush()


def DrawMatrixDots(matrixNumber, dots):
    for i in range(64):
        x = i % 8
        y = i // 8
        device.pixel(x + matrixNumber * 8, y, dots[i] and 1 or 0, False)
    device.flush()


def DrawMatrix(matrixNumber, draw):
    global device
    global default_draws
    if draw.type == "LETTER":
        device.letter(matrixNumber, ord(draw.text))
    elif draw.type == "DEFAULT":
        if not draw.text in default_draws:
            raise ValueError("Draw " + draw.text + " not found")
        DrawMatrixDots(matrixNumber, default_draws[draw.text])
    elif draw.type == "DOTS":
        DrawMatrixDots(matrixNumber, draw.dots)
    else:
        device.clear(matrixNumber)


def handleDrawMatrices(req):
    result = True
    try:
        for matrixNumber in range(len(req.draws)):
            DrawMatrix(matrixNumber, req.draws[matrixNumber])
    except Exception as e:
        rospy.logdebug("Error drawing matrices - " + str(e))
        result = False
    return draw_led_matricesResponse(result)


def handleClearMatrices(req):
    result = True
    try:
        device.clear()
    except Exception as e:
        rospy.logdebug("Error clearing matrices - " + str(e))
        result = False
    return clear_led_matricesResponse(result)


def handleSetMatricesBrightness(req):
    result = True
    try:
        brightness = req.brightness
        if brightness < 0:
            brightness = 0
        elif brightness > 16:
            brightness = 16
        device.brightness(brightness)
    except Exception as e:
        rospy.logdebug("Error setting matrices brightness - " + str(e))
        result = False
    return set_matrices_brightnessResponse(result)


def handleInvertMatrices(req):
    result = True
    try:
        device.invert(req.invert)
    except Exception as e:
        rospy.logdebug("Error inverting matrices - " + str(e))
        result = False
    return invert_matricesResponse(result)


def handleRotateMatrices(req):
    global default_orientation
    result = True
    try:
        angle = (req.angle + default_orientation) % 360
        device.orientation(angle)
    except Exception as e:
        rospy.logdebug("Error rotating matrices - " + str(e))
        result = False
    return rotate_matricesResponse(result)


def handleMoveDraws(req):
    result = True
    try:
        if not req.right:
            if req.rotate:
                device.rotate_left()
            else:
                device.scroll_left()
        else:
            if req.rotate:
                device.rotate_right()
            else:
                device.scroll_right()
    except Exception as e:
        rospy.logdebug("Error moving matrices draws - " + str(e))
        result = False
    return move_drawsResponse(result)


def handleMoveDrawsVertical(req):
    result = True
    try:
        if req.rotate:
            rotate_up(device)
        else:
            device.scroll_up()
    except Exception as e:
        rospy.logdebug("Error moving matrices draws vertical - " + str(e))
        result = False
    return move_draws_verticalResponse(result)


def handleShowMessage(req):
    result = True
    try:
        device.clear()
        device.show_message(text=req.message, delay=req.delay)
    except Exception as e:
        rospy.logdebug("Error showing message in matrices - " + str(e))
        result = False
    return show_messageResponse(result)


def LoadDefaultDraws():
    global default_draws
    path = rospy.get_param("/butia_led_matrix/default_draws_path",
                           "/home/ubuntu/robot-mascota/led_matrix/default_draws")
    try:
        default_draws = {}
        files = os.listdir(path)
        for fileName in files:
            draw = []
            with open(path + "/" + fileName) as f:
                content = f.readlines()
                if len(content) != 8:
                    raise ValueError(
                        "File " + fileName + " has invalid number of lines")
                for line in content:
                    line_a = line.split("\t")
                    if len(line_a) != 8:
                        raise ValueError(
                            "File " + fileName + " has invalid lines")
                    for dot in line_a:
                        draw.append(dot == "X" or dot == "X\n")
            default_draws[fileName] = draw
    except Exception as e:
        rospy.logdebug("Error loading default draws from " + path + " - " +
                       str(e))


def LedMatrixServer():
    global matrices
    global device
    global default_orientation

    matrices = int(rospy.get_param("/butia_led_matrix/matrices", "2"))
    device = led.matrix(cascaded=matrices, )
    default_orientation = int(
        rospy.get_param("/butia_led_matrix/default_orientation", "270"))
    device.orientation(default_orientation)

    rospy.Service(DRAW_LED_MATRICES_SRV_NAME, draw_led_matrices,
                  handleDrawMatrices)
    rospy.Service(CLEAR_LED_MATRICES_SRV_NAME, clear_led_matrices,
                  handleClearMatrices)
    rospy.Service(SET_LED_MATRICES_SRV_NAME, set_matrices_brightness,
                  handleSetMatricesBrightness)
    rospy.Service(INVERT_LED_MATRICES_SRV_NAME, invert_matrices,
                  handleInvertMatrices)
    rospy.Service(ROTATE_LED_MATRICES_SRV_NAME, rotate_matrices,
                  handleRotateMatrices)
    rospy.Service(MOVE_LED_MATRICES_DRAWS_SRV_NAME, move_draws, handleMoveDraws)
    rospy.Service(MOVE_LED_MATRICES_VERTICAL_DRAWS_SRV_NAME,
                  move_draws_vertical, handleMoveDrawsVertical)
    rospy.Service(MOVE_SHOW_MESSAGE_IN_LED_MATRIX_SRV_NAME, show_message,
                  handleShowMessage)
    log_data_info(LED_MATRIX_NODE_NAME, "Ready to show matrices")
    rospy.spin()
    device.clear()


if __name__ == "__main__":
    rospy.init_node(LED_MATRIX_NODE_NAME)
    log_data_info(LED_MATRIX_NODE_NAME, '------- START ------- ')

    LoadDefaultDraws()
    LedMatrixServer()
