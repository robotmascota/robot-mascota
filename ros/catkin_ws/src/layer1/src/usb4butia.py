#!/usr/bin/env python
#usb4butia
import sys
sys.path.insert(0, '/usr/share/sugar/activities/TurtleBots.activity/plugins/butia')
from pybot import usb4butia

class Usb4Butia:
    def __init__(self):
        self.robot = usb4butia.USB4Butia()
        self.SHARP = "distanc"
        self.SHARP_METERS = [
            { "distance" : 35000, "meters" : 0.10 },
            { "distance" : 45000, "meters" : 0.15 },
            { "distance" : 50000, "meters" : 0.20 },
            { "distance" : 52000, "meters" : 0.25 },
            { "distance" : 54000, "meters" : 0.30 }
        ]

    def getModules(self, moduleName):
        modules = []
        allModules = self.robot.getModulesList()
        for module in allModules:
            module_a = module.split(":")
            if len(module_a) != 2 or module_a[0] != moduleName:
                continue
            modules.append(int(module_a[1]))
        return modules

    def getSharpDistance(self, portNumber):
        dist = self.robot.getDistance(portNumber)
        return dist

    def sharpDistanceToMeters(self, distance):
        meters = 0.10
        for sharp in self.SHARP_METERS:
            if sharp["distance"] > distance:
                break
            meters = sharp["meters"]
        return meters

