#include "ros/ros.h"
#include "butia_vision/vision.h"
#include "butia_tracking/tracking.h"
#include <iostream>
#include <list>
#include "object_detected.h"
#include <exception>


using namespace std;
using namespace ros;
using namespace butia_tracking;


Publisher publisher;
//Rate loop_rate(10);
const static int __DELTA = 15;
int objectUUID = 1;
int imageID = -1;
//lista de listas conteniendo las ultimas N posiciones de todos los objetos detectados
list <object_detected> objectsNewImage;
list <object_detected> objectsPublish;
bool firstPublish;
const static int __GEST_SIZE = 3;
const static int __OBJ_SIZE = 5;
const static int __MAX_DISTANCE = 1000;
int msgObjNotFound = 0;
bool objNotFoundPublished = false;
bool firstGesture = true;


void publishObjNotFound(){
	// Create message
	tracking msg;
	msg.obj_found = false;

	// Publish message
	publisher.publish(msg);
	ros::spinOnce();
}

void publishObjCoord(list <object_detected> objectsPublish){

	//ROS_INFO("Object to publish %zd", objectsPublish.size());
	for (int index = 0; index < objectsPublish.size(); index++){


		int publishCount = __OBJ_SIZE;
		if ((*std::next(std::begin(objectsPublish), index)).getObjType() == -3){
			publishCount = __GEST_SIZE;
		}


		if ((*std::next(std::begin(objectsPublish), index)).getLastImage() > publishCount){

			// Create message
			tracking msg;
			msg.object_id = (*std::next(std::begin(objectsPublish), index)).getUUID();
			msg.pic_height = (*std::next(std::begin(objectsPublish), index)).getPicHeight();
			msg.pic_width = (*std::next(std::begin(objectsPublish), index)).getPicWidth();
			msg.obj_found = true;
			msg.obj_type = (*std::next(std::begin(objectsPublish), index)).getObjType();
			msg.obj_height = (*std::next(std::begin(objectsPublish), index)).getObjHeight();
			msg.obj_width = (*std::next(std::begin(objectsPublish), index)).getObjWidth();
			msg.obj_coord_x = (*std::next(std::begin(objectsPublish), index)).getObjCoordX();
			msg.obj_coord_y = (*std::next(std::begin(objectsPublish), index)).getObjCoordY();

			// Publish message
			publisher.publish(msg);
			ros::spinOnce();
//			loop_rate.sleep();
		}

	}
}

void imageLocationCallback(const butia_vision::vision::ConstPtr& imageLocationMsg){

	if (!imageLocationMsg->obj_found){
		//ROS_INFO("OBJ NOT FOUND");
		if (msgObjNotFound > __OBJ_SIZE){
			if (!objNotFoundPublished){
				publishObjNotFound();
				objNotFoundPublished = true;
				objectsPublish.clear();
			}
		}else{
			msgObjNotFound++;
		}
		return;
	}

	if (imageLocationMsg->obj_type == -3){
		if (firstGesture){
			firstGesture = false;
			objectsPublish.clear();
		}
	}else{
		firstGesture = true;
	}

	object_detected* newObject = new object_detected(-1, imageLocationMsg->pic_height, imageLocationMsg->pic_width, imageLocationMsg->obj_type, imageLocationMsg->obj_height, imageLocationMsg->obj_width, imageLocationMsg->obj_coord_x, imageLocationMsg->obj_coord_y);

	if (imageLocationMsg->image_id != imageID){ //new image

		if (imageID != -1){
			firstPublish = false;
		}

		if (firstPublish){ //first new image
			newObject->setUUID(objectUUID);
			objectUUID++;
			objectsPublish.push_front(*newObject);
		}else{ // classified objects from the last image and publish objects coordenates

			int index = 0;
			double eucDist = -1.0;
			double distanceMatrix[objectsNewImage.size()][objectsPublish.size()];
			int row, column;
			int rowIndex, columnIndex, indexImage;
			list <object_detected> objectsPublishAux;

			if (objectsNewImage.size() == 0){
				//publish all objects from objectsPublish
				publishObjCoord(objectsPublish);
			}else{

				int lastIndexImage = objectsNewImage.size();
				if (lastIndexImage > objectsPublish.size()){
					lastIndexImage = objectsPublish.size();
				}

				for (rowIndex = 0; rowIndex < objectsNewImage.size(); rowIndex++){
					//For each new object calculate the distance of the center with previous object detected
					for (columnIndex = 0; columnIndex < objectsPublish.size(); columnIndex++){
						//get the last position saved
						object_detected objNI = *std::next(std::begin(objectsNewImage), rowIndex);
						object_detected objP = *std::next(std::begin(objectsPublish), columnIndex);
						//euclidean distance
						eucDist = sqrt(pow((objP.getObjCoordX() - objNI.getObjCoordX()), 2) + pow((objP.getObjCoordY() - objNI.getObjCoordY()), 2));
						distanceMatrix[rowIndex][columnIndex] = eucDist; //store the distances
					}
				}

				for (indexImage = 0; indexImage < lastIndexImage; indexImage++){

					eucDist = __MAX_DISTANCE;
					for (rowIndex = 0; rowIndex < objectsNewImage.size(); rowIndex++){
						if (eucDist > distanceMatrix[rowIndex][0]){
							eucDist = distanceMatrix[rowIndex][0];
							row = rowIndex;
							column = 0;
						}

						for (columnIndex = 1; columnIndex < objectsPublish.size(); columnIndex++){
							if (eucDist > distanceMatrix[rowIndex][columnIndex]){
								eucDist = distanceMatrix[rowIndex][columnIndex];
								row = rowIndex;
								column = columnIndex;
							}
						}

					}

//					ROS_INFO("DIST: %f, ROW: %i, COLUMN: %i", eucDist, row, column);
					for (rowIndex = 0; rowIndex < objectsNewImage.size(); rowIndex++){
						distanceMatrix[rowIndex][column] = __MAX_DISTANCE;
					}
					for (columnIndex = 0; columnIndex < objectsPublish.size(); columnIndex++){
						distanceMatrix[row][columnIndex] = __MAX_DISTANCE;
					}

//					ROS_INFO("Adding object to collection %i", (*std::next(std::begin(objectsPublish), column)).getUUID());
					(*std::next(std::begin(objectsNewImage), row)).setUUID((*std::next(std::begin(objectsPublish), column)).getUUID());
					(*std::next(std::begin(objectsNewImage), row)).setLastImage((*std::next(std::begin(objectsPublish), column)).getLastImage()+1);

					objectsPublishAux.push_front((*std::next(std::begin(objectsNewImage), row)));

				}

				objectsPublish = objectsPublishAux;

			}
/*
			ROS_INFO("--- AUX COLLECTION --- ");
			for (int t = 0; t < objectsPublishAux.size(); t++){
				ROS_INFO("Object added to collection %i", (*std::next(std::begin(objectsPublishAux), t)).getUUID());
			}
			ROS_INFO("--- PUB COLLECTION --- ");
			for (int t = 0; t < objectsPublish.size(); t++){
				ROS_INFO("Object added to collection %i", (*std::next(std::begin(objectsPublish), t)).getUUID());
			}
*/
			for (rowIndex = 0; rowIndex < objectsNewImage.size(); rowIndex++){
				//For each new object check if correspond with a previous object detected
				object_detected objD = *std::next(std::begin(objectsNewImage), rowIndex);

				if (objD.getUUID() == -1){ //Doesnt correspond with a previous object, its a new object
					objD.setUUID(objectUUID); //Set the ID for the new object
					objectUUID++;
					objectsPublish.push_front(objD); //Add the new object to publish
				}
			}

			//Publish all objects
			publishObjCoord(objectsPublish);

			//Add new object
			objectsNewImage.clear();
			objectsNewImage.push_back(*newObject);
/*
			ROS_INFO("OBJECTS TO PUBLISH: %zd", objectsPublish.size());
			for (columnIndex = 0; columnIndex < objectsPublish.size(); columnIndex++){
				ROS_INFO("OBEJCT ID: %i", (*std::next(std::begin(objectsPublish), columnIndex)).getUUID());
			}
*/
		}
		imageID = imageLocationMsg->image_id;
	}else{ //in the same picture another object has been detected
		if (firstPublish){ //another object in the first picture
			newObject->setUUID(objectUUID);
			objectUUID++;
			objectsPublish.push_back(*newObject);
		}else{
			objectsNewImage.push_back(*newObject);
		}
	}

	spinOnce();
}

int main(int argc, char **argv){
	init(argc, argv, "imageTracking");
	NodeHandle n;
	firstPublish = true;
	ROS_INFO("Creating Publisher Tracking");
	publisher = n.advertise<tracking>("butia_tracking", 1000);
	Subscriber sub = n.subscribe("butia_vision", 1000, imageLocationCallback);
	spin();
	return 0;
}
