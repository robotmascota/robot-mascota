#include "object_detected.h"

object_detected::object_detected(int pUuid, int pPic_height, int pPic_width, int pObj_type, int pObj_height, int pObj_width, int pObj_coord_x, int pObj_coord_y){
	uuid = pUuid;
	lastImage = 1;
	pic_height = pPic_height;
	pic_width = pPic_width;
	obj_type = pObj_type;
	obj_height = pObj_height;
	obj_width = pObj_width;
	obj_coord_x = pObj_coord_x;
	obj_coord_y = pObj_coord_y;
}

void object_detected::setUUID(int pUUID){
	uuid = pUUID;
}

void object_detected::setLastImage(int pLastImage){
	lastImage = pLastImage;
}

void object_detected::setPicHeight(int pPic_height){
	pic_height = pPic_height;
}

void object_detected::setPicWidth(int pPic_width){
	pic_width = pPic_width;
}

void object_detected::setObjType(int pObj_type){
	obj_type = pObj_type;
}

void object_detected::setObjHeight(int pObj_height){
	obj_height = pObj_height;
}

void object_detected::setObjWidth(int pObj_width){
	obj_width = pObj_width;
}

void object_detected::setObjCoordX(int pObj_coord_x){
	obj_coord_x = pObj_coord_x;
}

void object_detected::setObjCoordY(int pObj_coord_y){
	obj_coord_y = pObj_coord_y;
}

int object_detected::getUUID(){
	return uuid;
}

int object_detected::getLastImage(){
	return lastImage;
}

int object_detected::getPicHeight(){
	return pic_height;
}

int object_detected::getPicWidth(){
	return pic_width;
}

int object_detected::getObjType(){
	return obj_type;
}

int object_detected::getObjHeight(){
	return obj_height;
}

int object_detected::getObjWidth(){
	return obj_width;
}

int object_detected::getObjCoordX(){
	return obj_coord_x;
}

int object_detected::getObjCoordY(){
	return obj_coord_y;
}

