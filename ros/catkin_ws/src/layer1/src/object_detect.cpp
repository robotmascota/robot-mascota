#include "object_detect.h"

object_detect::object_detect(int pObjectId, int pObjectType){
	objectId = pObjectId;
	objectType = pObjectType;
}

void object_detect::setId(int pObjectId){
	objectId = pObjectId;
}

int object_detect::getId(){
	return objectId;
}

void object_detect::setType(int pObjectType){
	objectType = pObjectType;
}

int object_detect::getType(){
	return objectType;
}

