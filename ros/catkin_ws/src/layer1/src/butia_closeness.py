#!/usr/bin/env python
import rospy
from api_constants import CLOSENESS_NODE_NAME, CLOSENESS_TOPIC_NAME
import usb4butia
from layer1.msg import sharp_distance_array, sharp_distance
from utils import log_data_info

TOPIC_FRECUENCY_HZ = 2


def publishTopic():
    pub = rospy.Publisher(CLOSENESS_TOPIC_NAME, sharp_distance_array,
                          queue_size=1)
    rospy.init_node(CLOSENESS_NODE_NAME, anonymous=True)
    log_data_info(CLOSENESS_NODE_NAME, '------- START ------- ')

    rate = rospy.Rate(TOPIC_FRECUENCY_HZ)
    butia = usb4butia.Usb4Butia()
    while not rospy.is_shutdown():
        distances = sharp_distance_array()
        modules = butia.getModules(butia.SHARP)
        for portNumber in modules:
            distance = sharp_distance()
            distance.portNumber = portNumber
            distance.sensorDistance = butia.getSharpDistance(portNumber)
            distance.metersDistance = butia.sharpDistanceToMeters(
                distance.sensorDistance)
            distances.sensors.append(distance)
        pub.publish(distances)
        rate.sleep()
if __name__ == '__main__':
    try:
        publishTopic()
    except rospy.ROSInterruptException:
        pass
