#ifndef object_detected_H
#define object_detected_H

class object_detected{

	private:
		int uuid;
		int lastImage;
		int pic_height;
		int pic_width;
		int obj_type;
		int obj_height;
		int obj_width;
		int obj_coord_x;
		int obj_coord_y;

	public:
		object_detected(int pUuid, int pPic_height, int pPic_width, int pObj_type, int pObj_height, int pObj_width, int pObj_coord_x, int pObj_coord_y);
		void setUUID(int pUUID);
		void setLastImage(int pLastImage);
		void setPicHeight(int pPic_height);
		void setPicWidth(int pPic_width);
		void setObjType(int pObj_type);
		void setObjHeight(int pObj_height);
		void setObjWidth(int pObj_width);
		void setObjCoordX(int pObj_coord_x);
		void setObjCoordY(int pObj_coord_y);
		int getUUID();
		int getLastImage();
		int getPicHeight();
		int getPicWidth();
		int getObjType();
		int getObjHeight();
		int getObjWidth();
		int getObjCoordX();
		int getObjCoordY();

};

#endif
