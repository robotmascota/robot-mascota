#include "ros/ros.h"
#include "butia_vision/vision.h"
#include "butia_vision/simple_object_detect.h"
#include "butia_vision/gesture_detect.h"
#include "butia_vision/capture_send.h"
#include "butia_vision/calibration_start.h"
#include "butia_vision/calibration_ok.h"
#include "std_msgs/String.h"
//#include "butia_vision/ColorConfig.h"
//#include <dynamic_reconfigure/server.h>
#include <exception>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include <exception>
#include "opencv/cv.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "object_detect.h"
#include "object_detect_color_shape.h"
#include "object_detect_classifier.h"
#include <unistd.h>
#include <thread>
#define GetCurrentDir getcwd


using namespace std;
using namespace cv;
using namespace butia_vision;
using namespace ros;


const static int __DELTA = 15;
const static int __AREA_DISCARD = 500;
const static int __OBJ_RECOGNITION = 5; //same value as __OBJ_SIZE from tracking.cpp
const static char* __PREFIX_DEBUG = "DEBUG";
const static char* __DEBUG_ACTIVE = "ON";
static const string base64_chars =
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";
// File with the configuration off all objects
string pathConfigurationFile = "/root/robot-mascota/ros/catkin_ws/src/layer1/src/motion.config";
// Capture the video from webcam
VideoCapture cap(0);
// id of the new object to detect
int idNewObject = -1;
// indicates if there's a new object to detect
bool newsimple_object_detection;
// stores configuration for all the objects to detect
map<int, object_detect*> objects;
int lowH, highH, lowS, highS, lowV, highV;
double gCalibLowH, gCalibHighH, gCalibLowS, gCalibHighS, gCalibLowV, gCalibHighV;
bool lastImageObjectFound = false;
bool detectHandGestures = false;
//Region of intrest to detect gesture
int roiTop = -1;
int roiHeight = -1;
int roiEdge = -1;
bool publishImage = false;
enum recognitionStates {none, objectDetect, gestureDetect, calibration};
recognitionStates state;


/*****************************************************/
string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len) {
	string ret;
	int i = 0;
	int j = 0;
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];

	while (in_len--) {
		char_array_3[i++] = *(bytes_to_encode++);
		if (i == 3){
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for(i = 0; (i < 4) ; i++)
				ret += base64_chars[char_array_4[i]];
			i = 0;
		}
	}

	if (i){
		for(j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (j = 0; (j < i + 1); j++)
			ret += base64_chars[char_array_4[j]];

		while((i++ < 3))
			ret += '=';

	}

	return ret;

}
/*****************************************************/

/**
 * Change the object to detect once there is a call to the service
 */
bool detectObject(simple_object_detect::Request &req, simple_object_detect::Response &res){

	state = objectDetect;

	roiTop = -1;
	roiHeight = -1;
	roiEdge = -1;

	if (req.object_id != idNewObject){
		idNewObject = req.object_id;
		res.success = true;
		ROS_INFO("Changing object to detect: [%d]", idNewObject);
		newsimple_object_detection = true;
	}

	return true;

}

/*
//Change the object to detect once there is a new message
void objectToDetectCallback(const brain::objectToDetect::ConstPtr& objToDetect){

	if (objToDetect->object_id != idNewObject){
		mtx.lock();
		ROS_INFO("Changing object to detect: [%d]", objToDetect->object_id);
		idNewObject = objToDetect->object_id;
		newsimple_object_detection = true;
		mtx.unlock();
//	}

	ros::spinOnce();
}
*/

/**
 * Change to detect a gesture
 */
bool detectGesture(gesture_detect::Request &req, gesture_detect::Response &res){
	state = gestureDetect;
	res.success = true;
	return true;
}

bool startCalibration(calibration_start::Request &req, calibration_start::Response &res){
	ROS_INFO("Calibration requested");
	state = calibration;
	res.success = true;
	return true;
}

bool saveCalibration(calibration_ok::Request &req, calibration_ok::Response &res){

	cout << "Calibration finished\n";

	int pIdNewObject = 2;

	cout << "ID object " << pIdNewObject << "\n";
	try{
		object_detect* obj = objects.at(pIdNewObject);
		// pIdNewObject not specified in configuration file
		if (!obj){
			cout << "Id NOT FOUND.\n";
			return false;
		}else{
			cout << "Object FOUND.\n";
		}

		switch (obj->getType()){

			case 1:
				object_detect_color_shape* objCS;
				objCS = static_cast<object_detect_color_shape*>(objects.at(pIdNewObject));

				//ROS_INFO("Saving calib %0.3f, %0.3f, %0.3f, %0.3f, %0.3f, %0.3f", gCalibLowH, gCalibHighH, gCalibLowS, gCalibHighS, gCalibLowV, gCalibHighV);

				objCS->setLowH(gCalibLowH);
				objCS->setHighH(gCalibHighH);
				objCS->setLowS(gCalibLowS);
				objCS->setHighS(gCalibHighS);
				objCS->setLowV(gCalibLowV);
				objCS->setHighV(gCalibHighV);

				newsimple_object_detection = true;

			break;

		}
	}catch(exception& ex){
//		ROS_INFO(ex.what().c_str());
		cout << ex.what() << endl;
	}

	//save the new values to configuration

	ofstream configFile;
	configFile.open (pathConfigurationFile);
	configFile << "DEBUG=ON";

	for(map<int, object_detect*>::iterator it = objects.begin(); it != objects.end(); ++it){
		configFile << "\n" << it->second->getType();
		switch (it->second->getType()){

			case 1:
				object_detect_color_shape* objCS;
				objCS = static_cast<object_detect_color_shape*>(it->second);

				configFile << "\n" << objCS->getId() << " " << objCS->getLowH() << " " << objCS->getHighH() << " "<< objCS->getLowS() << " " << objCS->getHighS() << " " << objCS->getLowV() << " "<< objCS->getHighV() << " "<< objCS->getShape();

			break;

			case 2:
				object_detect_classifier* objC;
				objC = static_cast<object_detect_classifier*>(it->second);

				configFile << "\n" << objC->getId() << " " << objC->getClassifierPath();

			break;
		}


	}
	configFile.close();

	state = none;
	res.success = true;
	return true;
}

string encode(Mat frame){

	vector<uchar> buf;

	imencode(".jpg", frame, buf);
	uchar *enc_msg = new uchar[buf.size()];

	for(int i=0; i < buf.size(); i++){
		enc_msg[i] = buf[i];
	}

	return base64_encode(enc_msg, buf.size());
}

/**
 * Service that return the current capture from the web cam
 */
bool sendImageCap(capture_send::Request &req, capture_send::Response &res){

//	ROS_INFO("CAPTURE REQUESTED");

	Mat frame;
	cap >> frame;

	res.encoded = encode(frame);
	return true;
}

/**
 * Helper function to find a cosine of angle between vectors
 * from pt0->pt1 and pt0->pt2
 * use to determine the shape of an object
 */
static double angle(Point pt1, Point pt2, Point pt0){

	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;

	return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);

}

/**
 * Helper function that returns the square of the euclidean distance between 2 points.
 */
double dist(Point x, Point y){
	return (x.x-y.x)*(x.x-y.x)+(x.y-y.y)*(x.y-y.y);
}

/**
 * Helper function that returns the radius and the center of the circle given 3 points
 * If a circle cannot be formed , it returns a zero radius circle centered at (0,0)
 */
pair<Point,double> circleFromPoints(Point p1, Point p2, Point p3){

	double offset = pow(p2.x,2) +pow(p2.y,2);
	double bc =   ( pow(p1.x,2) + pow(p1.y,2) - offset )/2.0;
	double cd =   (offset - pow(p3.x, 2) - pow(p3.y, 2))/2.0;
	double det =  (p1.x - p2.x) * (p2.y - p3.y) - (p2.x - p3.x)* (p1.y - p2.y);
	double TOL = 0.0000001;
	if (abs(det) < TOL) {
		return make_pair(Point(0,0),0);
	}

	double idet = 1/det;
	double centerx =  (bc * (p2.y - p3.y) - cd * (p1.y - p2.y)) * idet;
	double centery =  (cd * (p1.x - p2.x) - bc * (p2.x - p3.x)) * idet;
	double radius = sqrt( pow(p2.x - centerx,2) + pow(p2.y-centery,2));

	return make_pair(Point(centerx,centery),radius);
}

/**
 * Helper function to transform from int to string
 */
string intToString(int number){
	stringstream ss;
	ss << number;
	return ss.str();
}

/**
 * Helper function to display text in the center of a contour
 */
void setLabel(Mat& im, const string label, vector<Point>& contour){

	int fontface = FONT_HERSHEY_SIMPLEX;
	double scale = 0.4;
	int thickness = 1;
	int baseline = 0;

	Size text = getTextSize(label, fontface, scale, thickness, &baseline);
	Rect r = boundingRect(contour);

	Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
	rectangle(im, pt + Point(0, baseline), pt + Point(text.width, -text.height), CV_RGB(255,255,255), CV_FILLED);
	putText(im, label, pt, fontface, scale, CV_RGB(0, 0, 0), thickness, 8);
}

/**
 * Load all objects configuration from file
 */
bool loadConfig(string pPathConfigurationFile){

//	ROS_INFO("Load configuration called.");

//	ROS_INFO("%s", pPathConfigurationFile.c_str());
	ifstream configFile (pPathConfigurationFile);
	if (!configFile.is_open()){
		ROS_INFO("Configuration file not found.");
		exit(-1);
	}

//	ROS_INFO("Loading configuration.");

	char* fileLine;
	int objectType, objectId;
	int lowH, highH, lowS, highS, lowV, highV;
	int shape;
	string classifierPath;

	while (configFile >> fileLine){

		cout << fileLine << "\n";
		ROS_INFO("FILE LINE: %s", fileLine);

		try{
			objectType = stoi(fileLine);
		}catch(exception ex){
			objectType = -1;
			if (!strncmp(fileLine, __PREFIX_DEBUG, strlen(__PREFIX_DEBUG))){ // 0 if equals
				char* lineDebug = strtok(fileLine, "="); //DEBUG
				lineDebug = strtok(NULL, "="); //ON or OFF
				if (!strncmp(lineDebug, __DEBUG_ACTIVE, strlen(__DEBUG_ACTIVE))){
					publishImage = true;
				}
			}
		}

		if (objectType == 1){ // object defined by color shape
			configFile >> objectId >> lowH >> highH >> lowS >> highS >> lowV >> highV >> shape;
cout << "Object id: " << objectId << "\n";
			object_detect_color_shape* objCS = new object_detect_color_shape(objectId, objectType, lowH, highH, lowS, highS, lowV, highV, shape);
			objects.insert(pair<int,object_detect_color_shape*>(objectId, objCS));
		}else if (objectType == 2){ // object defined by classifier
			configFile >> objectId >> classifierPath;
cout << "Object id: " << objectId << "\n";
			object_detect_classifier* objC = new object_detect_classifier(objectId, objectType, classifierPath);
			objects.insert(pair<int,object_detect_classifier*>(objectId, objC));
		}
	}

	configFile.close();
	cout << "Configuration loaded.\n";
	cout << "LOADED " << objects.size() << "  OBJECTS DEFINITIONS.\n";

	newsimple_object_detection = false;

	return true;
}

/**
 * Load specific configuration for object equals pIdNewObject
 */
bool loadObjectConfig(int pIdNewObject, int& pLowH, int& pHighH, int& pLowS, int& pHighS, int& pLowV, int& pHighV,
	bool& pByColor, int& pColorConversion, int& pShape, bool& pByShape, CascadeClassifier& objectClassifier){

	object_detect* obj = objects.at(pIdNewObject);
	// pIdNewObject not specified in configuration file
	if (!obj){
		ROS_INFO("Id NOT FOUND.");
		return false;
	}

	switch (obj->getType()){

		case 1:
			object_detect_color_shape* objCS;
			objCS = static_cast<object_detect_color_shape*>(objects.at(pIdNewObject));

			pLowH = objCS->getLowH();
			pHighH = objCS->getHighH();

			pLowS = objCS->getLowS();
			pHighS = objCS->getHighS();

			pLowV = objCS->getLowV();
			pHighV = objCS->getHighV();

			pByColor = objCS->isByColor();
			if (pByColor){
				pColorConversion = COLOR_BGR2HSV;
			}else{
				pColorConversion = CV_BGR2GRAY;
			}

			pShape = objCS->getShape();

			pByShape = objCS->isByShape();
		break;

		case 2:
			object_detect_classifier* objC;
			objC = static_cast<object_detect_classifier*>(objects.at(pIdNewObject));

			objectClassifier = objC->getObjectClassifier();
			pByColor = false;
			pByShape = false;
		break;
	}

	return true;
}

/**
 * Helper function to display a circle around the detected object
 */
void showDetectedObject(Mat& pImgOriginal, int pObjectId, int pXPos, int pYPos, int pWidth, int pHeight){

	Scalar contoursLineColor = Scalar(0, 0, 0);
	Scalar xLineColor = Scalar(0, 255, 0);
	Scalar yLineColor = Scalar(0, 255, 0);

	if (pXPos > (pImgOriginal.cols/2) + __DELTA){
		xLineColor = Scalar(255, 0, 0);
	}else if (pXPos < (pImgOriginal.cols/2) - __DELTA){
		xLineColor = Scalar(0, 0, 255);
	}

	if (pYPos > (pImgOriginal.rows/2) + __DELTA){
		yLineColor = Scalar(255, 0, 0);
	}else if (pYPos < (pImgOriginal.rows/2) - __DELTA){
		yLineColor = Scalar(0, 0, 255);
	}

	int radius = pWidth / 2;
	if (pHeight > pWidth){
		radius = pHeight / 2;
	}

	// draw some crosshairs on the object
	circle(pImgOriginal, Point(pXPos,pYPos), radius, contoursLineColor, 2);
	line(pImgOriginal, Point(pXPos,pYPos), Point(pXPos,pYPos-25), xLineColor, 2);
	line(pImgOriginal, Point(pXPos,pYPos), Point(pXPos,pYPos+25), xLineColor, 2);
	line(pImgOriginal, Point(pXPos,pYPos), Point(pXPos-25,pYPos), yLineColor, 2);
	line(pImgOriginal, Point(pXPos,pYPos), Point(pXPos+25,pYPos), yLineColor, 2);
//	putText(pImgOriginal, "Tracking object " + intToString(pObjectId) + " at", Point(pXPos,pYPos), 1, 1, contoursLineColor, 2);
//	putText(pImgOriginal, "(" + intToString(pXPos) + "," + intToString(pYPos) + ")", Point(pXPos,pYPos+25), 1, 1, contoursLineColor, 2);
//	putText(pImgOriginal, "(" + intToString(pWidth) + "," + intToString(pHeight) + ")", Point(pXPos,pYPos+50), 1, 1, contoursLineColor, 2);

//	imshow("Image", pImgOriginal);
}

/**
 * Detect if in image pImgOriginal there's an object that fit the configuration
 */
void detectObjectByColorShape(Mat& pImgOriginal, bool pByColor, int pColorConversion,
	int pLowH, int pHighH, int pLowS, int pHighS, int pLowV, int pHighV, Publisher pub, Rate loop_rate, int pImageID, Publisher pubDebug){

	int ellipse = 10;
	int vertices;
	double mincos, maxcos;
	int radius;
	double area;
	Mat imgColorTransformed;
	Mat imgThresholded;
	vector<vector<Point> > contours;
	vector<Point> approx;
	bool objectDetected = false;

	// Remove noise
	GaussianBlur(pImgOriginal, imgColorTransformed, Size(5,5), 2, 2);

	// Convert the captured frame
	cvtColor(imgColorTransformed, imgColorTransformed, pColorConversion);

	// Threshold the image
	if (pByColor){
		// detect pixeles that are in the range of color
		inRange(imgColorTransformed, Scalar(pLowH, pLowS, pLowV), Scalar(pHighH, pHighS, pHighV), imgThresholded);

		// Reduce noise
		erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));
		dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));

		dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));
		erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));

	}else{
		// Reduce noise
		blur(imgColorTransformed, imgColorTransformed, Size(3,3));
		// Keep the contours
		Canny(imgColorTransformed, imgThresholded, 80, 240, 3);
	}

	// Find contours
	findContours(imgThresholded.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	for (int i = 0; i < contours.size(); i++){

		// Approximate contour with accuracy proportional to the contour perimeter
		//approxPolyDP(Mat(contours[i]), approx, arcLength(Mat(contours[i]), true)*0.02, true);

		// Skip small objects
		if (fabs(contourArea(contours[i])) < __AREA_DISCARD)
			continue;

		objectDetected = true;

		Rect boundingRectangle = boundingRect(contours[i]);
		// create message
		vision msg;
		msg.image_id = pImageID;
		msg.pic_height = pImgOriginal.rows;
		msg.pic_width = pImgOriginal.cols;
		msg.obj_found = true;
		msg.obj_height = boundingRectangle.height;
		msg.obj_width = boundingRectangle.width;
		msg.obj_coord_x = boundingRectangle.x + boundingRectangle.width/2;
		msg.obj_coord_y = boundingRectangle.y + boundingRectangle.height/2;

		vertices = approx.size();
		if ((vertices > 3) && (vertices < 7)){
			// Get the cosines of all corners
			vector<double> cos;
			for (int j = 2; j < vertices + 1; j++){
				cos.push_back(angle(approx[j%vertices], approx[j-2], approx[j-1]));
			}

			// Sort ascending the cosine values
			sort(cos.begin(), cos.end());

			// Get the lowest and the highest cosine
			mincos = cos.front();
			maxcos = cos.back();
		}

		switch (vertices){

			// Triangles
			case 3:
				setLabel(pImgOriginal, "TRI", contours[i]);
				msg.obj_type = 3;
			break;

			// Rectangles
			case 4:
				if ((mincos >= -0.1) && (maxcos <= 0.3)){
					setLabel(pImgOriginal, "RECT", contours[i]);
					msg.obj_type = 4;
				}else{
					msg.obj_type = -4;
				}
			break;

			// Pentagones
			case 5:
				if ((mincos >= -0.34) && (maxcos <= -0.27)){
					setLabel(pImgOriginal, "PENTA", contours[i]);
					msg.obj_type = 5;
				}else{
					msg.obj_type = -5;
				}
			break;

			// Hexagons
			case 6:
				if ((mincos >= -0.55) && (maxcos <= -0.45)){
					setLabel(pImgOriginal, "HEXA", contours[i]);
					msg.obj_type = 6;
				}else{
					msg.obj_type = -6;
				}
			break;

			// other consider circle or semi-circle
			default:
				area = contourArea(contours[i]);
				radius = boundingRectangle.width / 2;

				if (abs(1 - ((double)boundingRectangle.width / boundingRectangle.height)) <= 0.2 &&
					abs(1 - (area / (CV_PI * pow(radius, 2)))) <= 0.2){
					setLabel(pImgOriginal, "CIR", contours[i]);
					msg.obj_type = 1;
				}else{
					setLabel(pImgOriginal, "SEMI-CIR", contours[i]);
					msg.obj_type = 2;
				}
			break;

		}

		// display detected object
		showDetectedObject(pImgOriginal, i, msg.obj_coord_x, msg.obj_coord_y, msg.obj_width, msg.obj_height);

		// Publish message
		pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();

	}

	if (!objectDetected){
		vision msg;
		msg.obj_found = false;
		// Publish message
		pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
	}

	if (publishImage){
		string imgEncoded = encode(pImgOriginal);
		//ROS_INFO("\n%s\n", imgEncoded.c_str());
		std_msgs::String imgValue;
		imgValue.data = imgEncoded;
		pubDebug.publish(imgValue);
		ros::spinOnce();
		loop_rate.sleep();
	}

	// show the thresholded image
//	imshow("Thresholded Image", imgThresholded);

}

/**
 * Detect if in image pImgOriginal there's an object that fit the classifier
 */
void detectObjectByClassifier(Mat& pImgOriginal, CascadeClassifier pObjectClassifier, Publisher pub, Rate loop_rate, int pImageID, Publisher pubDebug){

	Mat imgColorTransformed;
	vector<Rect> objects;
	bool objectDetected = false;
/*
	roiTop = -1;
	roiHeight = -1;
	roiEdge = -1;
*/
	cvtColor(pImgOriginal, imgColorTransformed, COLOR_BGR2GRAY);
	equalizeHist(imgColorTransformed, imgColorTransformed);

	// Detect objects
	pObjectClassifier.detectMultiScale(imgColorTransformed, objects, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));

	for (int i = 0; i < objects.size(); i++){

		if (!objectDetected){
			roiTop = -1;
			roiHeight = -1;
			roiEdge = -1;
		}

		objectDetected = true;

		// Create message
		vision msg;
		msg.image_id = pImageID;
		msg.obj_type = idNewObject;
		msg.pic_height = pImgOriginal.rows;
		msg.pic_width = pImgOriginal.cols;
		msg.obj_found = true;
		msg.obj_height = objects[i].height;
		msg.obj_width = objects[i].width;
		msg.obj_coord_x = objects[i].x + objects[i].width/2;
		msg.obj_coord_y = objects[i].y + objects[i].height/2;

		//ROI
		if ((roiTop == -1) || (roiTop > objects[i].y)){
			roiTop = objects[i].y;
		}
		if ((roiHeight == -1) || (roiHeight < objects[i].y + objects[i].height)){
			roiHeight = objects[i].height;
		}
		if ((roiEdge == -1) || (roiEdge < objects[i].x)){
			roiEdge = objects[i].x;
		}

		// display detected object
		showDetectedObject(pImgOriginal, i, msg.obj_coord_x, msg.obj_coord_y, msg.obj_width, msg.obj_height);

		// Publish message
		pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();

	}

	if (!objectDetected){
		vision msg;
		msg.obj_found = false;
		// Publish message
		pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
	}


	if (roiTop > -1){
		line(pImgOriginal, Point(0, roiTop), Point(roiEdge, roiTop), Scalar(255,255,255), 2);
		line(pImgOriginal, Point(0, roiTop+roiHeight), Point(roiEdge, roiTop+roiHeight), Scalar(255,255,255), 2);
		line(pImgOriginal, Point(roiEdge, roiTop), Point(roiEdge, roiTop+roiHeight), Scalar(255,255,255), 2);
	}

	if (publishImage){
		string imgEncoded = encode(pImgOriginal);
		//ROS_INFO("\n%s\n", imgEncoded.c_str());
		std_msgs::String imgValue;
		imgValue.data = imgEncoded;
		pubDebug.publish(imgValue);
		ros::spinOnce();
		loop_rate.sleep();
	}
/*
	imshow("Image", pImgOriginal);
*/
}

float innerAngle(float px1, float py1, float px2, float py2, float cx1, float cy1){

	float dist1 = sqrt((px1-cx1)*(px1-cx1) + (py1-cy1)*(py1-cy1));
	float dist2 = sqrt((px2-cx1)*(px2-cx1) + (py2-cy1)*(py2-cy1));

	float Ax, Ay;
	float Bx, By;
	float Cx, Cy;

	Cx = cx1;
	Cy = cy1;

	if(dist1 < dist2){
		Bx = px1;
		By = py1;
		Ax = px2;
		Ay = py2;
	}else{
		Bx = px2;
		By = py2;
		Ax = px1;
		Ay = py1;
	}

	float Q1 = Cx - Ax;
	float Q2 = Cy - Ay;
	float P1 = Bx - Ax;
	float P2 = By - Ay;

	float A = acos((P1*Q1 + P2*Q2) / (sqrt(P1*P1+P2*P2) * sqrt(Q1*Q1+Q2*Q2)));
	A = A * 180 / CV_PI;

	return A;
}

/**
 *
 */
void detectHandGesture(Mat& pImgOriginal, Mat& fore, Ptr<BackgroundSubtractorMOG2> bg, Publisher pub, Rate loop_rate, int pImageID, Publisher pubDebug){

	if (roiTop == -1){
		return;
	}

//	ROS_INFO("GESTURE DETECTE CALLED");

	Mat back;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	vector<Point> approx;

//	ROS_INFO("TOP: %i, Edge: %i, Height: %i", roiTop, roiEdge, roiHeight);

	Rect const mask(0, roiTop, roiEdge, roiHeight);
//	ROS_INFO("IMG WIDTH: %i, IMG HEIGHT: %i", pImgOriginal.cols, pImgOriginal.rows);
	Mat maskRoi = pImgOriginal(mask);

//	ROS_INFO("CREATIN ROI");

	bool objectDetected = false;
	int ellipse = 2;

	bg->apply(maskRoi, fore);
//	ROS_INFO("MASK APPLIED");
	bg->getBackgroundImage(back);
//	ROS_INFO("GET BG");

	erode(fore, fore, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));
	dilate(fore, fore, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));

//	ROS_INFO("NOISE CLEANED");

	findContours(fore, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

//	ROS_INFO("CONTOURS FOUND");

	for( size_t i = 0; i < contours.size(); i++ ){

		if (fabs(contourArea(contours[i])) < 500)
			continue;

		vector<vector<Point> > hull(1);
		convexHull(Mat(contours[i]), hull[0], false);
		drawContours(maskRoi, hull, 0, Scalar(0, 0, 255), 3);

		if (hull[0].size() > 2){
			vector<int> hullIndexes(contours[i].size());
			convexHull(Mat(contours[i]), hullIndexes, true);
			vector<Vec4i> convDef(contours[i].size());
			vector<Point> validPoints;

			convexityDefects(contours[i], hullIndexes, convDef);
			Rect boundingBox = boundingRect(hull[0]);
			Point center = Point(boundingBox.x + boundingBox.width / 2, boundingBox.y + boundingBox.height / 2);

			for (size_t j = 0; j < convDef.size(); j++){
				Point p1 = contours[i][convDef[j][0]];
				Point p2 = contours[i][convDef[j][1]];
				Point p3 = contours[i][convDef[j][2]];
				line(maskRoi, p1, p3, Scalar(255, 0, 0), 2);
				line(maskRoi, p3, p2, Scalar(255, 0, 0), 2);

				double angle = atan2(center.y - p1.y, center.x - p1.x) * 180 / CV_PI;
				double inAngle = innerAngle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
				double length = sqrt(pow(p1.x - p3.x, 2) + pow(p1.y - p3.y, 2));

				if (angle > -30 && angle < 160 && abs(inAngle) > 20 && abs(inAngle) < 120 && length > 0.25 * boundingBox.height){
					validPoints.push_back(p1);
				}

			}

			if (validPoints.size() > 2){

				//ROS_INFO("FINGERS: %i", validPoints.size());
				//drawContours( maskRoi, contours, (int)i, Scalar(255, 0, 0), 2, 8, vector<Vec4i>(), 0, Point() );
				for (size_t k = 0; k < validPoints.size(); k++){
					circle(maskRoi, validPoints[k], 9, Scalar(0, 255, 255), 2);
				}

				objectDetected = true;

				Rect boundingRectangle = boundingRect(contours[i]);
				// create message
				vision msg;
				msg.image_id = pImageID;
				msg.obj_type = -3;
				msg.pic_height = pImgOriginal.rows;
				msg.pic_width = pImgOriginal.cols;
				msg.obj_found = true;
				msg.obj_height = boundingRectangle.height;
				msg.obj_width = boundingRectangle.width;
				msg.obj_coord_x = boundingRectangle.x + boundingRectangle.width/2;
				msg.obj_coord_y = boundingRectangle.y + boundingRectangle.height/2;

				// Publish message
				pub.publish(msg);
				ros::spinOnce();
				loop_rate.sleep();

			}
		}

	}

	if (!objectDetected){
		vision msg;
		msg.obj_found = false;
		// Publish message
		pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
	}

	if (publishImage){
		string imgEncoded = encode(pImgOriginal);
		//ROS_INFO("\n%s\n", imgEncoded.c_str());
		std_msgs::String imgValue;
		imgValue.data = imgEncoded;
		pubDebug.publish(imgValue);
		ros::spinOnce();
		loop_rate.sleep();
	}

/*
	for( size_t i = 0; i < contours.size(); i++ ){

		if (fabs(contourArea(contours[i])) < 500)
			continue;

//		ROS_INFO("POSSIBLE OBJECT");

//		drawContours( maskRoi, contours, (int)i, Scalar(255, 0, 0), 2, 8, vector<Vec4i>(), 0, Point() );
		simple_object_detected = true;

		Rect boundingRectangle = boundingRect(contours[i]);
		// create message
		vision msg;
		msg.image_id = pImageID;
		msg.pic_height = pImgOriginal.rows;
		msg.pic_width = pImgOriginal.cols;
		msg.obj_found = true;
		msg.obj_height = boundingRectangle.height;
		msg.obj_width = boundingRectangle.width;
		msg.obj_coord_x = boundingRectangle.x + boundingRectangle.width/2;
		msg.obj_coord_y = boundingRectangle.y + boundingRectangle.height/2;

		// Publish message
		pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();

		if (pubDebug && publishFirstGesture){
			ROS_INFO("PUBLISHING IMAGE");
			publishFirstGesture = false;
			string imgEncoded = encode(pImgOriginal);
			//ROS_INFO("\n%s\n", imgEncoded.c_str());
			std_msgs::String imgValue;
			imgValue.data = imgEncoded;
			pubDebug.publish(imgValue);
			ros::spinOnce();
			loop_rate.sleep();
		}

	}

	if (!simple_object_detected){
		vision msg;
		msg.obj_found = false;
		// Publish message
		pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
	}
*/

//	imshow("Mask", maskRoi);
//	if (roiTop > -1){
//		line(pImgOriginal, Point(0, roiTop), Point(roiEdge, roiTop), Scalar(255,255,255), 2);
//		line(pImgOriginal, Point(0, roiHeight), Point(roiEdge, roiHeight), Scalar(255,255,255), 2);
//		line(pImgOriginal, Point(roiEdge, roiTop), Point(roiEdge, roiHeight), Scalar(255,255,255), 2);
//	}
//	imshow("Image", pImgOriginal);


/*
//	Mat frame;
//	Mat back;
//	Mat fore;
	vector<pair<Point,double> > palm_centers;
	int backgroundFrame = 500;
	int ellipse = 3;
	bool simple_object_detected = false;

//	ROS_INFO("TIME PASSED: %0.3f", timeInterval);

	vector<vector<Point> > contours;
	//Get the frame
	cap >> pImgOriginal;

	//Update the current background model and get the foreground
	bg->apply(pImgOriginal, fore);

	//Enhance edges in the foreground by applying erosion and dilation
	erode(fore, fore, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));
	dilate(fore, fore, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));


	//Find the contours in the foreground
	findContours(fore, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

	for (int i = 0; i < contours.size(); i++){
		//Ignore all small insignificant areas
		if (contourArea(contours[i]) >= 1000){

			//Draw contour
			vector<vector<Point> > tcontours;
			tcontours.push_back(contours[i]);
//draw
			drawContours(pImgOriginal,tcontours,-1,cv::Scalar(0,0,255),2);

			//Detect Hull in current contour
			vector<vector<Point> > hulls(1);
			vector<vector<int> > hullsI(1);
			convexHull(Mat(tcontours[0]), hulls[0], false);
			convexHull(Mat(tcontours[0]), hullsI[0], false);
//draw
			drawContours(pImgOriginal, hulls, -1, cv::Scalar(0,255,0), 2);

			//Find minimum area rectangle to enclose hand
			RotatedRect rect = minAreaRect(Mat(tcontours[0]));

			//Find Convex Defects
			vector<Vec4i> defects;
			if (hullsI[0].size() > 0){
				Point2f rect_points[4];
				rect.points(rect_points);
//draw
				for (int j = 0; j < 4; j++)
					line(pImgOriginal, rect_points[j], rect_points[(j+1)%4], Scalar(255,0,0), 1, 8);

				Point rough_palm_center;
				convexityDefects(tcontours[0], hullsI[0], defects);
				if (defects.size() >= 3){
					vector<Point> palm_points;
					for(int j = 0; j < defects.size(); j++){
						int startidx = defects[j][0];
						Point ptStart(tcontours[0][startidx]);
						int endidx = defects[j][1];
						Point ptEnd(tcontours[0][endidx]);
						int faridx=defects[j][2];
						Point ptFar(tcontours[0][faridx]);
						//Sum up all the hull and defect points to compute average
						rough_palm_center += ptFar + ptStart + ptEnd;
						palm_points.push_back(ptFar);
						palm_points.push_back(ptStart);
						palm_points.push_back(ptEnd);
					}

					//Get palm center by 1st getting the average of all defect points, this is the rough palm center,
					//Then U chose the closest 3 points ang get the circle radius and center formed from them which is the palm center.
					rough_palm_center.x /= defects.size() * 3;
					rough_palm_center.y /= defects.size() * 3;
					Point closest_pt = palm_points[0];
					vector<pair<double,int> > distvec;

					for(int i = 0; i < palm_points.size(); i++)
						distvec.push_back(make_pair(dist(rough_palm_center, palm_points[i]), i));
					sort(distvec.begin(), distvec.end());

					//Keep choosing 3 points till you find a circle with a valid radius
					//As there is a high chance that the closes points might be in a linear line or too close that it forms a very large circle
					pair<Point,double> soln_circle;
					for(int i = 0; i+2 < distvec.size(); i++){
						Point p1 = palm_points[distvec[i+0].second];
						Point p2 = palm_points[distvec[i+1].second];
						Point p3 = palm_points[distvec[i+2].second];
						soln_circle = circleFromPoints(p1, p2, p3);//Final palm center,radius
						if(soln_circle.second != 0)
							break;
					}

					//Find avg palm centers for the last few frames to stabilize its centers, also find the avg radius
					palm_centers.push_back(soln_circle);
					if(palm_centers.size() > 10)
						palm_centers.erase(palm_centers.begin());

					Point palm_center;
					double radius = 0;
					for(int i = 0; i < palm_centers.size(); i++){
						palm_center += palm_centers[i].first;
						radius += palm_centers[i].second;
					}
					palm_center.x /= palm_centers.size();
					palm_center.y /= palm_centers.size();
					radius /= palm_centers.size();

					if (radius > 60){
						break;
					}

					//Draw the palm center and the palm circle
					//The size of the palm gives the depth of the hand
//draw
					circle(pImgOriginal,palm_center,5,Scalar(144,144,255),3);
					circle(pImgOriginal,palm_center,radius,Scalar(144,144,255),2);

					//Detect fingers by finding points that form an almost isosceles triangle with certain thesholds
					int no_of_fingers = 0;
					for(int j = 0; j < defects.size(); j++){

						int startidx = defects[j][0];
						Point ptStart(tcontours[0][startidx]);
						int endidx = defects[j][1];
						Point ptEnd(tcontours[0][endidx]);
						int faridx = defects[j][2];
						Point ptFar(tcontours[0][faridx]);

						double Xdist = sqrt(dist(palm_center,ptFar));
						double Ydist = sqrt(dist(palm_center,ptStart));
						if ((Ydist < radius*1.1) || (Xdist < radius*1.1)){ //Check distance from fingers to center of palm
//draw
							line( pImgOriginal, ptEnd, ptFar, Scalar(255,255,255), 3 );
							no_of_fingers++;
						}

					}

					if (no_of_fingers > 3){

						simple_object_detected = true;

						// Create message
						vision msg;
						msg.image_id = pImageID;
						msg.obj_type = -1;
						msg.pic_height = pImgOriginal.rows;
						msg.pic_width = pImgOriginal.cols;
						msg.obj_found = true;
						msg.obj_height = radius;
						msg.obj_width = radius;
						msg.obj_coord_x = palm_center.x;
						msg.obj_coord_y = palm_center.y;

						// Publish message
						pub.publish(msg);
						ros::spinOnce();
						loop_rate.sleep();

//						if (pubDebug){
//							string imgEncoded = encode(pImgOriginal);
							//ROS_INFO("\n%s\n", imgEncoded.c_str());
//							std_msgs::String imgValue;
//							imgValue.data = imgEncoded;
//							pubDebug.publish(imgValue);
//							ros::spinOnce();
//							loop_rate.sleep();
//						}

					}

				}
			}
		}

	}

//		if(backgroundFrame>0)
//			putText(frame, "Recording Background", cvPoint(30,30), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200,200,250), 1, CV_AA);
//		imshow("Background",back);
//		imshow("Frame",frame);

	if (simple_object_detected){
		lastImageObjectFound = true;
	}else if (lastImageObjectFound){
		lastImageObjectFound = false;
		vision msg;
		msg.obj_found = false;
		// Publish message
		pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
	}

*/
}

void calibrationService(Mat& pImgOriginal, Publisher pub, Rate loop_rate){

	int ellipse = 10;
	int vertices;
	double mincos, maxcos;
	int radius;
	double area;
	Mat imgColorTransformed;
	Mat imgThresholded;
	vector<vector<Point> > contours;
	vector<Point> approx;
	bool objectDetected = false;


	// Remove noise
	GaussianBlur(pImgOriginal, imgColorTransformed, Size(5,5), 2, 2);

	// Convert the captured frame
	cvtColor(imgColorTransformed, imgColorTransformed, COLOR_BGR2HSV);
	//cvtColor(pImgOriginal, imgColorTransformed, COLOR_BGR2HSV);

	Rect const mask(pImgOriginal.cols/2 - 50, pImgOriginal.rows/2 - 50, 100, 100);
//	ROS_INFO("IMG WIDTH: %i, IMG HEIGHT: %i", pImgOriginal.cols, pImgOriginal.rows);
	Mat maskRoi = imgColorTransformed(mask);
//	imshow("Mask", maskRoi);

	Scalar mean, dev;
	meanStdDev(maskRoi, mean, dev);

//	cout << "H: " << mean[0] << " S: " << mean[1] << " V: " << mean[2] << "\n";

	double coefDevH = 0.8;
	double coefDevS = 1.8;
	double coefDevV = 2.7;

	//HUE
	gCalibLowH = mean[0] - dev[0]*coefDevH;
	if (gCalibLowH < 0){
		gCalibLowH = 0;
	}
	gCalibHighH = mean[0] + dev[0]*coefDevH;
	if (gCalibHighH > 180){
		gCalibHighH = 180;
	}

	//SATURATION
	gCalibLowS = mean[1] - dev[1]*coefDevS;
	if (gCalibLowS < 0){
		gCalibLowS = 0;
	}
	gCalibHighS = mean[1] + dev[1]*coefDevS;
	if (gCalibHighS > 255){
		gCalibHighS = 255;
	}

	//VALUE
	gCalibLowV = mean[2] - dev[2]*coefDevV;
	if (gCalibLowV < 0){
		gCalibLowV = 0;
	}
	gCalibHighV = mean[2] + dev[2]*coefDevV;
	if (gCalibHighV > 255){
		gCalibHighV = 255;
	}


	// Threshold the image
	// detect pixeles that are in the range of color
	inRange(imgColorTransformed, Scalar(gCalibLowH, gCalibLowS, gCalibLowV), Scalar(gCalibHighH, gCalibHighS, gCalibHighV), imgThresholded);

	// Reduce noise
	erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));
	dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));

	dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));
	erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(ellipse, ellipse)));


	// Find contours
	findContours(imgThresholded.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	for (int i = 0; i < contours.size(); i++){

		// Approximate contour with accuracy proportional to the contour perimeter
		//approxPolyDP(Mat(contours[i]), approx, arcLength(Mat(contours[i]), true)*0.02, true);

		// Skip small objects
		if (fabs(contourArea(contours[i])) < __AREA_DISCARD)
			continue;

		drawContours(pImgOriginal, contours, 0, Scalar(0, 0, 255), 3);
	}


	rectangle(pImgOriginal, Point(pImgOriginal.cols/2 - 50, pImgOriginal.rows/2 - 50), Point(pImgOriginal.cols/2 + 50, pImgOriginal.rows/2 + 50), Scalar(0,255,255), 2);

	string imgEncoded = encode(pImgOriginal);
	std_msgs::String imgValue;
	imgValue.data = imgEncoded;
	pub.publish(imgValue);
	ros::spinOnce();
	loop_rate.sleep();

	// show the thresholded image
	//imshow("Thresholded Image", pImgOriginal);

}

/**
 *
 */
void rotateImage(Mat& imgOriginal){

	//imshow("Image Orig", imgOriginal);
	Point2f src_center(imgOriginal.cols/2.0F, imgOriginal.rows/2.0F);
	Mat rot_mat = getRotationMatrix2D(src_center, 180, 1.0);
	Mat dst;
	warpAffine(imgOriginal, imgOriginal, rot_mat, imgOriginal.size());
	//imshow("Image Inv", dst);

}

/**
 * Helper function to change the range of color in execution time
 * run "rosrun rqt_reconfigure rqt_reconfigure" to be able to change the color ranges
void paramsCallback(butia_vision::ColorConfig &config, uint32_t level) {
    //ROS_INFO("Reconfigure Request: %d %d %d %d %d %d",config.Hue_min, config.Hue_max, config.Saturation, config.SaturationH, config.Value, config.ValueH);
	lowH = config.Hue_min;
	highH = config.Hue_max;
	lowS = config.Saturation;
	highS = config.SaturationH;
	lowV = config.Value;
	highV = config.ValueH;
}
 */


int main(int argc, char** argv){

	init(argc, argv, "vision");
	NodeHandle n;

	// Get configuration file path
/*
	if(n.hasParam("/butia_vision/config_file")){
		n.getParam("/butia_vision/config_file", pathConfigurationFile);
	}else{
		ROS_INFO("Configuration file not specified.");
		exit(-1);
	}
*/
	// Dynamic reconfigure colors value
	/*
     dynamic_reconfigure::Server<butia_vision::ColorConfig> server;
     dynamic_reconfigure::Server<butia_vision::ColorConfig>::CallbackType f;
     f = boost::bind(&paramsCallback, _1, _2);
     server.setCallback(f);
	*/

	ROS_INFO("Initializing service object");
	ServiceServer service = n.advertiseService("object_detect", detectObject);
	spinOnce();

	ROS_INFO("Initializing service gesture");
	ServiceServer serviceGest = n.advertiseService("gesture_detect", detectGesture);
	spinOnce();

	ROS_INFO("Initializing service for start calibration");
	ServiceServer serviceStartCalib = n.advertiseService("start_calibration", startCalibration);
	spinOnce();

	ROS_INFO("Initializing service for save calibration");
	ServiceServer serviceSaveCalib = n.advertiseService("save_calibration", saveCalibration);
	spinOnce();

	ROS_INFO("Initializing service image");
	ServiceServer serviceSendImg = n.advertiseService("request_image", sendImageCap);
	spinOnce();

	ROS_INFO("Creating Publisher");
	Publisher pub = n.advertise<vision>("butia_vision", 50);

	Publisher pubDebug = n.advertise<std_msgs::String>("imgs", 50);

	Rate loop_rate(10);

	cout << "WIDTH: " << cap.get(cv::CAP_PROP_FRAME_WIDTH) << endl;
	cout << "HEIGHT: " << cap.get(cv::CAP_PROP_FRAME_HEIGHT) << endl;

	cap.set(cv::CAP_PROP_FRAME_WIDTH, 320);
	cap.set(cv::CAP_PROP_FRAME_HEIGHT, 240);
//	cap.set(cv::CAP_PROP_BUFFERSIZE, 1);

	cout << "WIDTH: " << cap.get(cv::CAP_PROP_FRAME_WIDTH) << endl;
	cout << "HEIGHT: " << cap.get(cv::CAP_PROP_FRAME_HEIGHT) << endl;

	if (!cap.isOpened()){
		cout << "Cannot open the web cam" << endl;
		exit(-1);
	}

	loadConfig(pathConfigurationFile);
	state = none;

	int shape;
	bool byColor, byShape;
	int colorConversion;
	CascadeClassifier objectClassifier;
	Mat imgOriginal;
	Mat fore;
	Ptr<BackgroundSubtractorMOG2> bg;
	int imageID;

	idNewObject = -1;
	newsimple_object_detection = true;
	bg = createBackgroundSubtractorMOG2(5, 2);
//	bg->setNMixtures(3);
	imageID = 0;

	// Capture a temporary image from the camera
	Mat imgTmp;
	cap.read(imgTmp);

	// Create a black image with the size as the camera output
	Mat imgLines = Mat::zeros(imgTmp.size(), CV_8UC3);

	while (ok() && !isShuttingDown()){

		// read a new frame from video
		bool bSuccess = cap.read(imgOriginal);

		if (!bSuccess){
			ROS_INFO("ERROR ACQUIRING IMAGE");
			return (-1);
		}

		//Agregado para la rotacion de imagenes
		//rotateImage(imgOriginal);

		// Load the configuration for new object to detect
		if (newsimple_object_detection){
			cout << idNewObject << "\n";
			if (!loadObjectConfig(idNewObject, lowH, highH, lowS, highS, lowV, highV, byColor, colorConversion, shape, byShape, objectClassifier)){
				break;
			}
			newsimple_object_detection = false;
		}

		switch (state){
			case objectDetect:
				if ((byColor) || (byShape)){
					detectObjectByColorShape(imgOriginal, byColor, colorConversion, lowH, highH, lowS, highS, lowV, highV, pub, loop_rate, imageID, pubDebug);
				}else{
					detectObjectByClassifier(imgOriginal, objectClassifier, pub, loop_rate, imageID, pubDebug);
				}
			break;

			case gestureDetect:
				detectHandGesture(imgOriginal, fore, bg, pub, loop_rate, imageID, pubDebug);
			break;

			case calibration:
				idNewObject = 2;
				calibrationService(imgOriginal, pubDebug, loop_rate);
			break;
		}

		ros::spinOnce();

		imageID = (imageID + 1) % 2;

		// Necesary to display image
		waitKey(30);
	}

	return 0;
}
