#include "object_detect_color_shape.h"
#include "object_detect.h"

/*
pre-condition: HSV descomposition or shape type can't be equals to -1 at the same time
*/
object_detect_color_shape::object_detect_color_shape(int pObjectId, int pObjectType, int pLowH, int pHighH, int pLowS, int pHighS, int pLowV, int pHighV, int pShape):
		object_detect(pObjectId, pObjectType){

	//set HSV descomposition to detect by color
	lowH = pLowH;
	highH = pHighH;
	lowS = pLowS;
	highS = pHighS;
	lowV = pLowV;
	highV = pHighV;
	//if any value is equal -1 then you need to find the object to detect only by its shape
	byColor = (pLowH != -1);

	//set the shape to detect
	shape = pShape;
	//if the shape type equals -1 then you need to find the object to detect only by color
	byShape = (pShape != -1);
}

void object_detect_color_shape::setLowH(int pLowH){
	lowH = pLowH;
	byColor = (pLowH != -1);
}

void object_detect_color_shape::setHighH(int pHighH){
	highH = pHighH;
	byColor = (pHighH != -1);
}

void object_detect_color_shape::setLowS(int pLowS){
	lowS = pLowS;
	byColor = (pLowS != -1);
}

void object_detect_color_shape::setHighS(int pHighS){
	highS = pHighS;
	byColor = (pHighS != -1);
}

void object_detect_color_shape::setLowV(int pLowV){
	lowV = pLowV;
	byColor = (pLowV != -1);
}

void object_detect_color_shape::setHighV(int pHighV){
	highV = pHighV;
	byColor = (pHighV != -1);
}

void object_detect_color_shape::setShape(int pShape){
	shape = pShape;
	byShape = (pShape != -1);
}

int object_detect_color_shape::getLowH(){
	return lowH;
}

int object_detect_color_shape::getHighH(){
	return highH;
}

int object_detect_color_shape::getLowS(){
	return lowS;
}

int object_detect_color_shape::getHighS(){
	return highS;
}

int object_detect_color_shape::getLowV(){
	return lowV;
}

int object_detect_color_shape::getHighV(){
	return highV;
}

int object_detect_color_shape::getShape(){
	return shape;
}

bool object_detect_color_shape::isByColor(){
	return byColor;
}

bool object_detect_color_shape::isByShape(){
	return byShape;
}
