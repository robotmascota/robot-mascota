#ifndef object_detect_classifier_H
#define object_detect_classifier_H

#include "object_detect.h"
#include "opencv/cv.hpp"
#include <stdlib.h>

using namespace cv;
using namespace std;

class object_detect_classifier: public object_detect{

	protected:
		string classifierPath;
		CascadeClassifier objectClassifier;

	public:
		object_detect_classifier(int pObjectId, int pObjectType, string pObjectClassifierName);
		void setObjectClassifier(CascadeClassifier pObjectClassifier);
		CascadeClassifier getObjectClassifier();
		string getClassifierPath();

};

#endif
