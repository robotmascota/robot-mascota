#ifndef object_detect_color_shape_H
#define object_detect_color_shape_H

#include "object_detect.h"

class object_detect_color_shape: public object_detect{

	protected:
		bool byColor;
		int lowH, highH, lowS, highS, lowV, highV;
		bool byShape;
		int shape;

	public:
		object_detect_color_shape(int pObjectId, int pObjectType, int pLowH, int pHighH, int pLowS, int pHighS, int pLowV, int pHighV, int pShape);
		void setLowH(int pLowH);
		void setHighH(int pHighH);
		void setLowS(int pLowS);
		void setHighS(int pHighS);
		void setLowV(int pLowV);
		void setHighV(int pHighV);
		void setShape(int pShape);
		int getLowH();
		int getHighH();
		int getLowS();
		int getHighS();
		int getLowV();
		int getHighV();
		int getShape();
		bool isByColor();
		bool isByShape();

};

#endif
