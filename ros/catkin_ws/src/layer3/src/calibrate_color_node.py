#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy

from api_constants import CALIBRATE_COLOR_NODE_NAME, CALIBRATE_COLOR_SRV_NAME, \
    GENERAL_NODE_RESULTS, START_CALIBRATION_SRV_NAME, SAVE_CALIBRATION_SRV_NAME
from utils import log_data_info
from layer1.srv import calibration_start, calibration_ok
from layer3.srv import butia_calibrate_color_service

listen_topic = None
listen_srv = None


def service_callback(data):
    if data.enable:
        start()
        return GENERAL_NODE_RESULTS['processing']
    else:
        stop()
        return GENERAL_NODE_RESULTS['disabled']


def stop():
    calibration_srv = \
        rospy.ServiceProxy(SAVE_CALIBRATION_SRV_NAME, calibration_ok)
    calibration_srv()


def start():
    calibration_srv = \
        rospy.ServiceProxy(START_CALIBRATION_SRV_NAME, calibration_start)
    calibration_srv()


if __name__ == '__main__':
    rospy.init_node(CALIBRATE_COLOR_NODE_NAME, log_level=rospy.INFO)
    log_data_info(CALIBRATE_COLOR_NODE_NAME, '------- START ------- ')

    rospy.Service(CALIBRATE_COLOR_SRV_NAME, butia_calibrate_color_service,
                  service_callback)
    log_data_info(CALIBRATE_COLOR_NODE_NAME, 'listen service enabled')
    
    rospy.spin()
