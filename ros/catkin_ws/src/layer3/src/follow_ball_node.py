#!/usr/bin/env python
import rospy
from api_constants import FOLLOW_BALL_NODE_NAME, SEARCH_SRV_NAME, \
    FOLLOW_SRV_NAME, FOLLOW_BALL_SRV_NAME, EXECUTION_STATUS, \
    GENERAL_NODE_RESULTS, SEARCH_MODES, TARGETS, SEARCH_TOPIC_NAME, \
    FOLLOW_TOPIC_NAME, PERSONALITY_HIGH_LOGIC_TOPIC_NAME, \
    PERSONALITY_HIGH_LOGIC_MODES, PERSONALITY_EMOTIONS, END_OF_GAME_TOPIC_NAME
from utils import log_data_info
from layer2.msg import butia_search_msg, butia_follow_msg, \
    butia_personality_high_logic_msg
from layer2.srv import butia_search_service, butia_follow_service, \
    butia_search_serviceRequest, butia_follow_serviceRequest
from layer3.msg import butia_end_of_game_msg
from layer3.srv import butia_follow_ball_service

STATES = {
    'initialize': 'INITIALIZE',
    'search': 'SEARCH',
    'follow': 'FOLLOW'
}

# create messages that are used to publish feedback/result
search_srv = None
follow_srv = None
search_topic = None
follow_topic = None
is_follow_disabled = False
is_search_disabled = False
execution_status = EXECUTION_STATUS['disabled']
current_state = STATES['initialize']
target_found = False


def service_callback(data):
    global execution_status
    if data.enable:
        if execution_status == EXECUTION_STATUS['disabled']:
            # Start node execution
            log_data_info(FOLLOW_BALL_NODE_NAME, 'Start')
            log_data_info(FOLLOW_BALL_NODE_NAME, 'Service enabled')
            global current_state
            current_state = STATES['initialize']

            search()

            execution_status = EXECUTION_STATUS['enabled']
            return GENERAL_NODE_RESULTS['processing']
        else:
            return GENERAL_NODE_RESULTS['already_running']
    else:
        if execution_status == EXECUTION_STATUS['enabled']:
            execution_status = EXECUTION_STATUS['disabled']
            if current_state == STATES['search']:
                tear_down_search()
            if current_state == STATES['follow']:
                tear_down_follow()

        log_data_info(FOLLOW_BALL_NODE_NAME, 'Service disabled')
        return GENERAL_NODE_RESULTS['disabled']


def search():
    log_data_info(FOLLOW_BALL_NODE_NAME, 'Searching')
    global target_found, current_state, is_search_disabled
    target_found = False
    current_state = STATES['search']
    is_search_disabled = False

    # Call Search service
    search_srv_data = butia_search_serviceRequest()
    search_srv_data.enable = True
    search_srv_data.mode = SEARCH_MODES['static']
    search_srv_data.target = TARGETS['object']
    search_srv(search_srv_data)

    global search_topic
    search_topic = rospy.Subscriber(
        SEARCH_TOPIC_NAME, butia_search_msg, search_callback)


def follow():
    log_data_info(FOLLOW_BALL_NODE_NAME, 'Following')
    global current_state, is_follow_disabled
    current_state = STATES['follow']
    is_follow_disabled = False

    # Call Follow service
    follow_srv_data = butia_follow_serviceRequest()
    follow_srv_data.enable = True
    follow_srv_data.target = TARGETS['object']
    follow_srv_data.return_when_too_close = True
    follow_srv(follow_srv_data)

    global follow_topic
    follow_topic = rospy.Subscriber(
        FOLLOW_TOPIC_NAME, butia_follow_msg, follow_callback)


def search_callback(data):
    try:
        if data.obj_found:
            global target_found
            target_found = True
            # Stop using Search node
            tear_down_search()
            follow()
    except Exception as e:
        if not is_search_disabled:
            raise e


def follow_callback(data):
    try:
        # Stop using Follow node
        tear_down_follow()
        log_data_info(FOLLOW_BALL_NODE_NAME, 'SUCCESS')

        pub = rospy.Publisher(PERSONALITY_HIGH_LOGIC_TOPIC_NAME,
                              butia_personality_high_logic_msg,
                              queue_size=1, latch=True)
        msg = butia_personality_high_logic_msg()
        msg.mode = PERSONALITY_HIGH_LOGIC_MODES['show_emotion']
        msg.value = PERSONALITY_EMOTIONS['happy']
        pub.publish(msg)

        pub2 = rospy.Publisher(END_OF_GAME_TOPIC_NAME, butia_end_of_game_msg, queue_size=1, latch=True)
        msg2 = butia_end_of_game_msg()
        msg2.success = True
        pub2.publish(msg2)
        global execution_status
        execution_status = EXECUTION_STATUS['disabled']

    except Exception as e:
        if not is_follow_disabled:
            raise e


def tear_down_search():
    log_data_info(FOLLOW_BALL_NODE_NAME, 'tear_down_search')
    global is_search_disabled
    is_search_disabled = True
    search_topic.unregister()
    search_srv_data = butia_search_serviceRequest()
    search_srv_data.enable = False
    search_srv(search_srv_data)


def tear_down_follow():
    log_data_info(FOLLOW_BALL_NODE_NAME, 'tear_down_follow')
    global is_follow_disabled
    is_follow_disabled = True
    follow_topic.unregister()
    follow_srv_data = butia_follow_serviceRequest()
    follow_srv_data.enable = False
    follow_srv(follow_srv_data)


if __name__ == '__main__':
    rospy.init_node(FOLLOW_BALL_NODE_NAME, log_level=rospy.INFO)
    log_data_info(FOLLOW_BALL_NODE_NAME, '------- START ------- ')

    # -------------- CONNECTIONS --------------
    # BUTIA_SEARCH service
    log_data_info(FOLLOW_BALL_NODE_NAME, '    waiting for service: %s' %
                  SEARCH_SRV_NAME)
    rospy.wait_for_service(SEARCH_SRV_NAME)
    log_data_info(FOLLOW_BALL_NODE_NAME, 'calling for service: %s' %
                  SEARCH_SRV_NAME)
    global search_srv
    search_srv = rospy.ServiceProxy(SEARCH_SRV_NAME, butia_search_service)

    # BUTIA_FOLLOW service
    log_data_info(FOLLOW_BALL_NODE_NAME, '    waiting for service: %s' %
                  FOLLOW_SRV_NAME)
    rospy.wait_for_service(FOLLOW_SRV_NAME)
    log_data_info(FOLLOW_BALL_NODE_NAME, 'calling for service: %s' %
                  FOLLOW_SRV_NAME)
    global follow_srv
    follow_srv = rospy.ServiceProxy(FOLLOW_SRV_NAME, butia_follow_service)
    # ------------------------------------------

    log_data_info(FOLLOW_BALL_NODE_NAME, 'waiting for a service call')
    rospy.Service(FOLLOW_BALL_SRV_NAME, butia_follow_ball_service,
                  service_callback)

    rospy.spin()
