#!/usr/bin/env python
# coding=utf-8
import rospy


from api_constants import LISTEN_SRV_NAME, ORDER_DETECTION_NODE_NAME, \
    MAIN_LISTEN_TOPIC_NAME, END_OF_GAME_TOPIC_NAME, \
    LISTEN_ADD_COMMANDS_SRV_NAME, MAIN_COMMANDS, \
    FOLLOW_BALL_SRV_NAME, HIDDEN_OBJECT_SRV_NAME, SAY_MY_NAME_SRV_NAME, \
    CALIBRATE_COLOR_SRV_NAME
from utils import log_data_info
from layer2.msg import butia_listen_msg
from layer2.srv import butia_listen_service, butia_listen_serviceRequest, \
    butia_commands_listen_service, butia_commands_listen_serviceRequest
from layer3.msg import butia_end_of_game_msg
from layer3.srv import butia_follow_ball_service, \
    butia_follow_ball_serviceRequest, butia_hidden_object_service, \
    butia_hidden_object_serviceRequest, butia_say_my_name_service, \
    butia_say_my_name_serviceRequest, butia_calibrate_color_service, \
    butia_calibrate_color_serviceRequest


listen_srv = None
behavior_srv = None
srv_data = None


def end_of_game_callback(data):
    global behavior_srv
    behavior_srv = None


def listen_callback(data):
    log_data_info(ORDER_DETECTION_NODE_NAME, 'listening callback')
    command = data.command
    global srv_data, behavior_srv

    if not behavior_srv:
        if command in MAIN_COMMANDS['follow_ball']:
            log_data_info(ORDER_DETECTION_NODE_NAME,
                          '    waiting for service: %s' %
                          FOLLOW_BALL_SRV_NAME)
            rospy.wait_for_service(FOLLOW_BALL_SRV_NAME)
            log_data_info(ORDER_DETECTION_NODE_NAME, 'calling for service: %s' %
                          FOLLOW_BALL_SRV_NAME)
            behavior_srv = rospy.ServiceProxy(FOLLOW_BALL_SRV_NAME,
                                              butia_follow_ball_service)
            srv_data = butia_follow_ball_serviceRequest()
            srv_data.enable = True
            srv_res = behavior_srv(srv_data)
            log_data_info(ORDER_DETECTION_NODE_NAME,
                          'start service %s result: %s' %
                          (FOLLOW_BALL_SRV_NAME, srv_res))
        elif command in MAIN_COMMANDS['calibrate']:
            rospy.wait_for_service(CALIBRATE_COLOR_SRV_NAME)
            log_data_info(CALIBRATE_COLOR_SRV_NAME, 'calling for service: %s' %
                          CALIBRATE_COLOR_SRV_NAME)
            behavior_srv = rospy.ServiceProxy(CALIBRATE_COLOR_SRV_NAME,
                                              butia_calibrate_color_service)
            srv_data = butia_calibrate_color_serviceRequest()
            srv_data.enable = True
            srv_res = behavior_srv(srv_data)
            log_data_info(ORDER_DETECTION_NODE_NAME,
                          'start service %s result: %s' %
                          (CALIBRATE_COLOR_SRV_NAME, srv_res))
        elif command in MAIN_COMMANDS['hidden_object']:
            log_data_info(ORDER_DETECTION_NODE_NAME,
                          '    waiting for service: %s' %
                          HIDDEN_OBJECT_SRV_NAME)
            rospy.wait_for_service(HIDDEN_OBJECT_SRV_NAME)
            log_data_info(ORDER_DETECTION_NODE_NAME, 'calling for service: %s' %
                          HIDDEN_OBJECT_SRV_NAME)
            behavior_srv = rospy.ServiceProxy(HIDDEN_OBJECT_SRV_NAME,
                                              butia_hidden_object_service)
            srv_data = butia_hidden_object_serviceRequest()
            srv_data.enable = True
            srv_res = behavior_srv(srv_data)
            log_data_info(ORDER_DETECTION_NODE_NAME,
                          'start service %s result: %s' %
                          (HIDDEN_OBJECT_SRV_NAME, srv_res))
        elif command in MAIN_COMMANDS['say_my_name']:
            log_data_info(ORDER_DETECTION_NODE_NAME,
                          '    waiting for service: %s' %
                          SAY_MY_NAME_SRV_NAME)
            rospy.wait_for_service(SAY_MY_NAME_SRV_NAME)
            log_data_info(ORDER_DETECTION_NODE_NAME, 'calling for service: %s' %
                          SAY_MY_NAME_SRV_NAME)
            behavior_srv = rospy.ServiceProxy(SAY_MY_NAME_SRV_NAME,
                                              butia_say_my_name_service)
            srv_data = butia_say_my_name_serviceRequest()
            srv_data.enable = True
            srv_res = behavior_srv(srv_data)
            log_data_info(ORDER_DETECTION_NODE_NAME,
                          'start service %s result: %s' %
                          (SAY_MY_NAME_SRV_NAME, srv_res))
    elif command in MAIN_COMMANDS['end_of_game']:
        srv_data.enable = False
        srv_res = behavior_srv(srv_data)
        log_data_info(ORDER_DETECTION_NODE_NAME,
                      'end current service result: %s' % srv_res)
        behavior_srv = None


if __name__ == '__main__':
    rospy.init_node(ORDER_DETECTION_NODE_NAME, log_level=rospy.INFO)
    log_data_info(ORDER_DETECTION_NODE_NAME, '------- START ------- ')

    # Listen service start
    log_data_info(ORDER_DETECTION_NODE_NAME, 'waiting for service: %s' %
                  LISTEN_SRV_NAME)
    rospy.wait_for_service(LISTEN_SRV_NAME)
    log_data_info(ORDER_DETECTION_NODE_NAME, 'calling for service: %s' %
                  LISTEN_SRV_NAME)
    listen_srv = rospy.ServiceProxy(LISTEN_SRV_NAME, butia_listen_service)
    listen_srv_data = butia_listen_serviceRequest()
    listen_srv_data.enable = True
    res = listen_srv(listen_srv_data)
    
    # Add commands to listen service
    rospy.wait_for_service(LISTEN_ADD_COMMANDS_SRV_NAME)
    butia_add_commands_listen_srv = \
        rospy.ServiceProxy(LISTEN_ADD_COMMANDS_SRV_NAME,
                           butia_commands_listen_service)
    for key, comms in MAIN_COMMANDS.items():
        for synonyms in comms:
            listen_srv_data = butia_commands_listen_serviceRequest()
            listen_srv_data.command_list = str([synonyms])
            listen_srv_data.topic_name = MAIN_LISTEN_TOPIC_NAME
            butia_add_commands_listen_srv(listen_srv_data)

    # Listen to listen topic
    rospy.Subscriber(MAIN_LISTEN_TOPIC_NAME, butia_listen_msg, listen_callback)
    rospy.Subscriber(END_OF_GAME_TOPIC_NAME, butia_end_of_game_msg,
                     end_of_game_callback)

    rospy.spin()
