#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
from api_constants import SEARCH_SRV_NAME, EXECUTION_STATUS, \
    GENERAL_NODE_RESULTS, SEARCH_MODES, TARGETS, SEARCH_TOPIC_NAME, \
    FOLLOW_TOPIC_NAME, HIDDEN_OBJECT_NODE_NAME, LISTEN_ADD_COMMANDS_SRV_NAME, \
    HIDDEN_OBJECT_SRV_NAME, LISTEN_SRV_NAME, \
    HIDDEN_OBJECT_LISTEN_TOPIC_NAME, HIDDEN_OBJECT_COMMANDS, FOLLOW_SRV_NAME, \
    FOLLOW_BALL_NODE_NAME, HIDDEN_OBJECT_COMMANDS_MOVEMENT, \
    MOVEMENT_DIRECTIONS, LISTEN_REMOVE_COMMANDS_SRV_NAME, \
    PERSONALITY_HIGH_LOGIC_TOPIC_NAME, PERSONALITY_HIGH_LOGIC_MODES, \
    PERSONALITY_EMOTIONS, MOVE_TOPIC_NAME, END_OF_GAME_TOPIC_NAME
from utils import log_data_info
from layer2.msg import butia_search_msg, butia_follow_msg, butia_listen_msg, \
    butia_personality_high_logic_msg, butia_move_msg
from layer2.srv import butia_search_service, butia_search_serviceRequest, \
    butia_follow_serviceRequest, \
    butia_commands_listen_service, \
    butia_listen_serviceRequest, butia_commands_listen_serviceRequest, \
    butia_listen_service, butia_follow_service
from layer3.msg import butia_end_of_game_msg
from layer3.srv import butia_hidden_object_service

STATES = {
    'initialize': 'INITIALIZE',
    'listen_to_command': 'LISTEN_TO_COMMAND',
    'search': 'SEARCH',
    'follow': 'FOLLOW',
    'move': 'MOVE'
}

# create messages that are used to publish feedback/result
search_srv = None
follow_srv = None
move_topic = None
search_topic = None
listen_topic = None
is_follow_disabled = False
is_search_disabled = False
is_listen_disabled = False
execution_status = EXECUTION_STATUS['disabled']
current_state = STATES['initialize']
target_found = False


def service_callback(data):
    global execution_status
    if data.enable:
        if execution_status == EXECUTION_STATUS['disabled']:
            # Start node execution
            log_data_info(HIDDEN_OBJECT_NODE_NAME, 'Start')

            initialize()

            log_data_info(HIDDEN_OBJECT_NODE_NAME, 'Service enabled')
            return GENERAL_NODE_RESULTS['processing']
        else:
            return GENERAL_NODE_RESULTS['already_running']
    elif execution_status == EXECUTION_STATUS['enabled']:
        global execution_status
        execution_status = EXECUTION_STATUS['disabled']
        if current_state == STATES['follow']:
            tear_down_follow()
        tear_down_search()
        tear_down_listen()

        listen_topic.unregister()
        move(MOVEMENT_DIRECTIONS['stop'])

    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'Service disabled')
    return GENERAL_NODE_RESULTS['disabled']


def initialize():
    global current_state
    current_state = STATES['initialize']
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'state: Initialize')

    # BUTIA_LISTEN add words service
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'waiting for service: %s' %
                  LISTEN_SRV_NAME)
    rospy.wait_for_service(LISTEN_ADD_COMMANDS_SRV_NAME)
    add_commands_listen_srv = \
        rospy.ServiceProxy(LISTEN_ADD_COMMANDS_SRV_NAME,
                           butia_commands_listen_service)
    for key, comms in HIDDEN_OBJECT_COMMANDS.items():
        for synonyms in comms:
            listen_srv_data = butia_commands_listen_serviceRequest()
            listen_srv_data.command_list = str([synonyms])
            listen_srv_data.topic_name = HIDDEN_OBJECT_LISTEN_TOPIC_NAME
            add_commands_listen_srv(listen_srv_data)

    # Listen to listening topic
    global listen_topic, is_listen_disabled
    is_listen_disabled = False
    listen_topic = rospy.Subscriber(
        HIDDEN_OBJECT_LISTEN_TOPIC_NAME, butia_listen_msg,
        listen_to_command_callback)
    # Start to search static
    search(SEARCH_MODES['static'])

    global execution_status
    execution_status = EXECUTION_STATUS['enabled']
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'enabled')


def listen_to_command_callback(data):
    try:
        global current_state, execution_status
        current_state = STATES['listen_to_command']
        log_data_info(HIDDEN_OBJECT_NODE_NAME,
                      'state: listening commands')

        command = data.command
        log_data_info(HIDDEN_OBJECT_NODE_NAME, 'command: %s' % command)
        moving_comms = [item
                        for synonyms in HIDDEN_OBJECT_COMMANDS_MOVEMENT.values()
                        for item in synonyms]
        if command in moving_comms:
            if current_state == STATES['search']:
                tear_down_search()
                search(SEARCH_MODES['static']) 

            raw_command = (k for k, v in HIDDEN_OBJECT_COMMANDS_MOVEMENT.items()
                           if command in v).next()
            current_state = STATES['move']
            move(MOVEMENT_DIRECTIONS[raw_command])
        elif command in HIDDEN_OBJECT_COMMANDS['search']:
            tear_down_search()
            current_state = STATES['search']
            search(SEARCH_MODES['around'])
    except Exception as e:
        if not is_listen_disabled:
            raise e


def search(search_mode):
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'state: Searching')
    global target_found, is_search_disabled
    target_found = False
    is_search_disabled = False

    # Call Search service
    search_srv_data = butia_search_serviceRequest()
    search_srv_data.enable = True
    search_srv_data.mode = search_mode
    search_srv_data.target = TARGETS['object']
    search_srv(search_srv_data)

    global search_topic
    search_topic = rospy.Subscriber(
        SEARCH_TOPIC_NAME, butia_search_msg, search_callback)


def move(direction):
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'state: MOVING TO %s' %
                  direction)
    move_data = butia_move_msg()
    move_data.direction = direction
    move_topic.publish(move_data)


def follow():
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'Following')
    global current_state, is_follow_disabled
    current_state = STATES['follow']
    is_follow_disabled = False

    # Call Follow service
    follow_srv_data = butia_follow_serviceRequest()
    follow_srv_data.enable = True
    follow_srv_data.target = TARGETS['object']
    follow_srv_data.return_when_too_close = True
    follow_srv(follow_srv_data)

    global follow_topic
    follow_topic = rospy.Subscriber(
        FOLLOW_TOPIC_NAME, butia_follow_msg, follow_callback)


def search_callback(data):
    try:
        if data.obj_found:
            log_data_info(HIDDEN_OBJECT_NODE_NAME, 'Search --> FOUND')
            global target_found
            target_found = True
            # Stop using Search node
            tear_down_search()
            follow()
    except Exception as e:
        if not is_search_disabled:
            raise e


def follow_callback(data):
    try:
        # Stop using Follow node
        tear_down_follow()
        log_data_info(HIDDEN_OBJECT_NODE_NAME, 'SUCCESS')

        pub = rospy.Publisher(PERSONALITY_HIGH_LOGIC_TOPIC_NAME,
                              butia_personality_high_logic_msg,
                              queue_size=1, latch=True)
        msg = butia_personality_high_logic_msg()
        msg.mode = PERSONALITY_HIGH_LOGIC_MODES['show_emotion']
        msg.value = PERSONALITY_EMOTIONS['happy']
        
        pub.publish(msg)
        
        tear_down_search()
        tear_down_listen()

        listen_topic.unregister()

        pub2 = rospy.Publisher(END_OF_GAME_TOPIC_NAME, butia_end_of_game_msg,
                               queue_size=1, latch=True)
        msg2 = butia_end_of_game_msg()
        msg2.success = True
        pub2.publish(msg2)
        global execution_status
        execution_status = EXECUTION_STATUS['disabled']
        
    except Exception as e:
        if not is_follow_disabled:
            raise e


def tear_down_search():
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'tear_down_search')
    global is_search_disabled
    is_search_disabled = True
    search_topic.unregister()
    search_srv_data = butia_search_serviceRequest()
    search_srv_data.enable = False
    search_srv(search_srv_data)


def tear_down_follow():
    log_data_info(FOLLOW_BALL_NODE_NAME, 'tear_down_follow')
    global is_follow_disabled
    is_follow_disabled = True
    follow_topic.unregister()
    follow_srv_data = butia_follow_serviceRequest()
    follow_srv_data.enable = False
    follow_srv(follow_srv_data)


def tear_down_listen():
    log_data_info(FOLLOW_BALL_NODE_NAME, 'tear_down_listen')
    global is_listen_disabled
    is_listen_disabled = True
    listen_topic.unregister()
    # BUTIA_LISTEN add words service
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'waiting for service: %s' %
                  LISTEN_REMOVE_COMMANDS_SRV_NAME)
    rospy.wait_for_service(LISTEN_REMOVE_COMMANDS_SRV_NAME)
    remove_commands_listen_srv = \
        rospy.ServiceProxy(LISTEN_REMOVE_COMMANDS_SRV_NAME,
                           butia_commands_listen_service)
    for key, comms in HIDDEN_OBJECT_COMMANDS.items():
        for synonyms in comms:
            listen_srv_data = butia_commands_listen_serviceRequest()
            listen_srv_data.command_list = str([synonyms])
            listen_srv_data.topic_name = HIDDEN_OBJECT_LISTEN_TOPIC_NAME
            remove_commands_listen_srv(listen_srv_data)


if __name__ == '__main__':
    rospy.init_node(HIDDEN_OBJECT_NODE_NAME, log_level=rospy.INFO)
    log_data_info(HIDDEN_OBJECT_NODE_NAME, '------- START ------- ')

    # -------------- CONNECTIONS --------------
    # BUTIA_MOVE service
    log_data_info(HIDDEN_OBJECT_NODE_NAME, '    waiting for topic: %s' %
                  MOVE_TOPIC_NAME)
    move_topic = rospy.Publisher(MOVE_TOPIC_NAME, butia_move_msg, queue_size=1)

    # BUTIA_SEARCH service
    log_data_info(HIDDEN_OBJECT_NODE_NAME, '    waiting for service: %s' %
                  SEARCH_SRV_NAME)
    rospy.wait_for_service(SEARCH_SRV_NAME)
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'calling for service: %s' %
                  SEARCH_SRV_NAME)
    search_srv = rospy.ServiceProxy(SEARCH_SRV_NAME, butia_search_service)

    # BUTIA_FOLLOW action
    log_data_info(HIDDEN_OBJECT_NODE_NAME, '    waiting for service: %s' %
                  FOLLOW_SRV_NAME)
    rospy.wait_for_service(FOLLOW_SRV_NAME)
    log_data_info(FOLLOW_BALL_NODE_NAME, 'calling for service: %s' %
                  FOLLOW_SRV_NAME)
    follow_srv = rospy.ServiceProxy(FOLLOW_SRV_NAME, butia_follow_service)

    # BUTIA_LISTEN service
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'waiting for service: %s' %
                  LISTEN_SRV_NAME)
    rospy.wait_for_service(LISTEN_SRV_NAME)
    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'calling for service: %s' %
                  LISTEN_SRV_NAME)
    listen_srv = rospy.ServiceProxy(LISTEN_SRV_NAME, butia_listen_service)
    listen_data = butia_listen_serviceRequest()
    listen_data.enable = True
    listen_srv(listen_data)
    
    # ------------------------------------------

    log_data_info(HIDDEN_OBJECT_NODE_NAME, 'waiting for a service call')
    rospy.Service(HIDDEN_OBJECT_SRV_NAME, butia_hidden_object_service,
                  service_callback)

    rospy.spin()
