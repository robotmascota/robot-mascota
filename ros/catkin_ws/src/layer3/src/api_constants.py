#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Node names
ORDER_DETECTION_NODE_NAME = 'butia_order_detection'
FOLLOW_BALL_NODE_NAME = 'butia_follow_ball'
HIDDEN_OBJECT_NODE_NAME = 'butia_hidden_object'
SAY_MY_NAME_NODE_NAME = 'butia_say_my_name'
CALIBRATE_COLOR_NODE_NAME = 'butia_calibrate_color'

# Service names
FOLLOW_BALL_SRV_NAME = 'butia_follow_ball_srv'
HIDDEN_OBJECT_SRV_NAME = 'butia_hidden_oject_srv'
SAY_MY_NAME_SRV_NAME = 'butia_say_my_name_srv'
CALIBRATE_COLOR_SRV_NAME = 'butia_calibrate_color_srv'

# Topic names
MAIN_LISTEN_TOPIC_NAME = 'butia_main_listen_topic'
END_OF_GAME_TOPIC_NAME = 'butia_end_of_game_topic'


# External services
LISTEN_SRV_NAME = 'butia_listen_srv'
START_CALIBRATION_SRV_NAME = 'start_calibration'
SAVE_CALIBRATION_SRV_NAME = 'save_calibration'
LISTEN_ADD_COMMANDS_SRV_NAME = 'butia_add_commands_listen_srv'
LISTEN_REMOVE_COMMANDS_SRV_NAME = 'butia_remove_commands_listen_srv'
SEARCH_SRV_NAME = 'butia_search_srv'
FOLLOW_SRV_NAME = 'butia_follow_srv'
MOVE_SRV_NAME = 'butia_move_srv'
GESTURE_RECOGNITION_SRV_NAME = 'butia_gesture_recognition_srv'
PERSONALITY_SOUND_SRV_NAME = 'butia_personality_sound_srv'
PERSONALITY_IMAGE_SRV_NAME = 'butia_personality_image_srv'
PERSONALITY_CLEAR_SRV_NAME = 'butia_personality_clear_srv'


# External topics
CALIBRATE_COLOR_LISTEN_TOPIC_NAME = 'butia_calibrate_color_topic'
HIDDEN_OBJECT_LISTEN_TOPIC_NAME = 'butia_hidden_object_listen_topic'
SEARCH_TOPIC_NAME = 'butia_search'
FOLLOW_TOPIC_NAME = 'butia_follow'
GESTURE_RECOGNITION_TOPIC_NAME = 'butia_gesture_recognition'
CLOSENESS_DETECTION_TOPIC_NAME = 'butia_closeness_detection'
PERSONALITY_HIGH_LOGIC_TOPIC_NAME = 'butia_personality_high_logic'
MOVE_TOPIC_NAME = 'butia_move'


# Others
MAIN_COMMANDS = {
    'hidden_object': ['escondida'],
    'say_my_name': ['tito'],
    'follow_ball': ['seguir pelotita', 'seguir la pelotita', 'seguir pelota',
                    'seguir la pelota'],
    'end_of_game': ['fin de juego', 'fin juego', 'fin juego', 'terminar juego', 
                    'finalizar', 'terminar'],
    'calibrate': ['calibrar colores']
}
EXECUTION_STATUS = {
    'enabled': 'ENABLED',
    'disabled': 'DISABLED'
}
GENERAL_NODE_RESULTS = {
    'processing': 'PROCESSING',
    'communication_error': 'COMMUNICATION_ERROR',
    'invalid_parameter': 'INVALID_PARAMETER',
    'disabled': 'DISABLED',
    'found': 'FOUND',
    'already_running': 'ALREADY_RUNNING',
    'processed': 'PROCESSED'
}
SEARCH_MODES = {
    'static': 'STATIC',
    'around': 'AROUND'
}
TARGETS = {
    'face': 'FACE',
    'object': 'OBJECT'
}
MOVEMENT_DIRECTIONS = {
    'left': 'LEFT',
    'right': 'RIGHT',
    'backward': 'BACKWARD',
    'forward': 'FORWARD',
    'stop': 'STOP'
}
HIDDEN_OBJECT_COMMANDS_MOVEMENT = {
    'left': ['izquierda', 'izquierdo'],
    'right': ['derecha', 'derecho'],
    'backward': ['atras', 'atrás'],
    'forward': ['adelante', 'frente'],
    'stop': ['parar', 'quieto', 'frenar']
}
HIDDEN_OBJECT_COMMANDS = {
    'search': ['busca']
}
HIDDEN_OBJECT_COMMANDS.update(HIDDEN_OBJECT_COMMANDS_MOVEMENT)
PERSONALITY_HIGH_LOGIC_MODES = {
    'show_emotion': 'SHOW_EMOTION',
    'show_task': 'SHOW_TASK',
    'show_phrase': 'SHOW_PHRASE'
}
PERSONALITY_EMOTIONS = {
    'happy': 'HAPPY',
    'sad': 'SAD'
}
