#!/usr/bin/env python
import rospy
from api_constants import SAY_MY_NAME_NODE_NAME, SEARCH_SRV_NAME, \
    FOLLOW_SRV_NAME, EXECUTION_STATUS, GENERAL_NODE_RESULTS, SEARCH_MODES, \
    TARGETS, SEARCH_TOPIC_NAME, SAY_MY_NAME_SRV_NAME, \
    GESTURE_RECOGNITION_TOPIC_NAME, GESTURE_RECOGNITION_SRV_NAME, \
    CLOSENESS_DETECTION_TOPIC_NAME, PERSONALITY_HIGH_LOGIC_TOPIC_NAME, \
    PERSONALITY_HIGH_LOGIC_MODES, PERSONALITY_EMOTIONS, MOVE_TOPIC_NAME, \
    MOVEMENT_DIRECTIONS, END_OF_GAME_TOPIC_NAME
from layer1.srv import localize_sound
from utils import log_data_info
from layer2.msg import butia_search_msg, butia_gesture_recognition_msg, \
    butia_close_detection_msg, butia_personality_high_logic_msg, butia_move_msg
from layer2.srv import butia_search_service, butia_follow_service, \
    butia_search_serviceRequest, butia_follow_serviceRequest, \
    butia_gesture_recognition_serviceRequest, butia_gesture_recognition_service
from layer3.msg import butia_end_of_game_msg
from layer3.srv import butia_say_my_name_service
from time import sleep

GESTURE_TIMEOUT = rospy.Duration(10)
STATES = {
    'initialize': 'INITIALIZE',
    'search': 'SEARCH',
    'follow': 'FOLLOW',
    'gesture_recognition': 'GESTURE_RECOGNITION'
}

# create messages that are used to publish feedback/result
search_srv = None
follow_srv = None
gesture_recognition_srv = None
search_topic = None
closeness_detection_topic = None
gesture_recognition_topic = None
is_close_detection_disabled = False
is_search_disabled = False
is_gesture_disabled = False
execution_status = EXECUTION_STATUS['disabled']
current_state = STATES['initialize']
target_found = False
gesture_found = False
timer = None
search_direction = MOVEMENT_DIRECTIONS['left']
move_topic = None


def service_callback(data):
    global execution_status, search_direction
    if data.enable:
        if execution_status == EXECUTION_STATUS['disabled']:
            # Start node execution
            log_data_info(SAY_MY_NAME_NODE_NAME, 'Start')
            log_data_info(SAY_MY_NAME_NODE_NAME, 'Service enabled')
            execution_status = EXECUTION_STATUS['enabled']

            search_direction = localize_sound_srv().direction
            search(search_direction)

            return GENERAL_NODE_RESULTS['processing']
        else:
            return GENERAL_NODE_RESULTS['already_running']
    else:
        if execution_status == EXECUTION_STATUS['enabled']:
            execution_status = EXECUTION_STATUS['disabled']
            if current_state == STATES['search']:
                tear_down_search()
            if current_state == STATES['gesture_recognition']:
                tear_down_gesture_recongition()
            if current_state == STATES['follow']:
                tear_down_follow_and_close_detection()

        log_data_info(SAY_MY_NAME_NODE_NAME, 'Service disabled')
        return GENERAL_NODE_RESULTS['disabled']


def search(moving_direction=None):
    log_data_info(SAY_MY_NAME_NODE_NAME, 'Searching')
    global target_found, current_state, is_search_disabled
    target_found = False
    current_state = STATES['search']
    is_search_disabled = False

    # Call Search service
    search_srv_data = butia_search_serviceRequest()
    search_srv_data.enable = True
    search_srv_data.target = TARGETS['face']
    search_srv_data.mode = SEARCH_MODES['static']
    if moving_direction:
        search_srv_data.mode = SEARCH_MODES['around']
        if moving_direction == MOVEMENT_DIRECTIONS['left']:
            search_srv_data.start_moving_left = True
    search_srv(search_srv_data)

    global search_topic
    search_topic = rospy.Subscriber(
        SEARCH_TOPIC_NAME, butia_search_msg, search_callback)


def gesture_recognition():
    log_data_info(SAY_MY_NAME_NODE_NAME, 'Recogniting gesture')
    global gesture_found, current_state, is_gesture_disabled
    gesture_found = False
    current_state = STATES['gesture_recognition']
    is_gesture_disabled = False

    # Call gesture_recognition service
    srv_data = butia_gesture_recognition_serviceRequest()
    srv_data.enable = True
    gesture_recognition_srv(srv_data)

    global gesture_recognition_topic
    gesture_recognition_topic = rospy.Subscriber(
        GESTURE_RECOGNITION_TOPIC_NAME,
        butia_gesture_recognition_msg,
        gesture_recongition_callback)

    global timer
    timer = rospy.Timer(GESTURE_TIMEOUT, gesture_not_found_callback)


def follow():
    log_data_info(SAY_MY_NAME_NODE_NAME, 'Following')
    global current_state
    current_state = STATES['follow']
    # Call Follow service
    follow_srv_data = butia_follow_serviceRequest()
    follow_srv_data.enable = True
    follow_srv_data.target = TARGETS['face']
    follow_srv(follow_srv_data)


def closeness_detection():
    log_data_info(SAY_MY_NAME_NODE_NAME, 'Closeness')
    global is_close_detection_disabled
    is_close_detection_disabled = False

    global closeness_detection_topic
    closeness_detection_topic = rospy.Subscriber(
        CLOSENESS_DETECTION_TOPIC_NAME, butia_close_detection_msg,
        close_detection_callback)


def search_callback(data):
    try:
        if data.obj_found:
            global target_found
            target_found = True
            # Stop using Search node
            tear_down_search()
            gesture_recognition()
    except Exception as e:
        if not is_search_disabled:
            raise e


def gesture_recongition_callback(data):
    try:
        if data.gesture_found:
            global gesture_found
            gesture_found = True
            # Stop using Search node
            tear_down_gesture_recongition()
            follow()
            closeness_detection()
    except Exception as e:
        if not is_gesture_disabled:
            raise e


def close_detection_callback(data):
    try:
        if data.front:
            tear_down_follow_and_close_detection()

            pub = rospy.Publisher(PERSONALITY_HIGH_LOGIC_TOPIC_NAME,
                                  butia_personality_high_logic_msg,
                                  queue_size=1, latch=True)
            msg = butia_personality_high_logic_msg()
            msg.mode = PERSONALITY_HIGH_LOGIC_MODES['show_emotion']
            msg.value = PERSONALITY_EMOTIONS['happy']
            pub.publish(msg)

            pub2 = rospy.Publisher(END_OF_GAME_TOPIC_NAME, butia_end_of_game_msg, queue_size=1, latch=True)
            msg2 = butia_end_of_game_msg()
            msg2.success = True
            pub2.publish(msg2)
            global execution_status
            execution_status = EXECUTION_STATUS['disabled']

            log_data_info(SAY_MY_NAME_NODE_NAME, 'SUCCESS')
    except Exception as e:
        if not is_close_detection_disabled:
            raise e


def gesture_not_found_callback(event):
    if not gesture_found and execution_status == EXECUTION_STATUS['enabled'] \
            and current_state == STATES['gesture_recognition']:
        log_data_info(SAY_MY_NAME_NODE_NAME, 'Gesture NOT FOUND')
        tear_down_gesture_recongition()

        move_data = butia_move_msg()
        move_data.direction = search_direction
        move_topic.publish(move_data)
        sleep(2)
        search(search_direction)


def tear_down_search():
    log_data_info(SAY_MY_NAME_NODE_NAME, 'tear_down_search')
    global is_search_disabled, search_topic
    is_search_disabled = True
    search_topic.unregister()
    search_topic = None
    srv_data = butia_search_serviceRequest()
    srv_data.enable = False
    search_srv(srv_data)


def tear_down_gesture_recongition():
    timer.shutdown()

    global is_gesture_disabled
    is_gesture_disabled = True
    gesture_recognition_topic.unregister()

    log_data_info(SAY_MY_NAME_NODE_NAME, 'tear_down_gesture_recongition')

    srv_data = butia_gesture_recognition_serviceRequest()
    srv_data.enable = False
    gesture_recognition_srv(srv_data)

    log_data_info(SAY_MY_NAME_NODE_NAME, 'state: STOP')
    move_data = butia_move_msg()
    move_data.direction = MOVEMENT_DIRECTIONS['stop']
    move_topic.publish(move_data)


def tear_down_follow_and_close_detection():
    log_data_info(SAY_MY_NAME_NODE_NAME, 'tear_down_follow_and_close_detection')
    global is_close_detection_disabled, closeness_detection_topic
    is_close_detection_disabled = True
    if closeness_detection_topic:
        closeness_detection_topic.unregister()
        closeness_detection_topic = None
    srv_data = butia_follow_serviceRequest()
    srv_data.enable = False
    follow_srv(srv_data)


if __name__ == '__main__':
    rospy.init_node(SAY_MY_NAME_NODE_NAME, log_level=rospy.INFO)
    log_data_info(SAY_MY_NAME_NODE_NAME, '------- START ------- ')

    # -------------- CONNECTIONS --------------
    # BUTIA_SEARCH service
    log_data_info(SAY_MY_NAME_NODE_NAME, '    waiting for service: %s' %
                  SEARCH_SRV_NAME)
    rospy.wait_for_service(SEARCH_SRV_NAME)
    log_data_info(SAY_MY_NAME_NODE_NAME, 'calling for service: %s' %
                  SEARCH_SRV_NAME)
    search_srv = rospy.ServiceProxy(SEARCH_SRV_NAME, butia_search_service)

    # BUTIA_FOLLOW service
    log_data_info(SAY_MY_NAME_NODE_NAME, '    waiting for service: %s' %
                  FOLLOW_SRV_NAME)
    rospy.wait_for_service(FOLLOW_SRV_NAME)
    log_data_info(SAY_MY_NAME_NODE_NAME, 'calling for service: %s' %
                  FOLLOW_SRV_NAME)
    follow_srv = rospy.ServiceProxy(FOLLOW_SRV_NAME, butia_follow_service)

    # BUTIA_GESTURE_RECOGNITION service
    log_data_info(SAY_MY_NAME_NODE_NAME, '    waiting for service: %s' %
                  GESTURE_RECOGNITION_SRV_NAME)
    rospy.wait_for_service(GESTURE_RECOGNITION_SRV_NAME)
    log_data_info(SAY_MY_NAME_NODE_NAME, 'calling for service: %s' %
                  GESTURE_RECOGNITION_SRV_NAME)
    gesture_recognition_srv = rospy.ServiceProxy(
        GESTURE_RECOGNITION_SRV_NAME, butia_gesture_recognition_service)

    # BUTIA_Localization service
    log_data_info(SAY_MY_NAME_NODE_NAME, '    waiting for service: %s' %
                  'butia_localize_sound')
    rospy.wait_for_service('butia_localize_sound')
    log_data_info(SAY_MY_NAME_NODE_NAME, 'calling for service: %s' %
                  'butia_localize_sound')
    localize_sound_srv = rospy.ServiceProxy('butia_localize_sound',
                                            localize_sound)

    # BUTIA_MOVE service
    log_data_info(SAY_MY_NAME_NODE_NAME, '    waiting for topic: %s' %
                  MOVE_TOPIC_NAME)
    move_topic = rospy.Publisher(MOVE_TOPIC_NAME, butia_move_msg, queue_size=1)
    # ------------------------------------------

    log_data_info(SAY_MY_NAME_NODE_NAME, 'waiting for a service call')
    rospy.Service(SAY_MY_NAME_SRV_NAME, butia_say_my_name_service,
                  service_callback)

    rospy.spin()
