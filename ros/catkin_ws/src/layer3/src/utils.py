import rospy

def log_data_info(node_name, message):
    rospy.loginfo('--> %s:    %s' % (node_name, message))
