import os
import signal
import subprocess
from subprocess import Popen
import sys

""" 
Este es un script para grabar frases desde el butia.
En esta primer version no se hace distincion desde donde (distancia y angulo) se debe leer las frases que aparecen en pantalla.

El script desplegar  una por una las frases del archivo raw-phrases.txt, cuando esta listo para comenzar a leer la frase precione ENTER, lea y vuelva a presionar ENTER una vez que termine dicha frase
"""


destFolder = 'recordings'
f = open('raw-phrases.txt','r')
content = f.readlines()
f2 = open('last-processed-line.txt','r')
lastline = int(f2.readline())
f2.close()

transcriptionFile = open('recordings.transcription','w')
fileIdsFile = open('recordings.fileids','w')

i = 1
for phrase in content:
	if i <= lastline:
		i=i+1
		continue
#	print(phrase)
#	print('presione ENTER para comenzar a grabar')
#	sys.stdin.readline()
#	p = Popen('arecord -Dhw:1,0 -fS16_LE -r16000 -c1 ./' + destFolder + '/recording' + str(i).zfill(4) + '.wav',
#    		shell = True,
#		preexec_fn=os.setsid)
#	print('presione ENTER para terminar la grabacion')
#	sys.stdin.readline()
#	os.killpg(p.pid, signal.SIGINT) # this magic step is taken from: http://stackoverflow.com/questions/4789837/how-to-terminate-a-python-subprocess-launched-with-shell-true
#	subprocess.call(['echo ' + str(i) + ' > last-processed-line.txt'], shell=True)
	transcriptionFile.write('<s> ' + phrase.strip() + ' </s> (recording_' + str(i).zfill(4) + ')\n')
	fileIdsFile.write(str(i).zfill(4) + '\n')
	i=i+1

transcriptionFile.close()
fileIdsFile.close()
