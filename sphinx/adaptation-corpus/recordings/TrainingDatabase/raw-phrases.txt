el sector todos que lidera tito cerró ayer el congreso que desarrolló durante el fin de semana en trinidad la capital de flores
en su discurso tito insistió en que el frente amplio es responsable de una década perdida
Además agregó que la administración presidida por tito pierde el tiempo en discusiones y se amputa la acción por tener problemas internos
tito incluso se preguntó si después de la presentación de las pautas salariales el grupo va a defender los intereses de los trabajadores o se va a alinear con el gobierno como hizo durante la campaña
durante el encuentro se discutió un documento que traza la estrategia de Todos con vistas a los próximos cinco años
Qué dejó esta deliberación
vamos a conversar con el senador tito del espacio cuarenta una de las agrupaciones que conforman el movimiento Todos.
Cuál era el objetivo de este congreso del sector
La consolidación del sector y su organización
porque nosotros como sector no sé si llegamos a dos años
nos creamos para enfrentar una elección interna
no existía el sector todos
es un grupo político que se conformó con la agrupación de diferentes sectores del partido nacional 
provenientes de orígenes diferentes para enfrentar la interna de junio de dos mil catorce
respaldando la candidatura de tito
salimos de una elección y entramos en otra
no habíamos tenido el tiempo de darnos la organización
de nombrar autoridades nacionales
que lo hicimos ayer
con la creación de la junta nacional del sector
el proceso electoral duró casi dos años terminó hace dos meses
los intendentes asumieron hace veinte días
a dos meses de haber terminado un proceso electoral de dos años 
quinientos compañeros y compañeras de todo el país nos reunimos durante un fin de semana en trinidad para mirar para adelante y organizarnos
un detalle no menor en la tarde del sábado hablaron cincuenta y cuatro compañeros para analizar un documento que nos fue remitido quince días antes
en el que se recibieron aportes vía electrónica y se puso en común hablando medio centenar de compañeros
haciendo aportes críticas agregados 
una cosa muy participativa para todos muy importante
cuánto hubo en este congreso de análisis a propósito del resultado electoral
de la derrota frente al frente amplio
le diría que el análisis político fue desde el comienzo del sector hasta el proceso de mayo
sin títulos pomposos aquellos de autocrítica
analizando un documento
diciendo las cosas que creemos que hicimos bastante bien y las cosas que no hicimos bien
con absoluta transparencia
con todos los compañeros del sector de todo el país
y con un grado de respeto y fraternidad muy interesante
yo lo decía el sábado
creo que después de tantos años en los que la vida política se inundó de insultos
en los que la calidad de debate político cayó tanto
en los que la mala palabra y la ordinariez muchas veces era cosa común para descalificar al otro
poder tener la posibilidad de devolverle al debate político un tono constructivo
de debate de ideas 
la verdad que está bueno
yo me formé en la política con tito
y él siempre decía
la vida política es debate de ideas no pelea de personas
me parece que eso es lo que enriquece
todo ese proceso fue analizado con un agregado
un documento arriba de la mesa
y mirando para adelante
los temas demográficos
el cambio climático
la competitividad de nuestros productos
obviamente que la seguridad
la educación
todo esto a dos meses de haber terminado el proceso electoral
se lo preguntaba teniendo en cuenta este comienzo que tiene la crónica de el observador de hoy
lo que pasó pasó
de poco vale flagelarse
preguntándose una y otra vez cuáles fueron las razones por las que el partido nacional quedó cuatrocientos mil votos debajo del frente amplio en las elecciones de octubre
y no se gana nada señalando a los responsables más o menos directos de la derrota del partido nacional
es una síntesis de lo que habría sido el espíritu de muchos de los participantes del congreso
seguramente sí
pero el resultado de las elecciones no lo cambiamos ni aunque estemos reunidos trescientos sesenta y cinco días veinticuatro horas
eso no se cambia más
lo que podemos construir ahora es nuestro trabajo de estos cinco años y lo que viene
no quiere decir no ver las cosas que podemos hacer mejor
tengamos en cuenta que nosotros nos enfrentamos no solo a un partido político
sino a un aparato de poder que manejó un volumen de recursos económicos
muchos de ellos de origen estatal
puestos al servicio de una candidatura
fue un partido político
fue el movimiento sindical
fue el dinero del Estado
todo puesto a favor de una candidatura política que fue la del doctor tito
nosotros iniciamos una campaña electoral un poco antes
con todo esto en contra
y con un dato que no es menor
tuvimos un poco más de una década
de bonanza económica como no se recuerda en el último siglo
son factores objetivos
no hay que enojarse con ellos
hay que analizarlos 
saber que existieron
también es verdad que cuando nadie pensaba que podía ser así renovamos al tito
pusimos una agenda de trabajo arriba de la mesa
creo que sembramos mucho para el futuro cercano
usted insiste en el tema de mirar hacia adelante
cómo se parará concretamente el sector en estos próximos cinco años
qué es lo que acordaron en cuanto a cuál será la estrategia
el camino a seguir
insisto mucho en mirar para adelante porque si miramos para atrás mucho no vamos a ganar
a nadie le interesa mucho que nos detengamos 
por eso es que miramos para adelante
nosotros tenemos una responsabilidad
en el proceso electoral pasado nuestro sector político
todos hacia adelante
se constituyó
no por decisión propia sino por respaldo popular
por votos
el sector más grande de todo el sistema político
es un dato que realmente nosotros repetimos poco y ustedes los medios también
siempre se decía
el sector del senador tito es el más grande
bueno eso sucedió durante tres elecciones de corrido
a unos pocos meses de haber constituido nuestro grupo en octubre del año pasado
el sector todos hacia adelante se constituyó por voto popular en el sector político más grande
con más respaldo electoral
superando al sector de tito
que había tenido esta característica años atrás
por lo tanto ahora tenemos una responsabilidad
somos el sector mayoritario del partido mayoritario de la oposición
tenemos la responsabilidad de representar
en la porción que nos corresponde