import re
import sys
from subprocess import call

keywordList=["derecha","izquierda","adelante","atrás","quieto","tito","frío","caliente","pelotita"]

bestAccuracy = -100

p = re.compile("TOTAL.* Accuracy = (.*)%")

for line in open(r'test.out'):
    match = p.match(line)
    if match:
        print match.group(1)
