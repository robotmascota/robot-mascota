/*
 * Copyright 2008 Kyoto University and Honda Motor Co.,Ltd.
 * All rights reserved.
 * HARK was developed by researchers in Okuno Laboratory
 * at the Kyoto University and Honda Research Institute Japan Co.,Ltd.
 */

#include <iostream>
#include "BufferedNode.h"
#include "Buffer.h"
#include "Vector.h"
#include <math.h>
#include "Source.h"

using namespace std;
//using namespace boost;
using namespace FD;

#if defined(ENABLE_DEBUGPRINT)

    #define DEBUG_PRINT(format, args...)                \
    fprintf(stderr, "[%s : %s, line %d] "format"\n",      \
        __FUNCTION__, __FILE__, __LINE__, ##args)

#else

    #define DEBUG_PRINT(format, args...)

#endif // #if defined(ENABLE_DEBUGPRINT)

class PrintSourceLocation;

DECLARE_NODE(PrintSourceLocation);

#include <string.h> 
#include "libharkio3.h"

/*Node
 *
 * @name PrintSourceLocation
 * @category HARK:Localization
 * @description Save source locations as a text file.
 *
 * @input_name SOURCES
 * @input_type Vector<ObjectRef>
 * @input_description Source locations with ID. Each element of the vector is a source location with ID specified by "Source".
 *
 * @output_name OUTPUT
 * @output_type Vector<ObjectRef>
 * @output_description The same as input.
 *
 * 
 END*/

class PrintSourceLocation : public BufferedNode {
    int sourcesID;
    int outputID;

public:
    PrintSourceLocation(string nodeName, ParameterSet params): 
        BufferedNode(nodeName, params)
    {
        sourcesID = addInput("SOURCES");
        outputID = addOutput("OUTPUT");
       
        inOrder = true;
    }

    void calculate(int output_id, int count, Buffer &out)
    {
        RCPtr<Vector<ObjectRef> > pSources = getInput(sourcesID, count);
        const Vector<ObjectRef>& sources = *pSources;

        out[count] = pSources;

        harkio_Position* tmp[sources.size()];
        for (int i = 0; i < sources.size(); i++) {
            RCPtr<Source> src = sources[i];
            harkio_Position* pos = harkio_Position_new(
                src->id,
                Cartesian,
                src->x,
                NULL
            );
            tmp[i] = pos;
        }

        harkio_Positions* positions = harkio_Positions_new(
            NULL,
            count,
            sources.size(),
            tmp
        );
        harkio_Positions_print(positions);

        return;
    }
};