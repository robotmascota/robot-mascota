import re
import sys
import math


def print_to_file(data, filename):
    with open(filename, 'a+') as dat_file_to_append:
        dat_file_to_append.write(data + '\n')

filename = sys.argv[1]
# file to save
name, ext = filename.split('.')
file_to_save = name + "_______processed." + ext
open(file_to_save, 'w').close()

line_to_process = False
sounds = {}
current_frame = 0
# for line in open(sys.argv[1]):
for line in open(filename):
    if "<positions frame=" in line:
        current_frame = int(re.findall('frame="(\d+)\"', line)[0])
        line_to_process = True
    elif "</positions>" in line:
        line_to_process = False
    elif line_to_process:
        matches = re.findall(
            '<position x="(.+)" y="(.+)" .+ id="(.+)"/>',
            line)
        for pattern in matches:
            sid = pattern[2]
            if sid not in sounds.keys():
                sounds[sid] = []
            sounds[sid].append({
                current_frame: [pattern[0], pattern[1]]})

for sid, frames in sounds.items():
    frame_num = 0
    for f in frames:
        while frame_num not in f.keys():
            str_frame_num = str(frame_num) \
                if frame_num > 9 else '0' + str(frame_num)
            print_to_file("id:%s\t\tframe:%s\t\tNONE" %
                          (sid, str_frame_num), file_to_save)
            frame_num += 1

        x = float(f[frame_num][0].replace(',', '.'))
        y = float(f[frame_num][1].replace(',', '.'))
        if x != 0:
            angle = math.degrees(math.atan(y/x))
        else:
            angle = 0
        angle = str(angle) + " degrees"
        frame_num += 1

        str_frame_num = str(frame_num) \
            if frame_num > 9 else '0' + str(frame_num)
        str_x = ' ' + str(x) if x > 0 else str(x)
        str_y = ' ' + str(y) if x > 0 else str(y)
        print_to_file("id:%s\t\tframe:%s\t\tangle:%s\t\tx:%s  y:%s" %
                      (sid, str_frame_num, str(angle), str_x, str_y),
                      file_to_save)

    print_to_file("-" * 120, file_to_save)
