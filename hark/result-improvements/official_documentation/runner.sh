#!/bin/bash

arr=("1-x-adelante_y-izquierda.wav"  "2-x-adelante.wav"  "3-x-adelante_y-derecha.wav"  "4-y-izquierda.wav"  "6-y-derecha.wav"  "7-x-atras_y-izquierda.wav"  "8-x-atras.wav"  "9-x-atras_y-derecha.wav")
for i in ${arr[@]}
do
	echo $i
	IN=$(echo $i| cut -f 1 -d".")
	./demoOffline.n "$i" plots/"$IN"_Localization.txt > plots/"$IN"_log.txt

	python online_plotMusicSpec.py plots/"$IN"_log.txt
	python processLocalization.py plots/"$IN"_Localization.txt
done


