#!/usr/bin/env python
import re
import subprocess
import os
import sys
import os.path
import numpy as np
path = str(os.path.dirname(os.path.abspath(__file__)))
print path

# ----------- -----------

default_thresh = 0

pseye_files = ['pseye_geotf.zip']
thresh_list = range(0, 100, 5)

audio_prefixes = ['audios/clean/', 'audios/noisy/']
audios = [
    'x-adelante.wav',
    'x-adelante_y-derecha.wav',
    'x-adelante_y-izquierda.wav',
    'x-atras.wav',
    'x-atras_y-derecha.wav',
    'x-atras_y-izquierda.wav',
    'y-derecha.wav',
    'y-izquierda.wav'
]
# ------------------------------

summary = open('results/summary.csv', 'w+')

res_index = 0

for pseye_conf in pseye_files:
    for thresh in thresh_list:

        for prefix in audio_prefixes:
            values = list([])
            for audio in audios:
                res_index = res_index + 1
                for i in range(0,4):
                    xCount = 0
                    xSum = 0
                
                    yCount = 0
                    ySum = 0
                
                    # First line of results
                    title_lines = 'pseye_configuration_file: %s\n' % pseye_conf
                    title_lines += 'thresh: %s\n' % thresh
                    title_lines += 'prefix: %s\n' % prefix
                    title_lines += 'audio: %s\n\n' % audio
                    
                    fname = 'results/' + str(i) + '/data' + str(res_index) + '.txt'
                    print(fname)
                    print(os.path.isfile(fname))
                    if os.path.isfile(fname):
                        file = open(fname)
                        
                        # Skip first lines
                        file.readline()
                        file.readline()
                        file.readline()
                        file.readline()
                        file.readline()
                        
                        line = file.readline()
                        
                        while line != '':
                            vals = line.split(',')
                            x = vals[0]
                            y = vals[1]
                            xCount = xCount + 1
                            xSum = xSum + float(x)
                            
                            yCount = yCount + 1
                            ySum = ySum + float(y)
                            
                            line = file.readline()
                        
                        if xCount > 0:
                            values.append(str(xSum/xCount))
                            values.append(str(ySum/yCount))
                        else:
                            values.append('')
                            values.append('')
                    else:
                        values.append('')
                        values.append('')
                        
                    
            hasNoise = prefix != 'audios/clean/'
            summary.write(str(res_index) + ';' + pseye_conf + ';' + str(thresh) + ';' + str(hasNoise) + ';' + ';'.join(values) + '\n')