Archivo de este directorio:

-> Audios:
1-x-adelante_y-izquierda.wav
2-x-adelante.wav
3-x-adelante_y-derecha.wav
4-y-izquierda.wav
6-y-derecha.wav
7-x-atras_y-izquierda.wav
8-x-atras.wav
9-x-atras_y-derecha.wav

Son audios grabados en una sala con aislamiento acústico reproduciendo siempre el mismo audio desde un dispositivo de reproducción y a una distancia fija del playstation eye.
Detalles de los audios:
Signed 16 bit Little Endian, Rate 16000 Hz, Channels 4


-> Directorio /plots
Contiene los archivos resultado de ejecutar los siguientes scripts:
* runner.sh
* processLocalization.py
Contiene además un el script raw_plotMusicSpec.py que es el script que viene en los ejemplos de HARK para graficar el resultado del nodo LocalizeMusic, ese resultado se puede encontrar en los archivos _log.txt de este directorio.


-> demoOffline.n
Red HARK encargada de localizar el sonido. Particularmente lo que hace es tomar un archivo de audio y generar dos archivos:
* _log.txt con información de debug del nodo LocalizeMusic
* _Localization.txt con información sobre los sonidos reconocidos y sus coordenadas


-> Scripts: online_plotMusicSpec.py y plotMusicSpec.py
Al igual que el script plots/raw_plotMusicSpec.py, estos scripts generan gráficas del resultado del nodo LocalizeMusic pero con modificaciones para generar gráficas más intutitivas. online_plotMusicSpec.py guarda además una imágen de cada gráfica generada en /plots


-> Script: processLocalization.py
Genera archivos de texto _______processed.txt en el directorio /plots que contienen el procesamientos de los archivos Localization.txt generados por el nodo SourceTracker. Los nuevos archivos contienen la siguiente información de los sonidos reconocidos:
(id, frame, angle, x, y)
El ángulo angle es el generado por el vector y el eje de las x.


-> Archivos de configuración: pseye_rectf.zip y pseye_geotf.zip
Archivos de configuración con la transfer function y otras características del playstation eye necesarias para HARK.


-> Script: runner.sh
Para cada archivo de audio del directorio (descriptos más arriba en Audios) hace lo siguiente:
- ejecuta la red HARK demoOffline.n
- ejecuta el script online_plotMusicSpec.py
- ejecuta el script processLocalization.py


-> separation_online_localization_from_wave.n
Red HARK encargada de localizar el sonido, separarlo y generar un archivo de audio sep_.wav por cada sonido detectado. La idea es que tenga la misma configuración que demoOffline.n.
Forma de ejecutarlo: ./separation_online_localization_from_wave.n audio_file




