import os
import re
import sys
import math

FRONT_LEFT = 'x-adelante_y-izquierda'
FRONT = 'x-adelante'
FRONT_RIGTH = 'x-adelante_y-derecha'
LEFT = 'y-izquierda'
RIGTH = 'y-derecha'
BACK_LEFT = 'x-atras_y-izquierda'
BACK = 'x-atras'
BACK_RIGHT = 'x-atras_y-derecha'


def process_file(direction):
    filename = 'hark_results.txt'

    line_to_process = False
    sounds = {}
    # for line in open(sys.argv[1]):
    for line in open(filename):
        if "<positions frame=" in line:
            line_to_process = True
        elif "</positions>" in line:
            line_to_process = False
        elif line_to_process:
            matches = re.findall(
                '<position x="(.+)" y="(.+)" .+ id="(.+)"/>',
                line)
            for pattern in matches:
                sid = pattern[2]
                if sid not in sounds.keys():
                    sounds[sid] = []
                sounds[sid].append({
                    'x': pattern[0].replace(',', '.'),
                    'y': pattern[1].replace(',', '.')
                })

    max_hits_for_an_id = 0
    for sid, directions in sounds.items():
        hits = 0
        for d in directions:
            x = float(d['x'])
            y = float(d['y'])
            if x > 0 and y > 0:
                dir = math.degrees(math.atan(y/x))
            elif x < 0 < y:
                dir = 90 + math.degrees(math.atan(-x/y))
            elif x < 0 and y < 0:
                dir = 180 + math.degrees(math.atan(y/x))
            elif x > 0 > y:
                dir = 270 + math.degrees(math.atan(-x/y))
            elif x == 0:
                if y > 0:
                    dir = 90
                elif y < 0:
                    dir = 270
            elif y == 0:
                if x > 0:
                    dir = 0
                elif x < 0:
                    dir = 180
            else:
                dir = 0

            alfa = float(45)/2
            if (direction == FRONT and (dir < alfa or dir > 15*alfa)
                or (direction == FRONT_LEFT and alfa < dir < 3 * alfa)
                or (direction == LEFT and 3 * alfa < dir < 5 * alfa)
                or (direction == BACK_LEFT and 5 * alfa < dir < 7 * alfa)
                or (direction == BACK and 7 * alfa < dir < 9 * alfa)
                or (direction == BACK_RIGHT and 9 * alfa < dir < 11 * alfa)
                or (direction == RIGTH and 11 * alfa < dir < 13 * alfa)
                or (direction == FRONT_RIGTH and 13 * alfa < dir < 15 * alfa)):
                hits += 1
        if hits > max_hits_for_an_id:
            max_hits_for_an_id = hits

    os.remove(filename)
    return max_hits_for_an_id