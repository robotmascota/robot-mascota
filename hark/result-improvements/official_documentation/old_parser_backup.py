#!/usr/bin/env python
# license removed for brevity
from shutil import copyfile
import subprocess
import os
import processParser
import numpy as np

path = str(os.path.dirname(os.path.abspath(__file__)))
print(path)

# ----------- -----------

hark_network = 'demoOffline.n'
default_thresh = 47.6
default_lower_freq = 200
default_upper_freq = 4000
default_min_src_interval = 20
default_pseye_conf = 'pseye_geotf.zip'
default_num_sources = 0


# DEBUG settings
# thresh_list = range(0, 10, 5)
# min_src_interval_list = range(0, 15, 5)
# pseye_files = ['pseye_geotf.zip']

thresh_list = np.arange(30, 50, 0.1)
MAX_BOUND_FREQUENCY = 4000
min_src_interval_list = [20]
pseye_files = ['pseye_geotf.zip', 'pseye_rectf.zip']
audio_prefixes = ['audios/clean/']
audios = [
    '1-%s.wav' % processParser.FRONT_LEFT,
    '2-%s.wav' % processParser.FRONT,
    '3-%s.wav' % processParser.FRONT_RIGTH,
    '4-%s.wav' % processParser.LEFT,
    '6-%s.wav' % processParser.RIGTH,
    '7-%s.wav' % processParser.BACK_LEFT,
    '8-%s.wav' % processParser.BACK,
    '9-%s.wav' % processParser.BACK_RIGHT
]
num_sources = range(3)

# ------------------------------


def set_config(**settings):
    data = []
    data.append([
        'name="THRESH" type="float" value="%s"' % str(settings['old_thresh']),
        'name="THRESH" type="float" value="%s"' % str(settings['new_thresh'])
    ])
    data.append([
        'name="LOWER_BOUND_FREQUENCY" type="int" value="%d"' %
        settings['old_lower_freq'],
        'name="LOWER_BOUND_FREQUENCY" type="int" value="%d"' %
        settings['new_lower_freq']
    ])
    data.append([
        'name="UPPER_BOUND_FREQUENCY" type="int" value="%d"' %
        settings['old_upper_freq'],
        'name="UPPER_BOUND_FREQUENCY" type="int" value="%d"' %
        settings['new_upper_freq']
    ])
    data.append([
        'name="MIN_SRC_INTERVAL" type="float" value="%s"' %
        str(settings['old_min_src_interval']),
        'name="MIN_SRC_INTERVAL" type="float" value="%s"' %
        str(settings['new_min_src_interval'])
    ])
    data.append([
        'name="A_MATRIX" type="string" value="%s"' %
        settings['old_pseye_conf'],
        'name="A_MATRIX" type="string" value="%s"' %
        settings['new_pseye_conf']
    ])
    data.append([
        'name="NUM_SOURCE" type="int" value="%d"' %
        settings['old_num_sources'],
        'name="NUM_SOURCE" type="int" value="%d"' %
        settings['new_num_sources']
    ])

    with open(hark_network, 'r') as hark_network_file:
        filedata = hark_network_file.read()
        for d in data:
            filedata = filedata.replace(d[0], d[1])

    with open(hark_network, 'w') as hark_network_file:
        hark_network_file.write(filedata)


def print_to_file(data, filename):
    with open(filename, 'a+') as dat_file_to_append:
        dat_file_to_append.write(data + '\n')


old_thresh = default_thresh
new_thresh = old_thresh
old_lower_freq = default_lower_freq
new_lower_freq = old_lower_freq
old_upper_freq = default_upper_freq
new_upper_freq = old_upper_freq
old_min_src_interval = default_min_src_interval
new_min_src_interval = old_min_src_interval
old_pseye_conf = default_pseye_conf
new_pseye_conf = old_pseye_conf
old_num_sources = default_num_sources
new_num_sources = old_num_sources

os.remove(hark_network)
copyfile('default_demoOffline.n', hark_network)
os.chmod(hark_network, 0o777)
filename = 'results_new_fashion.txt'
open(filename, 'w').close()
freq_range = 2400

for nsources in num_sources:
    new_num_sources = nsources
    for thresh in thresh_list:
        max_hits = -1
        counter = 0
        new_thresh = thresh

        for lower_freq in range(0, MAX_BOUND_FREQUENCY - freq_range, 200):
            new_lower_freq = lower_freq
            upper_freq = lower_freq + freq_range
            new_upper_freq = upper_freq
            for min_src_interval in min_src_interval_list:
                new_min_src_interval = min_src_interval
                for pseye_conf in pseye_files:
                    new_pseye_conf = pseye_conf
                    config = {
                        'old_thresh': old_thresh,
                        'new_thresh': new_thresh,
                        'old_lower_freq': old_lower_freq,
                        'new_lower_freq': new_lower_freq,
                        'old_upper_freq': old_upper_freq,
                        'new_upper_freq': new_upper_freq,
                        'old_min_src_interval': old_min_src_interval,
                        'new_min_src_interval': new_min_src_interval,
                        'old_pseye_conf': old_pseye_conf,
                        'new_pseye_conf': new_pseye_conf,
                        'old_num_sources': old_num_sources,
                        'new_num_sources': new_num_sources
                    }
                    set_config(**config)
                    for prefix in audio_prefixes:
                        for audio in audios:
                            total_messures = 0
                            dir_hits = 0
                            front_back_hits = 0
                            left_right_hits = 0

                            complete_name = prefix + audio
                            data = "---------- processing audio %s ----------" % \
                                   complete_name
                            print_to_file(data, filename)
                            print(data)

                            # Launch hark network
                            f = open("log.txt", "w")
                            p = subprocess.Popen(
                                [path + '/' + hark_network,
                                 prefix + audio],
                                stdout=f, stderr=subprocess.STDOUT
                            )
                            p.wait()

                            detected_directions = processParser.process_file()
                            expected_direction = \
                                processParser.direction_data[audio[2:][:-4]]
                            for dd in detected_directions:
                                total_messures += 1
                                if dd[0] == expected_direction[0]:
                                    dir_hits += 1
                                if dd[1] == expected_direction[1]:
                                    front_back_hits += 1
                                if dd[2] == expected_direction[2]:
                                    left_right_hits += 1
                            if total_messures:
                                dir_hits = dir_hits / total_messures
                                front_back_hits = front_back_hits / total_messures
                                left_right_hits = left_right_hits / total_messures

                                data = 'thresh: %s, lower_freq: %d, upper_freq: %d, ' \
                                       'min_src_interval: %d, pseye_conf:%s, ' \
                                       'audio: %s' \
                                       '\n\t XY percentage: %d, ' \
                                       '\n\t X percentage: %d, ' \
                                       '\n\t Y percentage: %d' % (
                                    new_thresh, new_lower_freq, new_upper_freq,
                                    new_min_src_interval, pseye_conf, audio, dir_hits,
                                    front_back_hits, left_right_hits)
                                print_to_file(data, filename)
                    old_pseye_conf = new_pseye_conf
                old_min_src_interval = new_min_src_interval
            old_upper_freq = new_upper_freq
            old_lower_freq = new_lower_freq
        old_thresh = new_thresh
    old_num_sources = new_num_sources


# Reboot thresh value
config = {
    'old_thresh': old_thresh,
    'new_thresh': default_thresh,
    'old_lower_freq': old_lower_freq,
    'new_lower_freq': default_lower_freq,
    'old_upper_freq': old_upper_freq,
    'new_upper_freq': default_upper_freq,
    'old_min_src_interval': old_min_src_interval,
    'new_min_src_interval': default_min_src_interval,
    'old_pseye_conf': old_pseye_conf,
    'new_pseye_conf': default_pseye_conf,
    'old_num_sources': old_num_sources,
    'new_num_sources': default_num_sources
}
set_config(**config)
