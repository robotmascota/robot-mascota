#!/usr/bin/env python
import pylab
import sys

lines = [line.split()[2:] for line
         in open(sys.argv[1]) if "MUSIC" in line]
new_matrix = []
for line in lines:
    new_line = []
    for num in line:
        new_line.extend([float(num.replace(',', '.')) for i in range(0, 5)])
    new_matrix.append(new_line)
spec = pylab.array(new_matrix)

pylab.imshow(spec, interpolation="nearest", aspect="auto")
pylab.xlabel("Direction")
pylab.ylabel("Time [frame]")
pylab.colorbar()


pylab.ylim([0, 5])
pylab.xlim([360, 0])
frame1 = pylab.gca()
for xlabel_i in frame1.axes.get_xticklabels():
    xlabel_i.set_visible(False)

pylab.savefig(sys.argv[1].split('.')[0], bbox_inches='tight')
#pylab.show()

