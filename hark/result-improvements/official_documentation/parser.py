#!/usr/bin/env python
# license removed for brevity
from shutil import copyfile
import subprocess
import os

from official_documentation.trash import old_processLocalization
from official_documentation import processLocalization

path = str(os.path.dirname(os.path.abspath(__file__)))
print(path)

# ----------- -----------

data_file = 'results/data'
hark_network = 'demoOffline.n'
default_lower_freq = 300
default_upper_freq = 2700
default_pseye_conf = 'pseye_geotf.zip'

MAX_BOUND_FREQUENCY = 8000
pseye_files = ['pseye_geotf.zip', 'pseye_rectf.zip']

# DEBUG
# thresh_list = [15]
# MAX_BOUND_FREQUENCY = 4000
# min_src_interval_list = [20]
# pseye_files = ['pseye_geotf.zip']

audio_prefixes = ['audios/clean/']
audios = [
    '1-%s.wav' % processLocalization.FRONT_LEFT,
    '2-%s.wav' % processLocalization.FRONT,
    '3-%s.wav' % processLocalization.FRONT_RIGTH,
    '4-%s.wav' % processLocalization.LEFT,
    '6-%s.wav' % processLocalization.RIGTH,
    '7-%s.wav' % processLocalization.BACK_LEFT,
    '8-%s.wav' % processLocalization.BACK,
    '9-%s.wav' % processLocalization.BACK_RIGHT
]

# DEBUG
# audio_prefixes = ['audios/clean/']
# audios = [
#     '1-%s.wav' % processLocalization.FRONT_LEFT
# ]

# ------------------------------


def set_config(**settings):
    data = []
    data.append([
        'name="LOWER_BOUND_FREQUENCY" type="int" value="%d"' %
        settings['old_lower_freq'],
        'name="LOWER_BOUND_FREQUENCY" type="int" value="%d"' %
        settings['new_lower_freq']
    ])
    data.append([
        'name="UPPER_BOUND_FREQUENCY" type="int" value="%d"' %
        settings['old_upper_freq'],
        'name="UPPER_BOUND_FREQUENCY" type="int" value="%d"' %
        settings['new_upper_freq']
    ])
    data.append([
        'name="A_MATRIX" type="string" value="%s"' %
        settings['old_pseye_conf'],
        'name="A_MATRIX" type="string" value="%s"' %
        settings['new_pseye_conf']
    ])

    with open(hark_network, 'r') as hark_network_file:
        filedata = hark_network_file.read()
        for d in data:
            filedata = filedata.replace(d[0], d[1])

    with open(hark_network, 'w') as hark_network_file:
        hark_network_file.write(filedata)


def print_to_file(data, filename):
    with open(filename, 'a+') as dat_file_to_append:
        dat_file_to_append.write(data + '\n')


old_lower_freq = default_lower_freq
new_lower_freq = old_lower_freq
old_upper_freq = default_upper_freq
new_upper_freq = old_upper_freq
old_pseye_conf = default_pseye_conf
new_pseye_conf = old_pseye_conf

os.remove('demoOffline.n')
copyfile('default_demoOffline.n', 'demoOffline.n')
os.chmod('demoOffline.n', 0o777)
filename = 'results.txt'
open(filename, 'w').close()

max_hits = 0
max_hits_config = {}
max_hits_directions = {}

upper_freq = 0
index = 0
last_index = -1

for lower_freq in range(0, MAX_BOUND_FREQUENCY, 200):
    if index > last_index:
        new_lower_freq = lower_freq
        upper_freq = lower_freq+2000
        new_upper_freq = upper_freq
    for pseye_conf in pseye_files:
        if index > last_index:
            new_pseye_conf = pseye_conf
        for prefix in audio_prefixes:
            hits = 0
            directions = []
            for audio in audios:
                if index > last_index:
                    config = {
                        'old_lower_freq': old_lower_freq,
                        'new_lower_freq': new_lower_freq,
                        'old_upper_freq': old_upper_freq,
                        'new_upper_freq': new_upper_freq,
                        'old_pseye_conf': old_pseye_conf,
                        'new_pseye_conf': new_pseye_conf
                    }
                    set_config(**config)
                    # Launch hark network
                    f = open("log.txt", "w")
                    p = subprocess.Popen(
                        [path + '/' + hark_network,
                         prefix + audio],
                        stdout=f, stderr=subprocess.STDOUT
                    )
                    p.wait()

                    detected_direction = old_processLocalization.process_file()
                    expected_direction = audio[2:][:-4]
                    if detected_direction == expected_direction:
                        hits += 1
                        directions.append(detected_direction)
                index += 1


            local_conf = 'lower_freq:%s, upper_freq:%s, pseye_conf:%s --> ' \
                         'hits: %d' % (lower_freq, upper_freq, pseye_conf, hits)

            data = ">  processing index: %d, hits:%d ---" % (index, hits)
            print_to_file(data, filename)
            print data
            data = "    Config: %s \n    Directions: %s " % (
                local_conf, directions)
            print_to_file(data, filename)
            print data

            if hits > max_hits:
                max_hits = hits
                if max_hits not in max_hits_config:
                    max_hits_config[max_hits] = []
                max_hits_config[max_hits].append(local_conf)
                if max_hits not in max_hits_directions:
                    max_hits_directions[max_hits] = []
                max_hits_directions[max_hits].append(directions)
        old_pseye_conf = new_pseye_conf
    old_upper_freq = new_upper_freq
    old_lower_freq = new_lower_freq


data = "--------- Max hits: %s ---------" % max_hits
print_to_file(data, filename)
print data
for i in range(len(max_hits_config[max_hits])):
    data = "    Config: %s \n    Directions: %s " % (
        max_hits_config[max_hits][i], max_hits_directions[max_hits][i])
    print_to_file(data, filename)
    print data

# Reboot thresh value
config = {
    'old_lower_freq': old_lower_freq,
    'new_lower_freq': default_lower_freq,
    'old_upper_freq': old_upper_freq,
    'new_upper_freq': default_upper_freq,
    'old_pseye_conf': old_pseye_conf,
    'new_pseye_conf': default_pseye_conf
}
set_config(**config)
