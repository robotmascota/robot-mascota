#!/usr/bin/env python
# license removed for brevity
from __future__ import division
from shutil import copyfile
import subprocess
import os
from old_experiments.processParser import direction_data

path = str(os.path.dirname(os.path.abspath(__file__)))
print(path)

from old_experiments import processParser
# ----------- -----------

hark_network = 'demoOffline.n'
default_pseye_conf = 'pseye_geotf.zip'
pseye_files = ['pseye_geotf.zip', 'pseye_rectf.zip']
default_thresh = 47.6

# DEBUG
# pseye_files = ['pseye_geotf.zip']

audio_prefixes = ['./']
audios = [
    '1-%s.wav' % processParser.FRONT_LEFT,
    '2-%s.wav' % processParser.FRONT,
    '3-%s.wav' % processParser.FRONT_RIGTH,
    '4-%s.wav' % processParser.LEFT,
    '6-%s.wav' % processParser.RIGTH,
    '7-%s.wav' % processParser.BACK_LEFT,
    '8-%s.wav' % processParser.BACK,
    '9-%s.wav' % processParser.BACK_RIGHT
]
thresh_list = {
    'pseye_rectf.zip': {
        processParser.FRONT_LEFT: 45.5,
        processParser.FRONT: 46.9,
        processParser.FRONT_RIGTH: 48,
        processParser.LEFT: 43.9,
        processParser.RIGTH: 46,
        processParser.BACK_LEFT: 46,
        processParser.BACK: 46,
        processParser.BACK_RIGHT: 46
    },
    'pseye_geotf.zip': {
        processParser.FRONT_LEFT: 48,
        processParser.FRONT: 46.2,
        processParser.FRONT_RIGTH: 46.1,
        processParser.LEFT: 45,
        processParser.RIGTH: 46,
        processParser.BACK_LEFT: 46,
        processParser.BACK: 45,
        processParser.BACK_RIGHT: 45
    }
}



# DEBUG
# audio_prefixes = ['audios/clean/']
# audios = [
#     '1-%s.wav' % processParser.FRONT_LEFT
# ]

# ------------------------------


def set_config(**settings):
    data = []
    data.append([
        'name="THRESH" type="float" value="%d"' % settings['old_thresh'],
        'name="THRESH" type="float" value="%d"' % settings['new_thresh']
    ])
    data.append([
        'name="A_MATRIX" type="string" value="%s"' %
        settings['old_pseye_conf'],
        'name="A_MATRIX" type="string" value="%s"' %
        settings['new_pseye_conf']
    ])

    with open(hark_network, 'r') as hark_network_file:
        filedata = hark_network_file.read()
        for d in data:
            filedata = filedata.replace(d[0], d[1])

    with open(hark_network, 'w') as hark_network_file:
        hark_network_file.write(filedata)


def print_to_file(data, filename):
    with open(filename, 'a+') as dat_file_to_append:
        dat_file_to_append.write(data + '\n')


old_pseye_conf = default_pseye_conf
new_pseye_conf = old_pseye_conf
old_thresh = default_thresh
new_thresh = old_thresh
os.remove('demoOffline.n')
copyfile('default_demoOffline.n', 'demoOffline.n')
os.chmod('demoOffline.n', 0o777)
filename = 'results.txt'
open(filename, 'w').close()

max_hits = 0
max_hits_config = {}
max_hits_directions = {}

upper_freq = 0
last_index = -1

for pseye_conf in pseye_files:
    new_pseye_conf = pseye_conf
    for prefix in audio_prefixes:
        for audio in audios:
            total_messures = 0
            dir_hits = 0
            front_back_hits = 0
            left_right_hits = 0

            new_thresh = thresh_list[pseye_conf][audio[2:][:-4]]
            config = {
                'old_pseye_conf': old_pseye_conf,
                'new_pseye_conf': new_pseye_conf,
                'old_thresh': old_thresh,
                'new_thresh': new_thresh
            }
            old_thresh = new_thresh

            set_config(**config)
            # Launch hark network
            f = open("log.txt", "w")
            p = subprocess.Popen(
                [path + '/' + hark_network,
                 prefix + audio],
                stdout=f, stderr=subprocess.STDOUT
            )
            p.wait()

            detected_directions = processParser.process_file()
            expected_direction = direction_data[audio[2:][:-4]]
            for dd in detected_directions:
                total_messures += 1
                if dd[0] == expected_direction[0]:
                    dir_hits += 1
                if dd[1] == expected_direction[1]:
                    front_back_hits += 1
                if dd[2] == expected_direction[2]:
                    left_right_hits += 1

            dir_hits = dir_hits / total_messures
            front_back_hits = front_back_hits / total_messures
            left_right_hits = left_right_hits / total_messures

            data = 'audio: %s , thresh: %s--> pseye_conf:%s, ' \
                   '\t\t\t hit percentage: %d, ' \
                   '\t\t\t front_back_hit percentage: %d, ' \
                   '\t\t\t left_right_hit percentage: %d' % (
                audio, new_thresh, pseye_conf, dir_hits, front_back_hits,
                left_right_hits)
            print_to_file(data, filename)
    old_pseye_conf = new_pseye_conf

# Reboot values
config = {
    'old_pseye_conf': old_pseye_conf,
    'new_pseye_conf': default_pseye_conf,
    'old_thresh': old_thresh,
    'new_thresh': new_thresh
}
set_config(**config)
