import math

FRONT_LEFT = 'x-adelante_y-izquierda'
FRONT = 'x-adelante'
FRONT_RIGTH = 'x-adelante_y-derecha'
LEFT = 'y-izquierda'
RIGTH = 'y-derecha'
BACK_LEFT = 'x-atras_y-izquierda'
BACK = 'x-atras'
BACK_RIGHT = 'x-atras_y-derecha'

# key: direction name
# value: (direction, x-direction, y-direction)
direction_data = {
    'x-adelante': 0,
    'x-adelante_y-izquierda': 45,
    'y-izquierda': 90,
    'x-atras_y-izquierda': 135,
    'x-atras': 180,
    'x-atras_y-derecha': 225,
    'y-derecha': 270,
    'x-adelante_y-derecha': 315
}


def process_file():
    filename = 'Localization.txt'

    sum_x = 0
    sum_y = 0
    count = 0
    for line in open(filename):
        line_to_process = False
        if "x=\"" in line and "y=\"" in line and "id=\"0\"" in line:
            line_to_process = True

        if line_to_process:
            count += 1
            sum_x += float(line.split("x=\"")[1].split("\"")[0].replace(',', '.'))
            sum_y += float(line.split("y=\"")[1].split("\"")[0].replace(',', '.'))

    grades = -1
    if count > 0:
        x = sum_x / count
        y = sum_y / count
        if x > 0 and y > 0:
            grades = math.degrees(math.atan(y/x))
        elif x < 0 < y:
            grades = 90 + math.degrees(math.atan(-x/y))
        elif x < 0 and y < 0:
            grades = 180 + math.degrees(math.atan(y/x))
        elif x > 0 > y:
            grades = 270 + math.degrees(math.atan(-x/y))
        elif x == 0:
            if y > 0:
                grades = 90
            elif y < 0:
                grades = 270
        elif y == 0:
            if x > 0:
                grades = 0
            elif x < 0:
                grades = 180

    return grades
