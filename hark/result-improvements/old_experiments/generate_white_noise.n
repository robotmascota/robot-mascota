#!/usr/bin/env batchflow
<?xml version="1.0"?>
<Document>
  <Network type="subnet" name="MAIN">
    <Node name="node_Constant_1" type="Constant" x="280" y="200">
      <Parameter name="VALUE" type="subnet_param" value="ARG1" description="The value"/>
    </Node>
    <Node name="node_InputStream_1" type="InputStream" x="530" y="190">
      <Parameter name="TYPE" type="int" value="" description="Type of stream: stream, fd, or FILE (default stream)"/>
      <Parameter name="RETRY" type="int" value="" description="If set to N, InputStream will retry N times on open fail"/>
    </Node>
    <Node name="node_MAIN_LOOP_1" type="MAIN_LOOP" x="810" y="190">
    </Node>
    <Link from="node_Constant_1" output="VALUE" to="node_InputStream_1" input="INPUT"/>
    <Link from="node_InputStream_1" output="OUTPUT" to="node_MAIN_LOOP_1" input="INPUT"/>
    <NetOutput name="OUTPUTCM" node="node_MAIN_LOOP_1" terminal="OUTPUTCM" object_type="any" description="Dynamic"/>
  </Network>
  <Network type="iterator" name="MAIN_LOOP">
    <Node name="node_AudioStreamFromWave_1" type="AudioStreamFromWave" x="280" y="130">
      <Parameter name="LENGTH" type="int" value="512" description="The frame length of each channel (in samples) [default: 512]."/>
      <Parameter name="ADVANCE" type="int" value="160" description="The shift length beween adjacent frames (in samples)[default: 160]."/>
      <Parameter name="USE_WAIT" type="bool" value="false" description="If true, real recording is simulated [default: false]."/>
    </Node>
    <Node name="node_ChannelSelector_1" type="ChannelSelector" x="650" y="130">
      <Parameter name="SELECTOR" type="object" value="&lt;Vector&lt;int&gt; &gt;" description="Channel selection setting which is a vector consisting of channel IDs (ID starts with 0). When the first three channels are selected from four channels, this should be set to &lt;Vector&lt;int&gt; 0 1 2&gt;."/>
    </Node>
    <Node name="node_MultiFFT_1" type="MultiFFT" x="1140" y="130">
      <Parameter name="LENGTH" type="int" value="512" description="FFT length in sample. [default: 512]"/>
      <Parameter name="WINDOW" type="string" value="CONJ" description="A window function for FFT. WINDOW should be CONJ, HAMMING, RECTANGLE, or HANNING. [default: CONJ]"/>
      <Parameter name="WINDOW_LENGTH" type="int" value="512" description="Window length of the window function. [default: 512]"/>
    </Node>
    <Node name="node_CMMakerFromFFTwithFlag_1" type="CMMakerFromFFTwithFlag" x="1020" y="300">
      <Parameter name="DURATION_TYPE" type="string" value="FLAG_PERIOD" description="If FLAG_PERIOD, CM is generated based on ADDER_FLAG. If WINDOW_PERIOD, CM is generated every PERIOD frames."/>
      <Parameter name="WINDOW" type="int" value="50" description="The number of frames used for calculating a correlation function."/>
      <Parameter name="PERIOD" type="int" value="50" description="The period for outputting the correlation matrix. Even if ADDER_FLAG is kept to be 1, this block forcely executes NormalizeCorrelation when sum_count is over PERIOD. Set this value 0 if you do not want to use this function."/>
      <Parameter name="WINDOW_TYPE" type="string" value="FUTURE" description="Window selection to accumulate a correlation function. If PAST, the past WINDOW frames from the current frame are used for the accumulation. If MIDDLE, the current frame will be the middle of the accumulated frames. If FUTURE, the future WINDOW frames from the current frame are used for the accumulation. FUTURE is the default from version 1.0, but this makes a delay since we have to wait for the future information. PAST generates a internal buffers for the accumulation, which realizes no delay for localization."/>
      <Parameter name="MAX_SUM_COUNT" type="int" value="100" description="The maximum count for adding the correlation matrix. Even if ADDER_FLAG is kept to be 1, this block forcely executes NormalizeCorrelation when sum_count is over MAX_SUM_COUNT. Set this value 0 if you do not want to use this function."/>
      <Parameter name="ENABLE_ACCUM" type="bool" value="false" description="enable to accumulate the hitory of correlation matrix."/>
      <Parameter name="ENABLE_DEBUG" type="bool" value="true" description="enable debug print"/>
    </Node>
    <Node name="node_Smaller_1" type="Smaller" x="730" y="320">
    </Node>
    <Node name="node_Constant_2" type="Constant" x="730" y="560">
      <Parameter name="VALUE" type="string" value="NOISEi.dat" description="The value"/>
    </Node>
    <Node name="node_Constant_3" type="Constant" x="280" y="700">
      <Parameter name="VALUE" type="int" value="50" description="The value"/>
    </Node>
    <Node name="node_Constant_4" type="Constant" x="730" y="440">
      <Parameter name="VALUE" type="string" value="NOISEr.dat" description="The value"/>
    </Node>
    <Node name="node_IterCount_1" type="IterCount" x="280" y="320">
    </Node>
    <Node name="node_CMSave_1" type="CMSave" x="1020" y="520">
      <Parameter name="ENABLE_DEBUG" type="bool" value="true" description="enable debug print"/>
    </Node>
    <Node name="node_Equal_1" type="Equal" x="730" y="680">
    </Node>
    <Link from="node_AudioStreamFromWave_1" output="AUDIO" to="node_ChannelSelector_1" input="INPUT"/>
    <Link from="node_ChannelSelector_1" output="OUTPUT" to="node_MultiFFT_1" input="INPUT"/>
    <Link from="node_MultiFFT_1" output="OUTPUT" to="node_CMMakerFromFFTwithFlag_1" input="INPUT"/>
    <Link from="node_Smaller_1" output="OUTPUT" to="node_CMMakerFromFFTwithFlag_1" input="ADDER_FLAG"/>
    <Link from="node_IterCount_1" output="OUTPUT" to="node_Smaller_1" input="INPUT1"/>
    <Link from="node_IterCount_1" output="OUTPUT" to="node_Equal_1" input="INPUT1"/>
    <Link from="node_Constant_3" output="VALUE" to="node_Equal_1" input="INPUT2"/>
    <Link from="node_Constant_3" output="VALUE" to="node_Smaller_1" input="INPUT2"/>
    <Link from="node_Constant_4" output="VALUE" to="node_CMSave_1" input="FILENAMER"/>
    <Link from="node_CMMakerFromFFTwithFlag_1" output="OUTPUT" to="node_CMSave_1" input="INPUTCM"/>
    <Link from="node_Constant_2" output="VALUE" to="node_CMSave_1" input="FILENAMEI"/>
    <Link from="node_Equal_1" output="OUTPUT" to="node_CMSave_1" input="OPERATION_FLAG"/>
    <NetInput name="INPUT" node="node_AudioStreamFromWave_1" terminal="INPUT" object_type="Stream" description="An audio input stream (IStream)."/>
    <NetOutput name="OUTPUTCM" node="node_CMSave_1" terminal="OUTPUTCM" object_type="Matrix&lt;complex&lt;float&gt; &gt;" description="Same as INPUTCM"/>
    <NetCondition name="CONDITION" node="node_AudioStreamFromWave_1" terminal="NOT_EOF"/>
  </Network>
</Document>
