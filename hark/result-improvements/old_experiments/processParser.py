import math

FRONT_LEFT = 'x-adelante_y-izquierda'
FRONT = 'x-adelante'
FRONT_RIGTH = 'x-adelante_y-derecha'
LEFT = 'y-izquierda'
RIGTH = 'y-derecha'
BACK_LEFT = 'x-atras_y-izquierda'
BACK = 'x-atras'
BACK_RIGHT = 'x-atras_y-derecha'

# key: direction name
# value: (direction, x-direction, y-direction)
direction_data = {
    'x-adelante_y-izquierda': ('adelante_izquierda', 'adelante', 'izquierda'),
    'x-adelante': ('adelante', 'adelante', None),
    'x-adelante_y-derecha': ('adelante_derecha', 'adelante', 'derecha'),
    'y-izquierda': ('izquierda', None, 'izquierda'),
    'y-derecha': ('derecha', None, 'derecha'),
    'x-atras_y-izquierda': ('atras_izquierda', 'atras', 'izquierda'),
    'x-atras': ('atras', 'atras', None),
    'x-atras_y-derecha': ('atras_derecha', 'atras', 'derecha')
}


def process_file():
    filename = 'Localization.txt'

    directions = []
    for line in open(filename):
        line_to_process = False
        if "x=\"" in line and "y=\"" in line:
            line_to_process = True

        if line_to_process:
            x = float(line.split("x=\"")[1].split("\"")[0].replace(',', '.'))
            y = float(line.split("y=\"")[1].split("\"")[0].replace(',', '.'))
            dir = 0
            if x > 0 and y > 0:
                dir = math.degrees(math.atan(y/x))
            elif x < 0 < y:
                dir = 90 + math.degrees(math.atan(-x/y))
            elif x < 0 and y < 0:
                dir = 180 + math.degrees(math.atan(y/x))
            elif x > 0 > y:
                dir = 270 + math.degrees(math.atan(-x/y))
            elif x == 0:
                if y > 0:
                    dir = 90
                elif y < 0:
                    dir = 270
            elif y == 0:
                if x > 0:
                    dir = 0
                elif x < 0:
                    dir = 180
        
            alfa = float(45)/2
            if dir < alfa or dir > 15*alfa:
                directions.append(direction_data[FRONT])
            if alfa < dir < 3 * alfa:
                directions.append(direction_data[FRONT_LEFT])
            if 3 * alfa < dir < 5 * alfa:
                directions.append(direction_data[LEFT])
            if 5 * alfa < dir < 7 * alfa:
                directions.append(direction_data[BACK_LEFT])
            if 7 * alfa < dir < 9 * alfa:
                directions.append(direction_data[BACK])
            if 9 * alfa < dir < 11 * alfa:
                directions.append(direction_data[BACK_RIGHT])
            if 11 * alfa < dir < 13 * alfa:
                directions.append(direction_data[RIGTH])
            if 13 * alfa < dir < 15 * alfa:
                directions.append(direction_data[FRONT_RIGTH])

    return directions
