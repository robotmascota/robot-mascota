#!/usr/bin/env python
# license removed for brevity
from __future__ import division
from shutil import copyfile
import subprocess
import os
from old_experiments.processParserFINAL import direction_data

path = str(os.path.dirname(os.path.abspath(__file__)))
print(path)

from old_experiments import processParserFINAL
# ----------- -----------

hark_network = 'demoOffline.n'
default_pseye_conf = 'pseye_geotf.zip'
default_num_source = 1
default_thresh = 47.6
default_music_algorithm = 'GEVD'

pseye_files = ['pseye_geotf.zip', 'pseye_rectf.zip']
num_sources = [1]
algorithms = ['GEVD', 'SEVD', 'GSVD']

# DEBUG
# pseye_files = ['pseye_geotf.zip']

prefix = './'
audios = [
    '1-%s.wav' % processParserFINAL.FRONT_LEFT,
    '2-%s.wav' % processParserFINAL.FRONT,
    '3-%s.wav' % processParserFINAL.FRONT_RIGTH,
    '4-%s.wav' % processParserFINAL.LEFT,
    '6-%s.wav' % processParserFINAL.RIGTH,
    '7-%s.wav' % processParserFINAL.BACK_LEFT,
    '8-%s.wav' % processParserFINAL.BACK,
    '9-%s.wav' % processParserFINAL.BACK_RIGHT
]
thresh_list = {
    'pseye_rectf.zip': {
        processParserFINAL.FRONT_LEFT:
        processParserFINAL.FRONT:
        processParserFINAL.FRONT_RIGTH:
        processParserFINAL.LEFT:
        processParserFINAL.RIGTH:
        processParserFINAL.BACK_LEFT:
        processParserFINAL.BACK:
        processParserFINAL.BACK_RIGHT: 22.6 #22.69
    },
    'pseye_geotf.zip': {
        # min thresh registered 22.32, fortunately the same for each audio
        processParserFINAL.FRONT_LEFT: 22.3,
        processParserFINAL.FRONT: 22.3,
        processParserFINAL.FRONT_RIGTH: 22.3,
        processParserFINAL.LEFT: 22.3,
        processParserFINAL.RIGTH: 22.3,
        processParserFINAL.BACK_LEFT: 22.3,
        processParserFINAL.BACK: 22.3,
        processParserFINAL.BACK_RIGHT: 22.3
    }
}

# ------------------------------


def set_config(**settings):
    data = []
    data.append([
        'name="THRESH" type="float" value="%d"' % settings['old_thresh'],
        'name="THRESH" type="float" value="%d"' % settings['new_thresh']
    ])
    data.append([
        'name="A_MATRIX" type="string" value="%s"' %
        settings['old_pseye_conf'],
        'name="A_MATRIX" type="string" value="%s"' %
        settings['new_pseye_conf']
    ])
    data.append([
        'name="NUM_SOURCE" type="int" value="%d"' % settings['old_num_source'],
        'name="NUM_SOURCE" type="int" value="%d"' % settings['new_num_source']
    ])
    data.append([
        'name="MUSIC_ALGORITHM" type="string" value="%s"' % settings['old_music_algorithm'],
        'name="MUSIC_ALGORITHM" type="string" value="%s"' % settings['new_music_algorithm']
    ])

    with open(hark_network, 'r') as hark_network_file:
        filedata = hark_network_file.read()
        for d in data:
            filedata = filedata.replace(d[0], d[1])

    with open(hark_network, 'w') as hark_network_file:
        hark_network_file.write(filedata)


def print_to_file(data, filename):
    with open(filename, 'a+') as dat_file_to_append:
        dat_file_to_append.write(data + '\n')


old_pseye_conf = default_pseye_conf
old_thresh = default_thresh
new_thresh = old_thresh
old_num_source = default_num_source
old_music_algorithm = default_music_algorithm

os.remove('demoOffline.n')
copyfile('default_demoOffline.n', 'demoOffline.n')
os.chmod('demoOffline.n', 0o777)
filename = 'resultsFINAL.txt'
open(filename, 'w').close()

max_hits = 0
max_hits_config = {}
max_hits_directions = {}

upper_freq = 0
last_index = -1

for pseye_conf in pseye_files:
    new_pseye_conf = pseye_conf
    for num_source in num_sources:
        new_num_source = num_source
        for algorithm in algorithms:
            new_music_algorithm = algorithm

            for audio in audios:
                total_messures = 0
                dir_hits = 0
                front_back_hits = 0
                left_right_hits = 0

                new_thresh = thresh_list[pseye_conf][audio[2:][:-4]]
                config = {
                    'old_pseye_conf': old_pseye_conf,
                    'new_pseye_conf': new_pseye_conf,
                    'old_thresh': old_thresh,
                    'new_thresh': new_thresh,
                    'old_num_source': old_num_source,
                    'new_num_source': new_num_source,
                    'old_music_algorithm': old_music_algorithm,
                    'new_music_algorithm': new_music_algorithm,
                }
                old_thresh = new_thresh

                set_config(**config)
                # Launch hark network
                f = open("Localization.txt", "w")
                p = subprocess.Popen(
                    [path + '/' + hark_network,
                     prefix + audio, 'Localization.txt'],
                    stdout=f, stderr=subprocess.STDOUT
                )
                p.wait()

                detected_grades = processParserFINAL.process_file()
                expected_grades = direction_data[audio[2:][:-4]]
                #  audio, pseye_conf, num_source, music_algorithm, detected_grades, expected_grades
                data = '%s, %s, %s, %s, %d, %d' % (
                    audio, new_pseye_conf, new_num_source, new_music_algorithm,
                    detected_grades, expected_grades)
                print(data)
                print_to_file(data, filename)

            old_music_algorithm = new_music_algorithm
        old_num_source = new_num_source
    old_pseye_conf = new_pseye_conf

# Reboot values
config = {
    'old_pseye_conf': old_pseye_conf,
    'new_pseye_conf': default_pseye_conf,
    'old_thresh': old_thresh,
    'new_thresh': default_thresh,
    'old_num_source': old_num_source,
    'new_num_source': default_num_source,
    'old_music_algorithm': old_music_algorithm,
    'new_music_algorithm': default_music_algorithm,
}
set_config(**config)
