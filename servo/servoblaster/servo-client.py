import cmd
import socket

class ServoController(cmd.Cmd):

	def __init__(self):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.connect(('192.168.1.55',51717));
		cmd.Cmd.__init__(self)

	def do_w(self,line):
		self.socket.send("1")
		print "adelante"

	def do_s(self,line):
		self.socket.send("2")
		print "atras"

	def do_izquierda(self,line):
		self.socket.send("3")
		print "izquierda"

	def do_derecha(self, line):
		self.socket.send("4")
		print "derecha"

	def do_quieto(self, line):
		self.socket.send("5")
		print "stop"

	def do_EOF(self, line):
		return True

if __name__ == '__main__':
	ServoController().cmdloop()

